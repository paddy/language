SRC=$(shell find -name '*.py')
TPL=$(shell find templates/ templates/admin/ templates/cards/ -maxdepth 1 -type f)

LANGUAGES=de

all: $(foreach x,$(LANGUAGES),po/$(x)/LC_MESSAGES/messages.mo)

po/messages.po: $(SRC) $(TPL)
	xgettext -L python --from-code utf-8 --foreign-user -o $@ $^
	sed -ie 's/CHARSET/UTF-8/' $@

po/%.po: po/messages.po
	msgmerge -U $@ $^

po/%/LC_MESSAGES/messages.mo: po/%.po
	mkdir -p $$(dirname $@)
	msgfmt -o $@ $^
