#!/usr/bin/env python3

import gettext
gettext.translation('messages', "./po/", languages=['de'], fallback=True).install()

from typing import *

from collections import defaultdict
import json
import sys
from languagedata import LanguageData, ParseError


def main() -> None:
    if len(sys.argv) == 2:
        filename = sys.argv[1]
    else:
        filename = "data/content.json"

    print("Importing %s..." % filename)
    try:
        ld = LanguageData.import_file(filename)
    except ParseError as e:
        sys.exit("Error: %s" % e)

    result = {
        'words': ld.word_index,
        'concepts': ld.concept_index,
        'phrases': ld.phrase_index,
        'lessons': ld.lesson_index,
        'courses': ld.course_index,
    }
    print(json.dumps(result, indent=4, ensure_ascii=False))

if __name__ == '__main__':
    main()
