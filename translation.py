from typing import *

import gettext as system_gettext
import sys

import bottle

translations = {
    'de': system_gettext.translation('messages', "./po/", languages=['de'], fallback=True),
    'en': system_gettext.NullTranslations(),
    None: system_gettext.translation('messages', "./po/", fallback=True),
}

use_bottle = False

def request_language() -> Optional[str]:
    if hasattr(bottle.request, 'request_language'):
        assert isinstance(bottle.request.request_language, str)
        return bottle.request.request_language
    else:
        return None

class LazyGettext:
    def __init__(self, msg: str) -> None:
        self.msg = msg

    def __str__(self) -> str:
        return translations[request_language()].gettext(self.msg)

    def __getattr__(self, attr: str) -> Any:
        if attr == '__setstate__':
            raise AttributeError
        return getattr(str(self), attr)

    def __format__(self, format_spec: Any) -> Any:
        return str(self).__format__(format_spec)

    def __mod__(self, other: Any) -> Any:
        return str(self).__mod__(other)

    def __add__(self, other: Any) -> Any:
        return str(self) + other

    def __radd__(self, other: Any) -> Any:
        return other + str(self)

    @property # type: ignore
    def __class__(self): # type: ignore
        return str

class LazyNGettext(LazyGettext):
    def __init__(self, msg: str, msgpl: str, n: int) -> None:
        self.msg = msg
        self.msgpl = msgpl
        self.n = n

    def __str__(self) -> str:
        return (translations[request_language()]
                .ngettext(self.msg, self.msgpl, self.n))

def gettext(msg: str) -> str:
    if use_bottle:
        # This is a lie, but LazyGettext tries to behave like str, so hopefully
        # we can get away with it
        return cast(str, LazyGettext(msg))
    else:
        return translations[None].gettext(msg)

def ngettext(msg: str, msgpl: str, n: int) -> str:
    if use_bottle:
        # This is a lie, but LazyGettext tries to behave like str, so hopefully
        # we can get away with it
        return cast(str, LazyNGettext(msg, msgpl, n))
    else:
        return translations[None].ngettext(msg, msgpl, n)

def validate_language(lang: str) -> bool:
    return lang in translations

class TranslationBottlePlugin:
    def __init__(self) -> None:
        bottle.BaseTemplate.defaults.update({
            '_': gettext,
            'ngettext': ngettext,
        })

    def apply(self, callback: Callable[..., Any], route: str) -> Any:
        def wrapper(*args: Any, **kwargs: Any) -> Any:
            lang = bottle.request.get_cookie('lang', 'ga')
            bottle.request.request_language = lang if lang in translations else 'de'
            return callback(*args, **kwargs)
        return wrapper

