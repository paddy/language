from typing import *

import abc
from enum import Enum
from uuid import UUID
import json_util

from .language import (
    Language, Dialect, DictWord, ConceptTranslationRule, TranslationContext)

from translation import gettext as _


class LessonContext(metaclass=abc.ABCMeta):
    def __init__(self, alias_resolver: json_util.AliasResolver) -> None:
        self.alias_resolver = alias_resolver

    @abc.abstractmethod
    def get_translation_context(self, lang: Language) -> TranslationContext:
        pass

class LessonPage:
    def __init__(self, lesson: 'Lesson', title: Optional[str] = None) -> None:
        self.lesson = lesson
        self.title = title

class Lesson:
    class ParseError(Exception):
        pass

    page_types: Dict[str, Type['LessonPage']] = {}

    def __init__(self, lctx: LessonContext,
                 id: int, name: str,
                 source_lang: str, target_lang: str,
                 sections: Sequence[Any],
                 alias: Optional[str] = None) -> None:
        self.lctx = lctx
        self.id = id
        self.name = name
        self.alias = alias
        self.lang = Language.get_by_code(target_lang)
        self.src_lang = Language.get_by_code(source_lang)

        self.pages: List[LessonPage] = []
        for s in sections:
            if not isinstance(s, dict):
                raise Lesson.ParseError(_("'sections' must only contain objects"))

            section_type = s.pop('type', 'text')
            if section_type not in self.page_types:
                raise Lesson.ParseError(_("Unknown section type '%s'") % section_type)

            s['lesson'] = self
            self.pages.append(json_util.create_from_json_dict(
                self.page_types[section_type], s))

class CourseSkillType(Enum):
    WORDS = 1
    GRAMMAR = 2

class Course:
    class Word:
        def __init__(self, word: str, dialects: Sequence[Dialect] = ()) -> None:
            self.uuid = UUID(word)
            self.dialects = Dialect.propagate_subdialects(set(dialects))

        @classmethod
        def create(cls, lang: Language, definition: Any) -> 'Course.Word':
            if isinstance(definition, str):
                return cls(word=definition)
            else:
                assert isinstance(definition, dict)
                return json_util.create_from_json_dict(cls, definition, {
                    Dialect: lang.dialects
                })

    class Skill:
        def __init__(self, id: int, lang: Language, type: CourseSkillType,
                     name: str, section: str, description: Optional[str] = None,
                     words: Sequence[Any] = (), tags: Sequence[str] = (),
                     priority: int = 50,
                     lesson: Optional[str] = None,
                     emoji: Optional[str] = None) -> None:
            self.id = id
            self.skill_type = type
            self.name = name
            self.section = section
            self.description = description
            # TODO Split into words counting towards the skill and words only
            # allowed to appear in phrases to make sentences possible
            self.words = [ Course.Word.create(lang, w) for w in words ]
            self.tags = tags
            self.priority = priority
            self.lesson = lesson
            self.emoji = emoji

        def get_words(self, config: 'Course.Config') -> Sequence['Course.Word']:
            return [ w for w in self.words
                     if not w.dialects or config.target_dialect in w.dialects ]

    class Config:
        def __init__(self, source_dialect: Dialect,
                     target_dialect: Dialect) -> None:
            self.source_dialect = source_dialect
            self.target_dialect = target_dialect

    def __init__(self, id: int, name: str,
                 source_lang: str, target_lang: str,
                 source_dialects: Sequence[str] = (),
                 target_dialects: Sequence[str] = (),
                 lessons: Sequence[str] = (),
                 skills: Sequence[Any] = ()) -> None:
        self.id = id
        self.name = name
        self.lessons = [UUID(l) for l in lessons]

        self.lang = Language.get_by_code(target_lang)
        if target_dialects:
            self.dialects = [self.lang.dialects[d] for d in target_dialects]
        else:
            self.dialects = [self.lang.default_dialect]
            self.dialects += [d for d in self.lang.dialects
                              if d != self.lang.default_dialect]

        self.src_lang = Language.get_by_code(source_lang)
        if source_dialects:
            self.src_dialects = [self.src_lang.dialects[d] for d in source_dialects]
        else:
            self.src_dialects = [self.src_lang.default_dialect]
            self.src_dialects += [d for d in self.src_lang.dialects
                                  if d != self.src_lang.default_dialect]

        self.skills = [
            json_util.create_from_json_dict(Course.Skill, { **s, 'id': i + 1, 'lang': self.lang })
            for i, s in enumerate(skills)
        ]

    def skill_by_id(self, i: int) -> 'Course.Skill':
        assert i >= 1
        return self.skills[i - 1]

    def skill_section_mapping(self) -> Dict[str, str]:
        return { s.name: s.section for s in self.skills}

    def word_count(self) -> int:
        count = 0
        for s in self.skills:
            count += len(s.words)
        return count
