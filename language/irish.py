from typing import (Any, Callable, Dict, Generator, Iterable, Iterator, List,
                    Optional, Sequence, Set, Tuple, Union)
import pprint
import abc
from enum import Enum
import enum
from collections import OrderedDict
import copy
import json_util
from uuid import UUID

from . import language
from .language import (
    Language,
    DictWord, DictWordForm, Inflection,
    Constituent,
    TranslationContext, Translation, TTranslationSource)

from translation import gettext as _

@enum.unique
class Number(language.GrammaticalCategory):
    SINGULAR    = (0, _("Singular"), "sg")
    PLURAL      = (1, _("Plural"), "pl")

@enum.unique
class Person(language.GrammaticalCategory):
    IMPERSONAL  = (0, _("Impersonal"), "sb")
    FIRST       = (1, _("First"), "1")
    SECOND      = (2, _("Second"), "2")
    THIRD       = (3, _("Third"), "3")

@enum.unique
class Tense(language.GrammaticalCategory):
    PRESENT     = (0, _("Present"), "pres")
    PAST        = (1, _("Past"), "past")
    FUTURE      = (2, _("Future"), "fut")
    CONDITIONAL = (3, _("Conditional"), "cond")
    HAB_PRESENT = (4, _("Habitual Present"), "hab-pres")
    HAB_PAST    = (5, _("Habitual Past"), "hab-past")
    IMPERATIVE  = (6, _("Imperative"),  "imp")

@enum.unique
class Gender(language.GrammaticalCategory):
    MASC        = (0, _("masculine"), "m")
    FEM         = (1, _("feminine"), "f")

@enum.unique
class Case(language.GrammaticalCategory):
    NOMINATIVE  = (0, _("Nominative"), "nom")
    GENITIVE    = (1, _("Genitive"), "gen")
    DATIVE      = (2, _("Dative"), "dat")

@enum.unique
class Definiteness(language.GrammaticalCategory):
    # Bare and indefinite are the same in Irish, but provide both for
    # convenience (can make the same concept definition work for multiple
    # languages)
    BARE        = (1, _("Bare"), "bare")
    INDEFINITE  = (2, _("Indefinite"), "indef")
    DEFINITE    = (3, _("Definite"), "def")

@enum.unique
class Dialect(language.Dialect):
    TEILEANN    = (0, 'Teileann', 'T')
    ULSTER_CO   = (1, _('Ulster (standard)'), 'u')
    CONNACHT_CO = (2, _('Connacht (standard)'), 'c')
    MUNSTER_CO  = (3, _('Munster (standard)'), 'm')
    ULSTER_D    = (4, _('Ulster (dialect)'), 'U', [TEILEANN], True)
    CONNACHT_D  = (5, _('Connacht (dialect)'), 'C')
    MUNSTER_D   = (6, _('Munster (dialect)'), 'M')
    ULSTER      = (7, 'Ulster', 'û', [ULSTER_CO, ULSTER_D])
    CONNACHT    = (8, 'Connacht', 'ĉ', [CONNACHT_CO, CONNACHT_D])
    MUNSTER     = (9, 'Munster', 'µ', [MUNSTER_CO, MUNSTER_D])

class IrishAgreementInfo(Translation.AgreementInfo):
    def __init__(self, person: Person, number: Number,
                 gender: Optional[Gender]) -> None:
        self.person = person
        self.number = number
        self.gender = gender

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, IrishAgreementInfo):
            return False
        if (self.person != other.person) or (self.number != other.number):
            return False
        if self.person == Person.THIRD and self.number == Number.SINGULAR:
            return self.gender == other.gender
        else:
            return True

def is_vowel(s: str) -> bool:
    return s in 'aáeéiíoóuú'

def count_syllables(word: str) -> int:
    count = 0
    was_vowel = False
    for letter in word:
        is_vowel = letter in 'aáeéiíoóuú'
        if not was_vowel and is_vowel:
            was_vowel = True
            count += 1
        elif was_vowel and not is_vowel:
            was_vowel = False
    return count

def has_slender_start(word: str) -> bool:
    for c in word:
        if c in 'aouáóú':
            return False
        elif c in 'eiéí':
            return True
    raise Exception("No vowel in the word '%s'" % word)

def has_slender_ending(word: str) -> bool:
    return has_slender_start(word[::-1])

def has_slender_cons_ending(word: str) -> bool:
    return not is_vowel(word[-1]) and has_slender_ending(word)

# TODO Deduplicate with German
def split_last_vowel_group(word: str) -> Tuple[str, str, str]:
    i = len(word) - 1
    while i >= 0 and not is_vowel(word[i]):
        i -= 1
    if i < 0:
        return ('', '', word)

    last_vowel = i

    while i >= 0 and is_vowel(word[i]):
        i -= 1

    first_vowel = i + 1

    return (word[:first_vowel], word[first_vowel:last_vowel+1], word[last_vowel+1:])


def palatalise(word: str) -> str:
    c1, v, c2 = split_last_vowel_group(word)

    if count_syllables(word) > 1:
        if word.endswith('each'):
            return c1 + 'igh'
        elif word.endswith('ach'):
            return c1 + 'aigh'
        elif word.endswith('íoch'):
            return c1 + 'ígh'

    palatalisation_map = {
        'ea': 'i',
        'éa': 'éi',
        'ia': 'éi',
        'io': 'i',
        'ío': 'í',
        'iu': 'i',
    }
    if v in palatalisation_map:
        return c1 + palatalisation_map[v] + c2

    if not v or has_slender_ending(v):
        return c1 + v + c2
    else:
        return c1 + v + "i" + c2

def depalatalise(word: str) -> str:
    c1, v, c2 = split_last_vowel_group(word)

    depalatalisation_map = {
        'ei': 'ea',
        'éi': 'éa',
        'i':  'ea',
        'io': 'ea',
        'í':  'ío',
        'ui': 'o',
    }
    if v in depalatalisation_map:
        return c1 + depalatalisation_map[v] + c2

    if v and has_slender_ending(v):
        if v[-1] == 'i' and not has_slender_start(v):
            v = v[:-1]

    return c1 + v + c2

def syncopate(word: str) -> str:
    if count_syllables(word) < 2:
        return word

    c1, v, c2 = split_last_vowel_group(word)
    if any(x in 'áéíóú' for x in v) or c2 not in ('l', 'n', 'r', 's'):
        return word
    elif c2 == 's' and c1[-1] not in 'lnr':
        return word
    else:
        return c1 + c2

def h_prefix(word: str) -> str:
    if len(word) and is_vowel(word[0]):
        return 'h' + word
    else:
        return word

def mutate_translations(translations: Generator[Translation, None, None],
                        fn: Callable[[str], str]
                        ) -> Generator[Translation, None, None]:
    for t in translations:
        yield t.update_text(fn(str(t)))

class IrishConstituent(Constituent):
    @staticmethod
    def lenite(word: str) -> str:
        if word[0] in ['b', 'c', 'd', 'f', 'g', 'm', 'p', 't']:
            if word[1] != 'h':
                return word[0] + 'h' + word[1:]
        elif word[0] == 's' and word[1] not in ['c', 'p', 't', 'm', 'f', 'h']:
            return word[0] + 'h' + word[1:]
        return word

    @staticmethod
    def eclipse(word: str) -> str:
        replacements = { 'b': 'm',
                         'c': 'g',
                         'd': 'n',
                         'f': 'bh',
                         'g': 'n',
                         'p': 'b',
                         't': 'd' }
        if word[0] in replacements:
            return replacements[word[0]] + word
        elif is_vowel(word[0]):
            return 'n-' + word
        else:
            return word

class IrishDictWord(DictWord):
    pass

class IrishVerb(IrishDictWord):

    name = _("verb")

    class IrishVerbForm(DictWordForm):
        def __init__(self, form: str, analytic: bool = False,
                     dialects: Sequence[Dialect] = [],
                     lenite: bool = False, past_particles: bool = True,
                     with_ending: Optional[bool] = None,
                     no_analytic_ending: bool = False,
                     tense_marker: Optional[str] = None,
                     use_tense_marker: bool = True) -> None:
            self.text = form
            self.analytic = analytic
            self.dialects = dialects or [d for d in Dialect]
            self.lenite = lenite
            self.with_ending = with_ending
            self.no_analytic_ending = no_analytic_ending
            self.past_particles = past_particles
            self.tense_marker = tense_marker
            self.use_tense_marker = use_tense_marker

        @classmethod
        def add_ending(cls, word: str, ending: str, tense_marker: Optional[str]) -> str:
            if not ending:
                return word

            if ending[0] == '%':
                ending = ending[1:]
                if word.endswith('igh'):
                    word = word[:-3]

            if tense_marker:
                ending = tense_marker + ending

            # -t/-th + -t- leaves a single -t-
            if ending[0] == 't':
                if word[-1] == "t":
                    word = word[:-1]
                elif word[-2:] == 'th':
                    word = word[:-2]

            # -bh/-mb + -th- results in -f-
            if ending[0:2] == 'th' and (word[-2:] == 'mh' or word[-2:] == 'bh'):
                word = word[:-2]
                ending = 'f' + ending[2:]

            word_slender = has_slender_ending(word)
            ending_slender = has_slender_start(ending)

            if ending[0] in 'aeiouáéíóú':
                if word[-1] in 'aeiouáéíóú':
                    if ending[0] in 'áéíóú' and word_slender == ending_slender:
                        word = word[:-1]
                    elif word_slender == ending_slender and word[-1] not in 'eé':
                        ending = ending[1:]
                    elif word[-1] in 'ií' and ending[0] == "a":
                        ending = "o" + ending[1:]
                elif word_slender != ending_slender:
                    # e + ó = eo
                    if ending[0] == 'ó':
                        ending = 'o' + ending[1:]
                    ending = ("a" if ending_slender else
                              "i" if ending[0] in "uú" else "e") + ending
            else:
                assert ending[1] in 'aeiouáéíóú'
                if word_slender != ending_slender:
                    ending = ending[0] + ("a" if ending_slender else "e") + ending[1:]
                if word[-1] in 'eé':
                    ending = "i" + ending

            return word + ending

        def add_stem(self, stem: 'IrishVerb.IrishVerbForm'
                    ) -> Optional['IrishVerb.IrishVerbForm']:
            if stem.with_ending is not None:
                if (self.text != "") != stem.with_ending:
                    return None

            form = copy.copy(self)
            if self.analytic and stem.no_analytic_ending:
                form.text = stem.text
            else:
                form.text = self.add_ending(stem.text, self.text,
                                            self.tense_marker if stem.use_tense_marker else None)
            form.past_particles = stem.past_particles and self.past_particles
            form.dialects = list(set(stem.dialects).intersection(set(self.dialects)))
            if not form.dialects:
                return None
            return form

    munster = [ Dialect.MUNSTER_D, Dialect.MUNSTER_CO ]
    connacht = [ Dialect.CONNACHT_D, Dialect.CONNACHT_CO ]
    ulster = [ Dialect.ULSTER_D, Dialect.ULSTER_CO ]

    standard = [ Dialect.MUNSTER_CO, Dialect.CONNACHT_CO, Dialect.ULSTER_CO ]
    connacht_ulster = connacht + ulster

    endings = [ {
        "pres-1sg": [ IrishVerbForm("im", False, []) ],
        "pres-2sg": [ IrishVerbForm("ann", True, []) ],
        "pres-3sg": [ IrishVerbForm("ann", True, []) ],
        "pres-1pl": [ IrishVerbForm("ann", True, connacht_ulster),
                      IrishVerbForm("imid", False, [ Dialect.MUNSTER_CO ]),
                      IrishVerbForm("imíd", False, [ Dialect.MUNSTER_D ]) ],
        "pres-2pl": [ IrishVerbForm("ann", True, []) ],
        "pres-3pl": [ IrishVerbForm("ann", True, []),
                      IrishVerbForm("id", False, [ Dialect.MUNSTER_D ]),
                      IrishVerbForm("id", True, [ Dialect.MUNSTER_D ]) ],
        "pres-sb":  [ IrishVerbForm("tar", False, []) ],

        "past-1sg": [ IrishVerbForm("", True, connacht_ulster, True),
                      IrishVerbForm("as", False, munster, True) ],
        "past-2sg": [ IrishVerbForm("", True, connacht_ulster, True),
                      IrishVerbForm("is", False, munster, True) ],
        "past-3sg": [ IrishVerbForm("", True, [], True) ],
        "past-1pl": [ IrishVerbForm("", True, connacht_ulster, True),
                      IrishVerbForm("amar", False, [ Dialect.MUNSTER_CO ], True),
                      IrishVerbForm("amair", False, [ Dialect.MUNSTER_D ], True) ],
        "past-2pl": [ IrishVerbForm("", True, connacht_ulster + [ Dialect.MUNSTER_CO ], True),
                      IrishVerbForm("abhair", False, [ Dialect.MUNSTER_D], True) ],
        "past-3pl": [ IrishVerbForm("", True, connacht_ulster, True),
                      IrishVerbForm("adar", False, munster, True) ],
        "past-sb":  [ IrishVerbForm("adh", False, [])],

        "fut-1sg": [ IrishVerbForm("idh", True, connacht_ulster + [ Dialect.MUNSTER_CO ], tense_marker="f"),
                     IrishVerbForm("ad", False, [ Dialect.MUNSTER_D ], tense_marker="f") ],
        "fut-2sg": [ IrishVerbForm("idh", True, connacht_ulster + [ Dialect.MUNSTER_CO ], tense_marker="f"),
                     IrishVerbForm("ir", False, [ Dialect.MUNSTER_D ], tense_marker="f") ],
        "fut-3sg": [ IrishVerbForm("idh", True, [], tense_marker="f") ],
        "fut-1pl": [ IrishVerbForm("idh", True, connacht_ulster, tense_marker="f"),
                     IrishVerbForm("imid", False, [ Dialect.MUNSTER_CO ], tense_marker="f"),
                     IrishVerbForm("imíd", False, [ Dialect.MUNSTER_D ], tense_marker="f") ],
        "fut-2pl": [ IrishVerbForm("idh", True, [], tense_marker="f") ],
        "fut-3pl": [ IrishVerbForm("idh", True, connacht_ulster+ [ Dialect.MUNSTER_CO ], tense_marker="f"),
                     IrishVerbForm("id", False, [ Dialect.MUNSTER_D ], tense_marker="f"),
                     IrishVerbForm("id", True, [ Dialect.MUNSTER_D ], tense_marker="f") ],
        "fut-sb":  [ IrishVerbForm("far", False, [])],

        "cond-1sg": [ IrishVerbForm("inn", False, [], True, tense_marker="f"), ],
        "cond-2sg": [ IrishVerbForm("fá", False, [], True), ],
        "cond-3sg": [ IrishVerbForm("adh", True, [], True, tense_marker="f") ],
        "cond-1pl": [ IrishVerbForm("adh", True, connacht_ulster, True, tense_marker="f"),
                      IrishVerbForm("as", True, [ Dialect.ULSTER_D ], True, tense_marker="f"),
                      IrishVerbForm("imis", False, [ Dialect.MUNSTER_CO ], True, tense_marker="f"),
                      IrishVerbForm("imíst", False, [ Dialect.MUNSTER_D ], True, tense_marker="f") ],
        "cond-2pl": [ IrishVerbForm("adh", True, [], True, tense_marker="f") ],
        "cond-3pl": [ IrishVerbForm("adh", True, [ Dialect.CONNACHT_D, Dialect.ULSTER_D ], True, tense_marker="f"),
                      IrishVerbForm("idís", False, [ Dialect.MUNSTER_CO, Dialect.CONNACHT_CO, Dialect.ULSTER_CO ], True, tense_marker="f"),
                      IrishVerbForm("idíst", False, [ Dialect.MUNSTER_D ], True, tense_marker="f") ],
        "cond-sb":  [ IrishVerbForm("fí", False, [], True)],

        "hab-past-1sg": [ IrishVerbForm("inn", False, [], True), ],
        "hab-past-2sg": [ IrishVerbForm("tá", False, [], True), ],
        "hab-past-3sg": [ IrishVerbForm("adh", True, [], True) ],
        "hab-past-1pl": [ IrishVerbForm("adh", True, connacht_ulster, True),
                          IrishVerbForm("imis", False, [ Dialect.MUNSTER_CO, Dialect.ULSTER_CO, Dialect.ULSTER_D ], True),
                          IrishVerbForm("amais", False, [ Dialect.ULSTER_D ], True),
                          IrishVerbForm("imist", False, [ Dialect.ULSTER_D ], True),
                          IrishVerbForm("imíst", False, [ Dialect.MUNSTER_D ], True) ],
        "hab-past-2pl": [ IrishVerbForm("adh", True, [], True) ],
        "hab-past-3pl": [ IrishVerbForm("adh", True, [ Dialect.CONNACHT_D, Dialect.ULSTER_D ], True),
                          IrishVerbForm("idís", False, [ Dialect.MUNSTER_CO, Dialect.CONNACHT_CO, Dialect.ULSTER_CO ], True),
                          IrishVerbForm("idíst", False, [ Dialect.MUNSTER_D ], True) ],
        "hab-past-sb":  [ IrishVerbForm("tí", False, [], True)],

        "imp-1sg": [ IrishVerbForm("im", False, []), ],
        "imp-2sg": [ IrishVerbForm("", False, []), ],
        "imp-3sg": [ IrishVerbForm("adh", True, []) ],
        "imp-1pl": [ IrishVerbForm("imis", False, []) ],
        "imp-2pl": [ IrishVerbForm("igí", False, []) ],
        "imp-3pl": [ IrishVerbForm("idís", False, []) ],
        "imp-sb":  [ IrishVerbForm("tar", False, [])],
    }, {
        "pres-1sg": [ IrishVerbForm("%ím", False, []) ],
        "pres-2sg": [ IrishVerbForm("%íonn", True, []) ],
        "pres-3sg": [ IrishVerbForm("%íonn", True, []) ],
        "pres-1pl": [ IrishVerbForm("%íonn", True, connacht_ulster),
                      IrishVerbForm("%ímid", False, [ Dialect.MUNSTER_CO ]),
                      IrishVerbForm("%ímíd", False, [ Dialect.MUNSTER_D ]) ],
        "pres-2pl": [ IrishVerbForm("%íonn", True, []) ],
        "pres-3pl": [ IrishVerbForm("%íonn", True, []),
                      IrishVerbForm("%íd", False, [ Dialect.MUNSTER_D ]),
                      IrishVerbForm("%íd", True, [ Dialect.MUNSTER_D ]) ],
        "pres-sb":  [ IrishVerbForm("%ítear", False, []) ],

        "past-1sg": [ IrishVerbForm("", True, connacht_ulster, True),
                      IrishVerbForm("%íos", False, munster, True) ],
        "past-2sg": [ IrishVerbForm("", True, connacht_ulster, True),
                      IrishVerbForm("%ís", False, munster, True) ],
        "past-3sg": [ IrishVerbForm("", True, [], True) ],
        "past-1pl": [ IrishVerbForm("", True, connacht_ulster, True),
                      IrishVerbForm("%íomar", False, [ Dialect.MUNSTER_CO ], True),
                      IrishVerbForm("%íomair", False, [ Dialect.MUNSTER_D ], True) ],
        "past-2pl": [ IrishVerbForm("", True, connacht_ulster + [ Dialect.MUNSTER_CO ], True),
                      IrishVerbForm("%íobhair", False, [ Dialect.MUNSTER_D], True) ],
        "past-3pl": [ IrishVerbForm("", True, connacht_ulster, True),
                      IrishVerbForm("%íodar", False, munster, True) ],
        "past-sb":  [ IrishVerbForm("%íodh", False, [])],

        "fut-1sg": [ IrishVerbForm("%óidh", True, standard + [ Dialect.CONNACHT_D ]),
                     IrishVerbForm("%óchaidh", True, [ Dialect.ULSTER_D ]),
                     IrishVerbForm("%ód", False, [ Dialect.MUNSTER_D ]) ],
        "fut-2sg": [ IrishVerbForm("%óidh", True, standard + [ Dialect.CONNACHT_D ]),
                     IrishVerbForm("%óchaidh", True, [ Dialect.ULSTER_D ]),
                     IrishVerbForm("%óir", False, [ Dialect.MUNSTER_D ]) ],
        "fut-3sg": [ IrishVerbForm("%óidh", True, connacht + munster + [ Dialect.ULSTER_CO ]),
                     IrishVerbForm("%óchaidh", True, [ Dialect.ULSTER_D ]) ],
        "fut-1pl": [ IrishVerbForm("%óidh", True, connacht + [ Dialect.ULSTER_CO ]),
                     IrishVerbForm("%óchaidh", True, [ Dialect.ULSTER_D ]),
                     IrishVerbForm("%óimid", False, [ Dialect.MUNSTER_CO ]),
                     IrishVerbForm("%óimíd", False, [ Dialect.MUNSTER_D ]) ],
        "fut-2pl": [ IrishVerbForm("%óidh", True, standard + [ Dialect.ULSTER_CO ]),
                     IrishVerbForm("%óchaidh", True, [ Dialect.ULSTER_D ]) ],
        "fut-3pl": [ IrishVerbForm("%óidh", True, standard + [ Dialect.CONNACHT_D ]),
                     IrishVerbForm("%óchaidh", True, [ Dialect.ULSTER_D ]),
                     IrishVerbForm("%óid", False, [ Dialect.MUNSTER_D ]),
                     IrishVerbForm("%óid", True, [ Dialect.MUNSTER_D ]) ],
        "fut-sb":  [ IrishVerbForm("%ófar", False, connacht + munster + [ Dialect.ULSTER_CO ]),
                     IrishVerbForm("%óchar", False, [ Dialect.ULSTER_D ])],

        "cond-1sg": [ IrishVerbForm("%óinn", False, standard + [ Dialect.CONNACHT_D ], True),
                      IrishVerbForm("%óchainn", False, [ Dialect.ULSTER_D ], True), ],
        "cond-2sg": [ IrishVerbForm("%ófá", False, standard + [ Dialect.CONNACHT_D ], True),
                      IrishVerbForm("%óchá", False, [ Dialect.ULSTER_D ], True), ],
        "cond-3sg": [ IrishVerbForm("%ódh", True, standard + [ Dialect.CONNACHT_D ], True),
                      IrishVerbForm("%óchadh", True, [ Dialect.ULSTER_D ], True), ],
        "cond-1pl": [ IrishVerbForm("%ódh", True, connacht + [ Dialect.ULSTER_CO ], True),
                      IrishVerbForm("%óchadh", True, [ Dialect.ULSTER_D ], True),
                      IrishVerbForm("%óimis", False, [ Dialect.MUNSTER_CO ], True),
                      IrishVerbForm("%óimíst", False, [ Dialect.MUNSTER_D ], True) ],
        "cond-2pl": [ IrishVerbForm("%ódh", True, standard + [ Dialect.CONNACHT_D ], True),
                      IrishVerbForm("%óchadh", True, [ Dialect.ULSTER_D ], True), ],
        "cond-3pl": [ IrishVerbForm("%ódh", True, [ Dialect.CONNACHT_D ], True),
                      IrishVerbForm("%óchadh", True, [ Dialect.ULSTER_D ], True),
                      IrishVerbForm("%óidís", False, standard, True),
                      IrishVerbForm("%óidíst", False, [ Dialect.MUNSTER_D ], True) ],
        "cond-sb":  [ IrishVerbForm("%ófaí", False, [], True),
                      IrishVerbForm("%óchaí", False, [ Dialect.ULSTER_D ], True) ],

        "hab-past-1sg": [ IrishVerbForm("%ínn", False, [], True), ],
        "hab-past-2sg": [ IrishVerbForm("%iteá", False, [], True), ],
        "hab-past-3sg": [ IrishVerbForm("%íodh", True, [], True) ],
        "hab-past-1pl": [ IrishVerbForm("%íodh", True, connacht_ulster, True),
                          IrishVerbForm("%ímis", False, [ Dialect.MUNSTER_CO, Dialect.ULSTER_CO, Dialect.ULSTER_D ], True),
                          IrishVerbForm("%íomais", False, [ Dialect.ULSTER_D ], True),
                          IrishVerbForm("%ímist", False, [ Dialect.ULSTER_D ], True),
                          IrishVerbForm("%ímíst", False, [ Dialect.MUNSTER_D ], True) ],
        "hab-past-2pl": [ IrishVerbForm("%íodh", True, [], True) ],
        "hab-past-3pl": [ IrishVerbForm("%íodh", True, [ Dialect.CONNACHT_D, Dialect.ULSTER_D ], True),
                          IrishVerbForm("%ídís", False, [ Dialect.MUNSTER_CO, Dialect.CONNACHT_CO, Dialect.ULSTER_CO ], True),
                          IrishVerbForm("%ídíst", False, [ Dialect.MUNSTER_D ], True) ],
        "hab-past-sb":  [ IrishVerbForm("%ítí", False, [], True)],

        "imp-1sg": [ IrishVerbForm("%ím", False, []), ],
        "imp-2sg": [ IrishVerbForm("", False, []), ],
        "imp-3sg": [ IrishVerbForm("%íodh", True, []) ],
        "imp-1pl": [ IrishVerbForm("%ímis", False, []) ],
        "imp-2pl": [ IrishVerbForm("%ígí", False, []) ],
        "imp-3pl": [ IrishVerbForm("%ídís", False, []) ],
        "imp-sb":  [ IrishVerbForm("%ítear", False, [])],
    } ]

    def __init__(self, root: Any, **kwargs: Any) -> None:
        self.stems: Dict[Tense, Sequence[IrishVerb.IrishVerbForm]] = {}
        self.dep_stems: Dict[Tense, Sequence[IrishVerb.IrishVerbForm]] = {}
        self.ni_dep_stems: Optional[Sequence[IrishVerb.IrishVerbForm]] = None

        self.root = self.IrishVerbForm.create(root)

        # FIXME Different stems can have different conjugations
        if "conjugation" in kwargs:
            conj = kwargs.pop("conjugation")
            if isinstance(conj, int):
                self.conjugation: int = conj - 1
            else:
                raise Exception(_("Invalid value for '%s'") % "conjugation")
        else:
            self.conjugation = self.guess_conjugation()

        self.stem = self.IrishVerbForm.create(kwargs.pop("stem", self.guess_stem()))
        self.verbal_noun = self.IrishVerbForm.create(kwargs.pop("vn", self.guess_verbal_noun()))

        self.init_stem(Tense.PRESENT, kwargs.pop("present", self.stem),
                                      kwargs.pop("present-dep", None),
                                      kwargs.pop("ní+present-dep", None))
        self.init_stem(Tense.PAST, kwargs.pop("past", self.stem),
                                   kwargs.pop("past-dep", None))
        self.init_stem(Tense.FUTURE, kwargs.pop("fut", self.stem),
                                     kwargs.pop("fut-dep", None))
        self.init_stem(Tense.CONDITIONAL, kwargs.pop("cond", self.stems[Tense.FUTURE]),
                                          kwargs.pop("cond-dep", self.dep_stems[Tense.FUTURE]))
        self.init_stem(Tense.HAB_PAST, kwargs.pop("hab-past", self.stem),
                                       kwargs.pop("hab-past-dep", None))
        self.init_stem(Tense.IMPERATIVE, kwargs.pop("imp", self.stem),
                                         kwargs.pop("imp-dep", None))

        self.inflected_forms: Dict[str, List['IrishVerb.IrishVerbForm']] = {}
        self.dep_inflected_forms: Dict[str, List['IrishVerb.IrishVerbForm']] = {}
        self.ni_dep_inflected_forms: Dict[str, List['IrishVerb.IrishVerbForm']] = {}

        for t in Tense:
            for n in Number:
                for p in Person:
                    if p == Person.IMPERSONAL and n != Number.SINGULAR:
                        continue
                    id = self.get_verb_form_id(t, p, n)
                    self.init_inflected_form(self.inflected_forms, id, id, kwargs)
                    self.init_inflected_form(self.dep_inflected_forms, "%s-dep" % id, id, kwargs)
                    self.init_inflected_form(self.ni_dep_inflected_forms, "ní+%s-dep" % id, id, kwargs)

        if len(kwargs):
            raise json_util.JSONParseException(
                _("Invalid key: '%s'") % next(iter(kwargs.keys())))

    def init_stem(self, tense: Tense, indep: Any, dep: Any, ni_dep: Any = None) -> None:
        self.stems[tense] = self.IrishVerbForm.create(indep)

        if dep:
            self.dep_stems[tense] = self.IrishVerbForm.create(dep)
        else:
            self.dep_stems[tense] = self.stems[tense]

        if ni_dep:
            assert tense == Tense.PRESENT
            self.ni_dep_stems = self.IrishVerbForm.create(ni_dep)

    def init_inflected_form(self, forms: Dict[str, List['IrishVerb.IrishVerbForm']],
                            srcid: str, dstid: str, kwargs: Any) -> None:
        if srcid in kwargs:
            forms[dstid] = []
            form: Any = kwargs.pop(srcid)
            if not isinstance(form, list):
                form = [ form ]
            for f in form:
                forms[dstid] += self.IrishVerbForm.create(f)

    def guess_stem(self) -> Sequence['IrishVerb.IrishVerbForm']:
        result: List['IrishVerb.IrishVerbForm'] = []
        for s in self.root:
            s = copy.copy(s)
            result.append(s)
            if ((s.text[-2:] in [ 'il', 'in', 'ir' ] and not is_vowel(s.text[-3])) or
                (s.text[-2:] ==  'is' and s.text[-3] in 'lnr')):
                s.text = s.text[:-2] + s.text[-1]
                if self.conjugation == 1:
                    s.text += 'igh' if has_slender_ending(s.text) else 'aigh'
            elif ((s.text[-3:] in [ 'ail', 'ain', 'air' ] and not is_vowel(s.text[-4])) or
                  (s.text[-3:] ==  'ais' and s.text[-4] in 'lnr')):
                s.text = s.text[:-3] + s.text[-1]
                if self.conjugation == 1:
                    s.text += 'igh' if has_slender_ending(s.text) else 'aigh'
            elif s.text[-3:] == 'áil':
                s.text = s.text[:-3] + 'ál'
            elif s.text[-3:] == 'igh' and s.text[-4] in 'áéóú':
                ulster = copy.copy(s)
                ulster.dialects = [ Dialect.ULSTER_D ]
                result.append(ulster)
                s.text = s.text[:-3]
        return result

    def guess_conjugation(self) -> int:
        root = self.root[0].text
        if count_syllables(root) == 1:
            return 0
        if root.endswith("áil"):
            return 0
        return 1

    def guess_verbal_noun(self) -> Sequence['IrishVerb.IrishVerbForm']:
        result: List['IrishVerb.IrishVerbForm'] = []
        for s in self.root:
            s = copy.copy(s)
            result.append(s)
            if ((s.text[-2:] in [ 'il', 'in', 'ir' ] and not is_vowel(s.text[-3])) or
                (s.text[-2:] ==  'is' and s.text[-3] in 'lnr')):
                s.text += 't'
            elif ((s.text[-3:] in [ 'ail', 'ain', 'air' ] and not is_vowel(s.text[-4])) or
                  (s.text[-3:] ==  'is' and s.text[-4] in 'lnr')):
                s.text += 't'
            elif s.text[-3:] == 'áil':
                pass
            elif s.text[-3:] == 'igh' and s.text[-4] in 'áéóú':
                s.text = s.text[:-3]
            elif self.conjugation == 0:
                s.text = s.add_ending(s.text, 'adh', None)
            else:
                assert self.conjugation == 1
                if s.text[-3:] == 'igh':
                    s.text = s.add_ending(s.text, '%ú', None)
                else:
                    pass
        return result

    def get_verb_form_id(self, tense: Tense, person: Person, number: Number) -> str:
        if person == Person.IMPERSONAL:
            p = "sb"
        else:
            p = "%d%s" % (person.value, number.get_id())

        return "%s-%s" % (tense.get_id(), p)

    def get_forms(self) -> Set[str]:
        forms = set(x.text for x in self.verbal_noun)

        for t in [ Tense.PRESENT, Tense.PAST, Tense.FUTURE, Tense.CONDITIONAL,
                   Tense.HAB_PAST, Tense.IMPERATIVE ]:
            for p in Person:
                if p == Person.IMPERSONAL:
                    for tr, synthetic in self.conjugate(t, p, Number.SINGULAR):
                        forms.add(str(tr))
                else:
                    for n in Number:
                        for tr, synthetic in self.conjugate(t, p, n):
                            forms.add(str(tr))
                        for tr, synthetic in self.conjugate(t, p, n, dependent=True):
                            forms.add(str(tr))

        return forms

    def dictionary_forms(self) -> List[Tuple[str, Union[str, Translation]]]:
        # TODO Pick the most standard form instead of the first one
        return [
            (_("Root"), self.root[0].text),
            (_("Present"), next(self.conjugate(Tense.PRESENT, Person.THIRD,
                                               Number.SINGULAR))[0]),
            (_("Future"), next(self.conjugate(Tense.FUTURE, Person.THIRD,
                                              Number.SINGULAR))[0]),
            (_("Past"), next(self.conjugate(Tense.PAST, Person.THIRD,
                                            Number.SINGULAR))[0]),
        ]

    def add_particle(self, f: IrishVerbForm, form: str, tense: Tense, particle: str) -> str:
        past = {
            'an': 'ar',
            'go': 'gur',
            'ní': 'níor',
        }
        lenition_after = [ 'a', 'ar', 'ní', 'níor', 'gur' ]
        eclipsis_after = [ 'an', 'go' ]

        if tense == Tense.PAST and particle in past and f.past_particles:
            particle = past[particle]

        if particle in lenition_after:
            form = IrishConstituent.lenite(form)
        if particle in eclipsis_after:
            form = IrishConstituent.eclipse(form)

        return "%s %s" % (particle, form)

    def inflect_form(self, tense: Tense, person: Person, number: Number) -> Any:
        pronoun = person != Person.IMPERSONAL
        indep = [x[0] for x in self.conjugate(tense, person, number, add_pronoun=pronoun)]
        dep = [x[0] for x in self.conjugate(tense, person, number, add_pronoun=pronoun, dependent=True)]

        if indep == dep:
            return indep
        else:
            neg = [x[0] for x in self.conjugate(tense, person, number, add_pronoun=pronoun, dependent=True, particle="ní")]
            quest = [x[0] for x in self.conjugate(tense, person, number, add_pronoun=pronoun, dependent=True, particle="an")]
            return (indep, neg, quest)

    def inflection(self) -> Dict[str, Any]:
        info: Dict[str, Any] = {
            _("General"): {
                _("Lemma"): [ Translation(irish, x.text, dialects=x.dialects)
                              for x in self.root ],
                _("Inflection"): _("First conjugation") if self.conjugation == 0 else _("Second conjugation"),
                _("Verbal noun"): [ Translation(irish, x.text, dialects=x.dialects)
                                    for x in self.verbal_noun ],
            }
        }

        for t in [ Tense.PRESENT, Tense.PAST, Tense.FUTURE, Tense.CONDITIONAL,
                   Tense.HAB_PAST, Tense.IMPERATIVE ]:
            info[t.get_name()] = {
                **{
                    n.get_name(): {
                        p.get_name(): self.inflect_form(t, p, n)
                        for p in Person if p != Person.IMPERSONAL
                    }
                    for n in Number
                },
                Person.IMPERSONAL.get_name(): self.inflect_form(t, Person.IMPERSONAL, Number.SINGULAR)
            }

        return info

    def conjugate(self, tense: Tense, person: Person, number: Number,
                  add_pronoun: bool = False, dependent: bool = False,
                  analytic: bool = False,
                  particle: Optional[str] = None) -> Generator[Tuple[Translation, bool], None, None]:
        id = self.get_verb_form_id(tense, person, number)

        if person == Person.IMPERSONAL:
            analytic = False

        if particle == 'ní' and id in self.ni_dep_inflected_forms and tense == Tense.PRESENT:
            inflected_forms = self.ni_dep_inflected_forms
            particle = None
        else:
            inflected_forms = self.dep_inflected_forms if dependent else self.inflected_forms

        if id in inflected_forms:
            forms = inflected_forms[id]
        else:
            endings = self.endings[self.conjugation][id]
            if particle == 'ní' and self.ni_dep_stems and tense == Tense.PRESENT:
                stems = self.ni_dep_stems
                particle = None
            else:
                cur_stems = self.dep_stems if dependent else self.stems
                stems = cur_stems.get(tense) or self.root
            forms = []
            for ending in endings:
                for stem in stems:
                    cur_form = ending.add_stem(stem)
                    if cur_form:
                        if cur_form.text == self.stem[0].text:
                            cur_form.text = self.root[0].text
                        forms.append(cur_form)

        for f in forms:
            form = f.text

            if analytic and not f.analytic:
                continue

            if particle:
                f.lenite = False

            if f.lenite:
                form = IrishConstituent.lenite(form)

            if f.lenite and tense in [ Tense.PAST, Tense.CONDITIONAL, Tense.HAB_PAST ]:
                if form[0] in 'aeiouáéíóú' or (form[0] == 'f' and
                                               form[2] in 'aeiouáéíóúlr'):
                    form = "d'" + form
                # TODO Prefix 'do' for Munster

            if particle:
                form = self.add_particle(f, form, tense, particle)

            if f.analytic and add_pronoun:
                form += " %s" % (IrishPronounNP.get_form(person, number))

            yield (Translation(irish, form, word=self, dialects=f.dialects,
                               dialect_feature_name=_("The form '%s'") % form,
                               word_grammar_tags=["verb-%s" % id]),
                   not f.analytic)

    def vn(self, particle: Optional[str] = None) -> Generator[Tuple[Translation, bool], None, None]:
        for f in self.verbal_noun:
            form = f.text

            if particle:
                form = self.add_particle(f, form, Tense.PRESENT, particle)

            yield (Translation(irish, form, word=self, dialects=f.dialects,
                               dialect_feature_name=_("The form '%s'") % form,
                               word_grammar_tags=["verb-vn"]),
                   False)

class IrishNoun(IrishDictWord):

    name = _("noun")

    class IrishNounForm(DictWordForm):
        def __init__(self, form: str,  genders: Sequence[Gender] = (),
                     dialects: Sequence[Dialect] = []) -> None:
            self.text = form
            self.genders = genders
            self.dialects = dialects or [d for d in Dialect]

    def guess_genders(self) -> None:
        for f in self.forms['nom-sg']:
            if not f.genders:
                if has_slender_cons_ending(f.text):
                    f.genders = (Gender.FEM,)
                else:
                    f.genders = (Gender.MASC,)

    def __init__(self, dialects: Sequence[Dialect] = (), **kwargs: Any) -> None:
        self.forms: Dict[str, Sequence[IrishNoun.IrishNounForm]] = {}

        for id in [ 'nom-sg', 'gen-sg', 'nom-pl', 'gen-pl' ]:
            self.forms[id] = self.IrishNounForm.create(kwargs.pop(id))

        self.guess_genders()
        self.genders = set(g for f in self.forms['nom-sg'] for g in f.genders)

        self.dialects = dialects

    def get_forms(self) -> Set[str]:
        return { str(t)
                 for c in Case
                 for n in Number
                 for _, t in self.decline(c, n) }

    def dictionary_forms(self) -> List[Tuple[str, Union[str, Translation]]]:
        # TODO Pick the most standard form instead of the first one
        return [
            ("%s %s" % (_("Nominative"), _("Singular")),
             next(self.decline(Case.NOMINATIVE, Number.SINGULAR))[1]),
            ("%s %s" % (_("Genitive"), _("Singular")),
             next(self.decline(Case.GENITIVE, Number.SINGULAR))[1]),
            ("%s %s" % (_("Nominative"), _("Plural")),
             next(self.decline(Case.NOMINATIVE, Number.PLURAL))[1]),
        ]

    def inflection(self) -> Dict[str, Any]:
        declension: Dict[str, Any] = {}
        for n in (Number.SINGULAR, Number.PLURAL):
            declension[n.get_name()] = {
                c.get_name(): list(t for _, t in self.decline(c, n))
                for c in (Case.NOMINATIVE, Case.GENITIVE)
            }

        return {
            _("Declension"): declension
        }

    def get_noun_form_id(self, case: Case, number: Number) -> str:
        if case == Case.DATIVE:
            case = Case.NOMINATIVE
        return "%s-%s" % (case.get_id(), number.get_id())

    def decline(self, case: Case, number: Number
               ) -> Generator[Tuple['IrishNoun.IrishNounForm',Translation], None, None]:
        id = self.get_noun_form_id(case, number)

        for f in self.forms[id]:
            result = f.text
            t = Translation(irish, result, word=self, dialects=self.dialects,
                            dialect_feature_name=_("The word '%s'") % result)
            if 0 < len(f.genders) < len(self.genders):
                t.add_dialect_constraint(f.dialects,
                                         _("The form '%s' (%s)")
                                         % (result, ", ".join(g.get_name()
                                            for g in f.genders)))
            else:
                t.add_dialect_constraint(f.dialects, _("The form '%s'") % result)
            yield f, t

class IrishAdjective(IrishDictWord):

    name = _("adjective")

    class IrishAdjectiveForm(DictWordForm):
        def __init__(self, form: str, dialects: Sequence[Dialect] = []) -> None:
            self.text = form
            self.declension = -1
            self.dialects = dialects or [d for d in Dialect]

        def guess_declension(self) -> int:
            if self.text.endswith(('úil', 'ir')):
                return 2
            elif is_vowel(self.text[-1]):
                return 3
            else:
                return 1

    def get_adj_form_id(self, case: Case, number: Number, gender: Gender) -> str:
        return "%s-%s-%s" % (case.get_id(), number.get_id(), gender.get_id())

    def guess_form(self, case: Case, number: Number,
                   gender: Gender) -> Sequence[IrishAdjectiveForm]:
        result: List['IrishAdjective.IrishAdjectiveForm'] = []
        for r in self.root:
            if len(r.text) == 1 and r.text in 'XYZ':
                form = r.text
            elif case == Case.GENITIVE and number == Number.SINGULAR:
                if r.declension == 1:
                    form = syncopate(r.text) if gender == Gender.FEM else r.text
                    if not has_slender_ending(form):
                        form = palatalise(form)
                    if gender == Gender.FEM:
                        form += "e"
                elif r.declension == 2:
                    if gender == Gender.MASC:
                        form = r.text
                    else:
                        form = depalatalise(syncopate(r.text)) + "a"
                else:
                    assert r.declension == 3
                    form = r.text
                if form.endswith('ighe'):
                    form = form[:-4] + 'í'
            elif case == Case.NOMINATIVE and number == Number.PLURAL:
                if r.declension == 1:
                    form = syncopate(r.text)
                    form += ("e" if has_slender_ending(form) else "a")
                elif r.declension == 2:
                    form = depalatalise(syncopate(r.text)) + "a"
                else:
                    assert r.declension == 3
                    form = r.text
            else:
                form = r.text
            result.append(self.IrishAdjectiveForm(form, r.dialects))
        return result

    def __init__(self, dialects: Sequence[Dialect] = (), **kwargs: Any) -> None:
        self.forms: Dict[str, Sequence[IrishAdjective.IrishAdjectiveForm]] = {}

        self.root = self.IrishAdjectiveForm.create(kwargs.pop("root"))
        self.dialects = dialects

        declension = kwargs.pop('declension', None)
        for r in self.root:
            if declension in (1, 2, 3):
                r.declension = declension
            elif declension is None:
                r.declension = r.guess_declension()
            else:
                raise Exception(_("Invalid value for '%s'") % "declension")

        for c in Case:
            for n in Number:
                if c == Case.GENITIVE and n == Number.PLURAL:
                    continue
                for g in Gender:
                    id = self.get_adj_form_id(c, n, g)
                    if id in kwargs:
                        forms = self.IrishAdjectiveForm.create(kwargs.pop(id))
                    else:
                        forms = self.guess_form(c, n, g)
                    self.forms[id] = forms

    def get_forms(self) -> Set[str]:
        return { str(t)
                 for c in Case
                 for n in Number
                 for g in Gender
                 for definite in (True, False)
                 for strong_pl in (True, False)
                 for slender_ending in (True, False)
                 for t in self.decline(c, n, g, definite, strong_pl,
                                       slender_ending) }

    def dictionary_forms(self) -> List[Tuple[str, Union[str, Translation]]]:
        return [
            ("%s %s" % (_("Nominative"), _("Singular")),
             next(self.decline(Case.NOMINATIVE, Number.SINGULAR, Gender.MASC,
                               False, False, False))),
            ("%s %s" % (_("Genitive"), _("Singular")),
             next(self.decline(Case.GENITIVE, Number.SINGULAR, Gender.MASC,
                               False, False, False))),
            ("%s %s" % (_("Nominative"), _("Plural")),
             next(self.decline(Case.NOMINATIVE, Number.PLURAL, Gender.MASC,
                               False, False, False))),
        ]

    def inflection(self) -> Dict[str, Any]:
        declension: Dict[str, Any] = {}
        for n in (Number.SINGULAR, Number.PLURAL):
            n_name = n.get_name()
            declension[n_name] = {}
            for c in (Case.NOMINATIVE, Case.GENITIVE):
                if n == Number.SINGULAR:
                    declension[n_name][c.get_name()] = {
                        g.get_name(): list(self.decline(c, n, g, False, False, False))
                        for g in Gender
                    }
                elif c == Case.NOMINATIVE:
                    declension[n_name][c.get_name()] = {
                        _("Normal"): list(self.decline(c, n, Gender.MASC,
                                                       False, False, False)),
                        _("After slender ending"): list(self.decline(c, n, Gender.MASC,
                                                                     False, False, True)),
                    }
                elif c == Case.GENITIVE:
                    declension[n_name][c.get_name()] = {
                        _("Weak plural"): list(self.decline(c, n, Gender.MASC,
                                                            False, False, False)),
                        _("Strong plural"): list(self.decline(c, n, Gender.MASC,
                                                              False, True, False)),
                    }

        return {
            _("General"): {
                _("Lemma"): [ Translation(irish, x.text, dialects=x.dialects)
                              for x in self.root ],
            },
            _("Declension"): declension
        }

    def decline(self, case: Case, number: Number, gender: Gender,
                definite: bool, strong_plural: bool, slender_ending: bool
                ) -> Generator[Translation, None, None]:

        id = self.get_adj_form_id(case, number, gender)
        if case == Case.DATIVE and number == Number.SINGULAR:
            grammar_tags = ["adj-%s-%s" % ("def" if definite  else "indef", id)]
        else:
            grammar_tags = ["adj-%s" % id]

        if case == Case.GENITIVE and number == Number.PLURAL:
            case = Case.NOMINATIVE
            if not strong_plural:
                number = Number.SINGULAR

        id = self.get_adj_form_id(case, number, gender)
        for f in self.forms[id]:
            result = f.text
            # TODO dative (only after article)
            # TODO plural after slender consonant
            if id in ('nom-sg-f', 'gen-sg-m'):
                result = IrishConstituent.lenite(result)
            if id.startswith('nom-pl-') and slender_ending:
                result = IrishConstituent.lenite(result)

            t = Translation(irish, result, word=self, dialects=self.dialects,
                            dialect_feature_name=_("The word '%s'") % f.text,
                            word_grammar_tags=grammar_tags)
            t.add_dialect_constraint(f.dialects, _("The form '%s'") % f.text)
            yield t

class IrishVP(IrishConstituent):
    class VPType(Enum):
        DICTIONARY = 0
        STATEMENT = 1
        QUESTION = 2
        RELATIVE = 3
        VN_A = 4        # Verbal noun with "a"
        VN_GEN = 5      # Verbal noun with genitive

        def is_sentence(self) -> bool:
            return self in (IrishVP.VPType.STATEMENT, IrishVP.VPType.QUESTION)

        def grammar_tag(self) -> str:
            return {
                IrishVP.VPType.DICTIONARY:  'vp-dict',
                IrishVP.VPType.STATEMENT:   'vp-decl',
                IrishVP.VPType.QUESTION:    'vp-question',
                IrishVP.VPType.RELATIVE:    'vp-relative',
                IrishVP.VPType.VN_A:        'vp-vn-a',
                IrishVP.VPType.VN_GEN:      'vp-vn-genitive',
            }[self]

    def __init__(self, ctx: TranslationContext,
                 verb: Union[str, UUID, None] = None,
                 neg: bool = False,
                 dialects: Optional[List[Dialect]] = None,
                 post: Optional[List[Any]] = None,
                 literal: Optional[str] = None,
                 vptype: Optional[VPType] = None,
                 subject: Optional[Any] = None,
                 object: Optional[Any] = None,
                 comp: Optional[Any] = None,
                 adverbial: Optional[List[Any]] = None) -> None:
        super().__init__(ctx, dialects, post)
        self.literal = literal
        # TODO Get an Irish dictionary entry and base things like whether an object is needed/allowed on that
        self.tense = Tense.PRESENT
        self.neg = neg

        if vptype is None:
            if subject:
                self.vptype = self.VPType.STATEMENT
            elif object:
                self.vptype = self.VPType.VN_A
            else:
                self.vptype = self.VPType.DICTIONARY
                self.tense = Tense.IMPERATIVE
        else:
            self.vptype = vptype

        self.verb = None
        self.subject: Union[IrishNP, IrishPronounNP, None] = None
        self.object = None
        self.comp = None

        if not literal:
            assert verb is not None
            self.verb = ctx.get_word(verb, IrishVerb)
            if subject:
                c = IrishConstituent.create(ctx, subject)
                assert isinstance(c, (IrishNP, IrishPronounNP))
                self.subject = c
                if isinstance(self.subject, IrishPronounNP):
                    self.subject.set_form(True)
            if object:
                self.object = IrishConstituent.create(ctx, object)
            if comp:
                if isinstance(comp, dict) and 'vptype' not in comp:
                    comp['vptype'] = self.VPType.VN_A
                c = IrishConstituent.create(ctx, comp)
                assert isinstance(c, IrishVP)
                self.comp = c

        self.adverbials: List[Constituent] = []
        for x in adverbial or ():
            self.adverbials.append(IrishConstituent.create(ctx, x))

    def get_number(self) -> Number:
        if self.subject:
            return self.subject.get_number()
        else:
            return Number.SINGULAR

    @classmethod
    def word_list(cls, d: Dict[str, Any]) -> List[str]:
        return [ d["verb"] ]

    def _word(self) -> str:
        assert self.verb
        return self.verb.root[0].text

    def _generate(self) -> Generator[Translation, None, None]:
        if self.literal:
            yield Translation(irish, self.literal)
            return

        assert self.verb

        subjects: Iterable[Optional[Translation]] = [None]
        subject_ref = None
        if self.subject:
            subject_ref = self.subject.get_antecedent()
            subjects = list(self.subject.generate())

        for subject in subjects:
            is_vn_construction = self.vptype in (self.VPType.VN_A, self.VPType.VN_GEN)
            analytic = False
            if self.vptype == self.VPType.DICTIONARY:
                p = Person.SECOND
                n = Number.SINGULAR
            elif not subject:
                assert is_vn_construction
                p = Person.IMPERSONAL
                n = Number.SINGULAR
            elif subject_ref:
                info = subject.agreement_info[subject_ref]
                assert isinstance(info, IrishAgreementInfo)
                (p, n) = (info.person, info.number)
            else:
                assert self.subject
                p = self.subject.get_person()
                n = self.subject.get_number()
                analytic = not isinstance(self.subject, IrishPronounNP)

            sentence_grammar_tags = [self.vptype.grammar_tag()]

            particle = None
            if self.neg:
                particle = 'ní'
                sentence_grammar_tags.append("vp-neg")

            if not is_vn_construction:
                verbs = self.verb.conjugate(self.tense, p, n, particle=particle,
                                            analytic=analytic)
            else:
                particle = "a" if subject or self.object else None
                verbs = self.verb.vn(particle=particle)

            for verb, synthetic in verbs:
                t: List[TTranslationSource] = []

                if self.vptype != self.VPType.VN_A:
                    t.append(verb)

                if not synthetic and subject:
                    t.append(subject)
                elif subject_ref:
                    agreement = IrishAgreementInfo(p, n, None)
                    verb.add_agreement_constraint(subject_ref, agreement)

                if self.object:
                    t.append(self.object.generate())

                if self.vptype == self.VPType.VN_A:
                    t.append(verb)

                for adv in self.adverbials:
                    t.append(adv.generate())

                if self.comp:
                    t.append(self.comp.generate())

                if self.vptype == self.VPType.STATEMENT:
                    t.append(Translation(irish, ".", contract_with_previous=True))

                yield from Translation.create(irish, t,
                                              capitalise=self.vptype.is_sentence(),
                                              sentence_grammar_tags=sentence_grammar_tags)

class IrishCopP(IrishConstituent):
    def __init__(self, ctx: TranslationContext,
                 dialects: Optional[List[Dialect]] = None,
                 post: Optional[List[Any]] = None,
                 vptype: Optional[IrishVP.VPType] = None,
                 subject: Optional[Any] = None,
                 predicate: Optional[Any] = None,
                 neg: bool = False) -> None:
        super().__init__(ctx, dialects, post)
        self.tense = Tense.PRESENT
        self.subject = IrishConstituent.create(ctx, subject)
        self.predicate = IrishConstituent.create(ctx, predicate)
        self.subpredicate = None
        self.subsubject = None
        self.neg = neg

        if vptype is None:
            self.vptype = IrishVP.VPType.STATEMENT

        if isinstance(self.subject, IrishPronounNP):
            self.subject.set_form(False, True)
        if isinstance(self.predicate, IrishPronounNP):
            self.predicate.set_form(True, True)

        if isinstance(self.predicate, IrishNP) and self.predicate.is_definite():
            if (isinstance(self.subject, IrishPronounNP)
                    and self.subject.get_person() == Person.THIRD):
                # With a third person pronoun subject, the subpredicate matches
                # the subject in form
                self.subpredicate = copy.copy(self.subject)
            else:
                pred_id = self.predicate.get_id(ctx, True)
                # FIXME Must refer to alias in the language data
                # TODO siad
                pronoun = ctx.find_words_by_lemma(['sé', 'sí'])
                self.subpredicate = IrishPronounNP(ctx, pronoun=pronoun,
                                                   ref=pred_id,
                                                   subject_form=True,
                                                   copula_form=True)
        elif isinstance(self.subject, IrishNP) and self.subject.is_definite():
            subj_id = self.subject.get_id(ctx, True)
            self.subsubject = IrishPronounNP(ctx, ref=subj_id, copula_form=True)

    def _word(self) -> str:
        return "is"

    def _generate(self) -> Generator[Translation, None, None]:

        fronted_subject = False
        use_subsubj_list = [ False ]
        if self.subsubject and not isinstance(self.predicate, IrishPronounNP):
            use_subsubj_list = [ True, False ]

        if (isinstance(self.subject, IrishPronounNP)
            and self.subject.get_person() != Person.THIRD
            and isinstance(self.predicate, IrishNP)
            and self.predicate.is_definite()):
                fronted_subject = True

        if isinstance(self.predicate, IrishNP):
            if self.predicate.is_definite():
                grammar_tags= ['cop-identification']
            else:
                grammar_tags= ['cop-classification']

        for use_subsubj in use_subsubj_list:
            words: List[TTranslationSource] = [ "ní" if self.neg else "is" ]

            subpred = None
            if fronted_subject:
                assert isinstance(self.subject, IrishPronounNP)
                self.subject.set_form(True, True)
                subpred = self.subject.generate()
            elif self.subpredicate:
                subpred = self.subpredicate.generate()

            # "ní" causes h-prefix on pronouns
            if subpred and self.neg:
                subpred = mutate_translations(subpred, h_prefix)

            if subpred:
                words.append(subpred)

            words.append(self.predicate.generate())

            if use_subsubj:
                assert self.subsubject
                subsubj = self.subsubject.generate()
                words.append(subsubj)

            if not fronted_subject:
                words.append(self.subject.generate())

            if self.vptype == IrishVP.VPType.STATEMENT:
                words.append(Translation(irish, ".", contract_with_previous=True))

            yield from Translation.create(irish, words,
                                          sentence_grammar_tags=grammar_tags,
                                          capitalise=self.vptype.is_sentence())

TMutate = Callable[[Translation], Generator[Translation, None, None]]

class IrishNP(IrishConstituent):
    """ Noun Phrase """
    def __init__(self, ctx: TranslationContext,
                 noun: Union[str, UUID, None] = None,
                 dialects: Optional[List[Dialect]] = None,
                 post: Optional[List[Any]] = None,
                 literal: Optional[str] = None,
                 id: Optional[str] = None,
                 definite: Definiteness = Definiteness.INDEFINITE,
                 case: Case = Case.NOMINATIVE,
                 number: Number = Number.SINGULAR,
                 attributes: Optional[List[Any]] = None) -> None:
        super().__init__(ctx, dialects, post)
        self.literal = literal
        if literal:
            self.noun = IrishNoun(dialects=(), **{
                "nom-sg": { "form": literal, "genders": [ "MASC" ] },
                "gen-sg": literal,
                "nom-pl": literal,
                "gen-pl": literal,
            })
        else:
            assert noun is not None
            self.noun = ctx.get_word(noun, IrishNoun)
        self.definiteness = definite
        self.case = case
        self.number = number
        self.mutate: Optional[TMutate] = None
        self.id = id

        self.attributes: List[Constituent] = []
        for x in attributes or []:
            self.attributes.append(IrishConstituent.create(ctx, x))

    @classmethod
    def word_list(cls, d: Dict[str, Any]) -> List[str]:
        return [ d["noun"] ]

    def get_id(self, ctx: TranslationContext, generate: bool = False
               ) -> Optional[str]:
        if not self.id and generate:
            self.id = ctx.generate_constituent_id()
        return self.id

    def set_case(self, case: Case, mutate: Optional[TMutate] = None) -> None:
        self.case = case
        self.mutate = mutate

    def get_person(self) -> Person:
        return Person.THIRD

    def get_number(self) -> Number:
        return self.number

    def is_definite(self) -> bool:
        return self.definiteness == Definiteness.DEFINITE

    def add_article(self, noun: Translation, c: Case, n: Number,
                    g: Gender) -> Generator[Translation, None, None]:
        #  Tuples are: (lenition/ts, eclipsis, t-prefix)
        articles = {
            Number.SINGULAR: {
                Case.NOMINATIVE: {
                    Gender.MASC: ("an", False, False, True),
                    Gender.FEM:  ("an", True, False, False),
                },
                Case.GENITIVE: {
                    Gender.MASC: ("an", True, False, False),
                    Gender.FEM:  ("na", False, False, False),
                },
                Case.DATIVE: {
                    Gender.MASC: ("an", False, False, False),
                    Gender.FEM:  ("an", False, False, False),
                },
            },
            Number.PLURAL: {
                Case.NOMINATIVE: {
                    Gender.MASC: ("na", False, False, False),
                    Gender.FEM:  ("na", False, False, False),
                },
                Case.GENITIVE: {
                    Gender.MASC: ("na", False, True, False),
                    Gender.FEM:  ("na", False, True, False),
                },
                Case.DATIVE: {
                    Gender.MASC: ("na", False, False, False),
                    Gender.FEM:  ("na", False, False, False),
                },
            },
        }

        a = articles[n][c][g]
        if self.mutate:
            result: Union[TTranslationSource] = self.mutate(noun)
        elif a[1]:
            noun_text = str(noun)
            if noun_text[0] == 's':
                noun_text = 't' + noun_text
            else:
                noun_text = IrishConstituent.lenite(noun_text)
            noun = noun.update_text(noun_text)
            result = noun
        elif a[2]:
            noun = noun.update_text(IrishConstituent.eclipse(str(noun)))
            result = noun
        elif a[3]:
            noun_text = str(noun)
            if is_vowel(noun_text[0]):
                noun = noun.update_text('t-' + noun_text)
            result = noun

        grammar_tag = "art-%s-%s-%s" % (c.get_id(), n.get_id(), g.get_id())
        article = Translation(irish, a[0], word_grammar_tags=[grammar_tag])
        return Translation.create(irish, [article, result])

    def _do_generate(self, case: Case, number: Number,
                     definite: Definiteness) -> Generator[Translation, None, None]:
        is_def = definite == Definiteness.DEFINITE
        nouns = self.noun.decline(case, number)
        for form, noun in nouns:
            for g in (form.genders or self.noun.genders):
                result: List[TTranslationSource] = []

                grammar_tag = "noun-%s-%s-%s-%s" % ("def" if is_def else "indef",
                                                    case.get_id(),
                                                    number.get_id(),
                                                    g.get_id())
                noun.add_word_grammar_tag(grammar_tag)

                if self.id:
                    agreement = IrishAgreementInfo(self.get_person(),
                                                   self.number, g)
                    noun.add_agreement_constraint(self.id, agreement)

                if is_def:
                    result = [ self.add_article(noun, case, number, g) ]
                else:
                    result = [ noun ]

                for attr in self.attributes:
                    if isinstance(attr, IrishAP):
                        # FIXME Nominative plural: strong plural
                        attr.set_parent_info(case, number, g, is_def, False,
                                             has_slender_cons_ending(str(noun)))
                    result.append(attr.generate())

                yield from Translation.create(irish, result)

    def _generate(self) -> Generator[Translation, None, None]:
        return self._do_generate(self.case, self.number, self.definiteness)

    def _word(self) -> str:
        return str(next(self._do_generate(Case.NOMINATIVE, Number.SINGULAR,
                                          Definiteness.BARE)))

class IrishAP(IrishConstituent):
    """ Adjective Phrase """
    def __init__(self, ctx: TranslationContext,
                 adjective: Union[str, UUID, None] = None,
                 dialects: Optional[List[Dialect]] = None,
                 post: Optional[List[Any]] = None,
                 literal: Optional[str] = None) -> None:
        super().__init__(ctx, dialects, post)
        if literal:
            self.adjective = IrishAdjective(root=literal)
        else:
            assert adjective is not None
            self.adjective = ctx.get_word(adjective, IrishAdjective)

        self.set_parent_info(Case.NOMINATIVE, Number.SINGULAR, Gender.MASC,
                             False, False, False)

    def set_parent_info(self, c: Case, n: Number, g: Gender, definite: bool,
                        strong_plural: bool, slender_ending: bool) -> None:
        self.case = c
        self.number = n
        self.gender = g
        self.definite = definite
        self.strong_plural = strong_plural
        self.slender_ending = slender_ending

    @classmethod
    def word_list(cls, d: Dict[str, Any]) -> List[str]:
        return [ d["adjective"] ]

    def _word(self) -> str:
        return str(next(self.adjective.decline(Case.NOMINATIVE, Number.SINGULAR,
                                               Gender.MASC, False, False, False)))

    def _generate(self) -> Generator[Translation, None, None]:
        yield from self.adjective.decline(self.case, self.number, self.gender,
                                          self.definite, self.strong_plural,
                                          self.slender_ending)

class IrishPronoun(IrishDictWord):

    name = _("pronoun")

    class IrishPronounForm(DictWordForm):
        def __init__(self, form: str, dialects: Sequence[Dialect] = []) -> None:
            self.text = form
            self.dialects = dialects or [d for d in Dialect]

    def __init__(self, person: Person, number: Number, gender: Gender = Gender.MASC,
                 dialects: Sequence[Dialect] = (), **kwargs: Any) -> None:
        self.person = person
        self.number = number
        self.gender = gender
        self.dialects = dialects

        self.infl = Inflection(self.IrishPronounForm)
        self.infl.add("subj", kwargs)
        self.infl.add("subj-emph", kwargs)

        self.infl.add("obj", kwargs, lambda x: (x, ), "subj")
        self.infl.add("obj-emph", kwargs, lambda x: (x, ), "subj-emph")

        if len(kwargs):
            raise json_util.JSONParseException(
                _("Invalid key: '%s'") % next(iter(kwargs.keys())))

    def get_forms(self) -> Set[str]:
        return self.infl.get_forms()

    def dictionary_forms(self) -> List[Tuple[str, Union[str, Translation]]]:
        # TODO Pick the most standard form instead of the first one
        return [
            (_("Lemma"), next(self.get_form(True, False, False))[1]),
            (_("Emphatic form"), next(self.get_form(True, False, True))[1]),
        ]

    def inflection(self) -> Dict[str, Any]:
        declension = {
            name: form for name, form in self.dictionary_forms()
        }
        return {
            _("Declension"): declension
        }

    def get_form(self, subject_form: bool, copula_form: bool, emphatic: bool
                 ) -> Iterator[Tuple['IrishPronoun.IrishPronounForm',Translation]]:
        index = self.number.value * 3 + self.person.value
        if not subject_form or (copula_form and index != 2):
            form_id = "obj"
        else:
            form_id = "subj"

        if emphatic:
            form_id += "-emph"

        for f in self.infl[form_id]:
            form = f.text
            t = Translation(irish, form, word=self, dialects=self.dialects,
                            dialect_feature_name=_("The word '%s'") % form)
            yield f, t

class IrishPronounNP(IrishConstituent):
    """Pronoun phrase"""
    def __init__(self, ctx: TranslationContext,
                 dialects: Optional[List[Dialect]] = None,
                 post: Optional[List[Any]] = None,
                 pronoun: Union[str, UUID, None, Sequence[str]] = None,
                 literal: Optional[str] = None,
                 ref: Optional[str] = None,
                 subject_form: Optional[bool] = None,
                 copula_form: bool = False,
                 emphatic: bool = False) -> None:
        super().__init__(ctx, dialects, post)

        if literal:
            self.pronouns = [ IrishPronoun(dialects=(), person=Person.THIRD,
                                           number=Number.SINGULAR,
                                           gender=Gender.MASC, **{
                "subj": { "form": literal },
                "subj-emph": { "form": literal },
            }) ]
        else:
            assert pronoun is not None
            if isinstance(pronoun, list):
                assert ref is not None
                pronouns = pronoun
            else:
                pronouns = [pronoun]

            self.pronouns = [
                ctx.get_word(pronoun, IrishPronoun)
                for pronoun in pronouns
            ]

        self.antecedent = ref
        self.subject_form = subject_form if subject_form is not None else True
        self.subject_form_overrridden = subject_form is not None
        self.copula_form = copula_form
        self.emphatic = emphatic

    @classmethod
    def word_list(cls, d: Dict[str, Any]) -> List[str]:
        if isinstance(d["pronoun"], list):
            return d["pronoun"]
        else:
            return [ d["pronoun"] ]

    @staticmethod
    def get_form(person: Person, number: Number) -> str:
        # FIXME This shouldn't exist
        index: int = number.value * 3 + person.value - 1
        return ['mé', 'tú', 'sé', 'muid', 'sibh', 'siad'][index]

    def get_person(self) -> Person:
        assert not self.antecedent
        return self.pronouns[0].person

    def get_number(self) -> Number:
        assert not self.antecedent
        return self.pronouns[0].number

    def get_gender(self) -> Gender:
        assert not self.antecedent
        return self.pronouns[0].gender

    def get_antecedent(self) -> Optional[str]:
        return self.antecedent

    def set_form(self, subject_form: bool, copula: bool = False) -> None:
        if not self.subject_form_overrridden:
            self.subject_form = subject_form
        self.copula_form = copula

    def _generate(self) -> Generator[Translation, None, None]:
        for p in self.pronouns:
            result = p.get_form(self.subject_form, self.copula_form,
                                self.emphatic)

            for form, t in result:
                if self.antecedent:
                    agreement = IrishAgreementInfo(p.person, p.number, p.gender)
                    t.add_agreement_constraint(self.antecedent, agreement)
                yield t

    def _word(self) -> str:
        return str(next(self._generate()))

class ArticleMutPattern(Enum):
    DEFAULT = 0

class IrishPrep(IrishDictWord):

    name = _("preposition")

    class IrishPrepForm(DictWordForm):
        def __init__(self, form: str, analytic: bool = True,
                     art_mut: ArticleMutPattern = ArticleMutPattern.DEFAULT,
                     dialects: Sequence[Dialect] = []) -> None:
            self.text = form
            self.analytic = analytic
            self.art_mut = art_mut
            self.dialects = dialects or [d for d in Dialect]

    def __init__(self, **kwargs: Any) -> None:
        self.word = self.IrishPrepForm.create(kwargs.pop("word"))
        self.forms: Dict[str, Sequence[IrishPrep.IrishPrepForm]] = {}

        for id in [ '1sg', '2sg', '3sg-m', '3sg-f', '1pl', '2pl', '3pl', 'art-sg' ]:
            self.forms[id] = self.IrishPrepForm.create(kwargs.pop(id, self.word))

    def get_art_mut_fn(self, prep_form: str) -> TMutate:
        def mut(t: Translation) -> Generator[Translation, None, None]:
            word = str(t)
            if word[0] == 's':
                # TODO Most dialects only for feminine
                yield t.update_text('t' + word)
            elif word[0] in ['d', 't']:
                yield t
            else:
                result = t.update_text(IrishConstituent.lenite(word))
                result.add_dialect_constraint(
                    [ Dialect.ULSTER ],
                    _("%(mutation)s after '%(prep)s an'") % {
                        "mutation": _("Lenition"),
                        "prep": prep_form,
                    })
                yield result

                result = t.update_text(IrishConstituent.eclipse(word))
                result.add_dialect_constraint(
                    [ Dialect.CONNACHT, Dialect.MUNSTER ],
                    _("%(mutation)s after '%(prep)s an'") % {
                        "mutation": _("Eclipsis"),
                        "prep": prep_form,
                    })
                yield result
        return mut

    def conjugate(self, person: Person, number: Number, gender: Optional[Gender],
                  analytic: bool = False) -> Generator[Tuple[Translation, bool], None, None]:

        if analytic:
            forms = self.word
        else:
            id = "%d%s" % (person.value, number.get_id())
            if id == "3sg":
                assert gender is not None
                id += "-" + gender.get_id()
            forms = self.forms[id]

        for f in forms:
            yield (Translation(irish, f.text, word=self, dialects=f.dialects,
                               dialect_feature_name=_("The form '%s'") % f.text),
                   not f.analytic)

    def get_forms(self) -> Set[str]:
        return { str(t)
                 for p in Person if p != Person.IMPERSONAL
                 for n in Number
                 for g in Gender
                 for t, _ in self.conjugate(p, n, g)
               } | {
                   f.text for f in self.word
               }

    def dictionary_forms(self) -> List[Tuple[str, Union[str, Translation]]]:
        # TODO Pick the most standard form instead of the first one
        return [
            (_("Lemma"), self.word[0].text),
        ]

    def inflection(self) -> Dict[str, Any]:
        forms: OrderedDict[str, Any] = OrderedDict()
        info = {
            _("General"): {
                _("Lemma"): [ Translation(irish, x.text, dialects=x.dialects)
                              for x in self.word ],
            },
            _("Forms"): forms,
        }

        for n in Number:
            d: OrderedDict[str, Any] = OrderedDict()
            for p in Person:
                if p == Person.IMPERSONAL:
                    pass
                elif n == Number.SINGULAR and p == Person.THIRD:
                    d[p.get_name()] = OrderedDict([
                        (g.get_name(), [x[0] for x in self.conjugate(p, n, g)]) for g in Gender
                    ])
                else:
                    d[p.get_name()] = [x[0] for x in self.conjugate(p, n, None)]
            forms[n.get_name()] = d

        return info

class IrishPP(IrishConstituent):
    """ Prepositional Phrase """

    def __init__(self, ctx: TranslationContext,
                 object: Union[str, UUID, IrishConstituent],
                 preposition: Union[str, UUID, None] = None,
                 dialects: Optional[List[Dialect]] = None,
                 post: Optional[List[Any]] = None,
                 literal: Optional[str] = None) -> None:
        super().__init__(ctx, dialects, post)
        if literal:
            self.preposition = IrishPrep(word=literal)
        else:
            assert preposition is not None
            self.preposition = ctx.get_word(preposition, IrishPrep)

        c = IrishConstituent.create(ctx, object)
        if isinstance(c, (IrishVP)):
            assert c.vptype == IrishVP.VPType.VN_GEN
        else:
            assert isinstance(c, (IrishNP, IrishPronounNP))
        self.object = c

    @classmethod
    def word_list(cls, d: Dict[str, Any]) -> List[str]:
        return [ d["preposition"] ]

    def _word(self) -> str:
        return self.preposition.word[0].text

    def _generate(self) -> Generator[Translation, None, None]:
        if isinstance(self.object, IrishPronounNP):
            for p, synthetic in self.preposition.conjugate(self.object.get_person(),
                                                           self.object.get_number(),
                                                           self.object.get_gender()):
                yield p
        else:
            # FIXME Indefinite nouns mutate differently
            result = self.preposition.conjugate(Person.THIRD,
                                                self.object.get_number(),
                                                None,
                                                analytic=True)
            for p, synthetic in result:
                assert not synthetic
                if isinstance(self.object, IrishNP):
                    self.object.set_case(Case.DATIVE, self.preposition.get_art_mut_fn(str(p)))
                for object in self.object.generate():
                    yield from Translation.create(irish, [p, object])

class IrishParticle(IrishDictWord):

    name = _("particle")

    class IrishParticleForm(DictWordForm):
        def __init__(self, form: str) -> None:
            self.text = form

    def __init__(self, **kwargs: Any) -> None:
        self.word = self.IrishParticleForm.create(kwargs.pop("word"))

    def get_forms(self) -> Set[str]:
        return { f.text for f in self.word }

    def dictionary_forms(self) -> List[Tuple[str, Union[str, Translation]]]:
        return [
            (_("Word"), self.word[0].text),
        ]

    def inflection(self) -> Dict[str, Any]:
        return {
            _("General"): {
                _("Lemma"): [ Translation(irish, f.text) for f in self.word ],
            },
        }

class IrishPar(IrishConstituent):
    def __init__(self, ctx: TranslationContext,
                 particle: Union[str, UUID],
                 dialects: Optional[List[Dialect]] = None,
                 post: Optional[List[Any]] = None) -> None:
        super().__init__(ctx, dialects, post)
        self.particle = ctx.get_word(particle, IrishParticle)

    def _word(self) -> str:
        return self.particle.word[0].text

    @classmethod
    def word_list(cls, d: Dict[str, Any]) -> List[str]:
        return [ d["particle"] ]

    def _generate(self) -> Generator[Translation, None, None]:
        for w in self.particle.word:
            yield Translation(irish, w.text, word=self.particle)

irish = Language("ga", _("Irish"), IrishDictWord, IrishConstituent, Dialect, Dialect.ULSTER_CO) # type: ignore
