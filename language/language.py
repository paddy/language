from typing import *
from enum import Enum
from uuid import UUID

from json_util import (JSONParseException, dict_str_to_enum,
                       create_from_json_dict)

import abc
import copy
from collections import defaultdict
import inspect
import json
import json_util
import uuid

from translation import gettext as _

class Dialect(int, Enum):
    """
    Abstract base class for an enumeration of all dialects. To be
    subclassed for each language.
    """
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__()
        self.dname: str
        self.letter: str
        self.subdialects: Set['Dialect']
        self.has_unnamed_subdialects: bool

    def __new__(cls, value: int, dname: str, letter: str,
                subdialects: Sequence['Dialect'] = (),
                has_unnamed_subdialects: bool = False) -> 'Dialect':
        obj = cast('Dialect', int.__new__(cls, value)) # type: ignore
        obj._value_ = value # type: ignore
        obj.dname = dname
        obj.letter = letter
        obj.subdialects = set([cls(d[0]) for d in subdialects]) # type: ignore
        obj.has_unnamed_subdialects = has_unnamed_subdialects
        return obj

    def get_letter(self) -> str:
        return self.letter

    @classmethod
    def collapse_subdialects(cls, dialects: Set['Dialect']) -> Set['Dialect']:
        changed = True
        while changed:
            changed = False
            for d in cls:
                if d.has_unnamed_subdialects and d not in dialects:
                    continue
                if d.subdialects and dialects >= d.subdialects:
                    dialects -= d.subdialects
                    dialects.add(d)
                    changed = True
        return dialects

    @classmethod
    def propagate_subdialects(cls, dialects: Set['Dialect']) -> Set['Dialect']:
        s = set([ sd for d in dialects for sd in d.subdialects ])
        while not dialects >= s:
            dialects |= s
            s = set([ sd for d in dialects for sd in d.subdialects ])
        return dialects

    @classmethod
    def set_to_str(cls, dialects: Iterable['Dialect']) -> str:
        l = []

        for d in cls.collapse_subdialects(cls.propagate_subdialects(set(dialects))):
            l.append(d.dname)

        if len(l) == 0:
            return _("No dialect")
        elif len(l) == 1:
            return l[0]
        else:
            return ", ".join(str(x) for x in l[:-1]) + _(" and ") + l[-1]

TTextContextCB = Optional[Callable[[Optional[str], str, Optional[str]], str]]

class TranslationPart:
    def __init__(self, text: str, word: Optional['DictWord'] = None,
                 text_context_cb: TTextContextCB = None,
                 contract_with_previous: bool = False,
                 grammar_tags: Collection[str] = ()) -> None:
        self.base_text = text
        self.text = text
        self.text_context_cb = text_context_cb
        self.contract_with_previous = contract_with_previous
        self.corrected = False
        self.grammar_tags: Set[str] = set(grammar_tags)

        if hasattr(word, "uuid"):
            self.word = word
        else:
            self.word = None

    def __repr__(self) -> str:
        return self.text

TTranslationSource = Union[Generator['Translation', None, None], 'Translation', str, None]

class Translation:
    """
    Represents a single generated variant of a (part of a) sentence.

    Besides containing the actual text string, it adds bookkeeping for
    dialectal features that are used, so that the caller can determine whether
    a specific sentence is consistent within a single dialect, and if so, in
    which dialect(s) it is a correct sentence.
    """

    class AgreementInfo:
        pass

    class AgreementViolation(Exception):
        pass

    def __init__(self, lang: 'Language', text: Optional[str] = None,
                 children: List['Translation'] = [],
                 dialects: Sequence[Dialect] = [],
                 dialect_feature_name: Optional[str] = None,
                 word: Optional['DictWord'] = None,
                 text_context_cb: TTextContextCB = None,
                 sentence_grammar_tags: Collection[str] = (),
                 word_grammar_tags: Collection[str] = (),
                 contract_with_previous: bool = False) -> None:
        self.lang = lang
        self.dialect_num_features = 0
        self.dialect_counter: Dict[Dialect, int] = {}
        self.dialect_features: Dict[str, Set[Dialect]] = {}
        self.agreement_info: Dict[str, 'Translation.AgreementInfo'] = {}
        self.sentence_grammar_tags: Set[str] = set(sentence_grammar_tags)
        self.meaning = 0

        for dialect in lang.dialects:
            self.dialect_counter[dialect] = 0

        if text is not None:
            assert not children
            self.parts = [ TranslationPart(text, word, text_context_cb,
                                           contract_with_previous,
                                           word_grammar_tags) ]

            if dialects:
                self.add_dialect_constraint(dialects, dialect_feature_name)
        else:
            assert not text
            assert not text_context_cb
            assert not dialects
            assert not dialect_feature_name
            assert not sentence_grammar_tags
            assert not word_grammar_tags

            self.parts = []
            for c in children:
                self.parts += c.parts
                self.sentence_grammar_tags |= c.sentence_grammar_tags
                self.dialect_num_features += c.dialect_num_features
                self.dialect_features.update(c.dialect_features)
                for dialect in lang.dialects:
                    self.dialect_counter[dialect] += c.dialect_counter[dialect]
                for id, value in c.agreement_info.items():
                    self.add_agreement_constraint(id, value)

        padded_list: List[Optional[TranslationPart]] = [None]
        padded_list += self.parts
        padded_list += [None]

        for prev, cur, nxt in zip(padded_list, padded_list[1:], padded_list[2:]):
            assert cur
            if cur.text_context_cb:
                cur.text = cur.text_context_cb(
                    prev.text if prev else None,
                    cur.base_text,
                    nxt.text if nxt else None)

    @classmethod
    def _create(cls,
                words: List[Sequence['Translation']], i: int
               ) -> Generator['Translation', None, None]:
        word = words[i]
        if i >= len(words) - 1:
            if isinstance(word, Translation):
                yield word
            else:
                yield from word
        else:
            for x in word:
                for y in cls._create(words, i + 1):
                    try:
                        yield Translation(x.lang, children=[x, y])
                    except cls.AgreementViolation:
                        # This is fine, we just don't yield a translation then
                        pass

    @classmethod
    def create(cls, lang: 'Language', words: Sequence[TTranslationSource],
               sentence_grammar_tags: Sequence[str] = (),
               capitalise: bool = False) -> Generator['Translation', None, None]:

        twords: List[Sequence[Translation]] = []
        for word in words:
            if word is None:
                continue
            elif isinstance(word, str):
                twords.append([Translation(lang, word)])
            elif isinstance(word, Translation):
                twords.append([word])
            else:
                # Convert generator to list so it can be evaluated multiple times
                twords.append(list(word))

        grammar_tag_set = set(sentence_grammar_tags)
        for t in cls._create(twords, 0):
            t.sentence_grammar_tags |= grammar_tag_set
            if capitalise and t.parts:
                t.parts[0].text = t.parts[0].text.capitalize()
            yield t

    def update_text(self, text: str) -> 'Translation':
        assert len(self.parts) == 1
        t = copy.copy(self)
        t.parts = [ copy.copy(t.parts[0]) ]
        t.parts[0].text = text
        return t

    def set_meaning(self, meaning: int) -> None:
        self.meaning = meaning

    def add_dialect_constraint(self, dialect_list: Sequence[Dialect],
                               dialect_feature_name: Optional[str] = None) -> None:
        dialects = self.lang.dialects.propagate_subdialects(
            self.lang.dialects.collapse_subdialects(set(dialect_list)))
        self.dialect_num_features += 1
        for dialect in dialects:
            self.dialect_counter[dialect] += 1
        if dialect_feature_name is not None:
            self.dialect_features[dialect_feature_name] = set(dialects)

    def add_agreement_constraint(self, id: str,
                                 info: 'Translation.AgreementInfo') -> None:
        if id in self.agreement_info:
            if self.agreement_info[id] != info:
                raise self.AgreementViolation()
        else:
            self.agreement_info[id] = info

    def add_word_grammar_tag(self, tag: str) -> None:
        assert len(self.parts) == 1
        self.parts[0].grammar_tags.add(tag)

    def all_grammar_tags(self) -> Set[str]:
        tags = self.sentence_grammar_tags
        for p in self.parts:
            tags |= p.grammar_tags
        return tags

    def merge(self, other: 'Translation') -> None:
        assert self.dialect_num_features == other.dialect_num_features
        assert str(self) == str(other)

        # When deduplicating translations, agreement for each given translation
        # is already ensured, but might be different between both.
        self.agreement_info = {}

        for dialect in self.lang.dialects:
            self.dialect_counter[dialect] = max(self.dialect_counter[dialect],
                                                other.dialect_counter[dialect])

        for feature in other.dialect_features:
            if feature not in self.dialect_features:
                self.dialect_features[feature] = set()
            self.dialect_features[feature] |= other.dialect_features[feature]

    def dialect_str(self) -> str:
        s = ''
        dialects = self.dialects()
        for dialect in self.lang.dialects:
            if dialect in dialects:
                s += dialect.get_letter()
            else:
                s += '-'

        return s

    def dialects(self) -> Set[Dialect]:
        s = set()
        for dialect in self.lang.dialects:
            if self.dialect_counter[dialect] == self.dialect_num_features:
                s.add(dialect)
        return s

    def dialect_hints(self) -> Generator[str, None, None]:
        for feature in self.dialect_features:
            if self.dialect_features[feature] != set(self.lang.dialects.__members__.values()):
                yield _("%(feature)s is typical for %(dialects)s") % {
                    "feature": feature,
                    "dialects": self.lang.dialects.set_to_str(
                                    self.dialect_features[feature])
                }

    @staticmethod
    def best_translation(translations: Iterator['Translation'],
                         preferred_dialect: Dialect,
                         most_unique: bool = False,
                         word_priorities: Optional[Dict[UUID, float]] = None) -> 'Translation':
        best = None
        best_score = 0.0
        best_uniqueness = 0
        p = preferred_dialect.propagate_subdialects(set([preferred_dialect]))

        for x in translations:
            score = 0.0

            if word_priorities:
                max_priority = 0.0
                for part in x.parts:
                    if part.word and part.word.uuid in word_priorities:
                        max_priority = max(max_priority, word_priorities[part.word.uuid])
                score += max_priority

            # TODO Normalise between 0.7 and 1.0 if len(...) > 1 for some translation
            dialects = x.dialects()
            factor = 0.7 + 0.3 * len(p & dialects)
            score *= factor

            uniqueness = sum(1 for d in x.lang.dialects if d not in dialects)
            if (best is None or score > best_score
                or (score == best_score and most_unique and uniqueness > best_uniqueness)
                or (score == best_score and not most_unique and uniqueness < best_uniqueness)):
                best = x
                best_score = score
                best_uniqueness = uniqueness

        if best is None:
            raise Exception("No translation given")

        return best

    def __eq__(self, other: Any) -> Any:
        return (self.lang == other.lang and str(self) == str(other) and
                self.dialect_counter == other.dialect_counter)

    def __str__(self) -> str:
        first = True
        result = ""
        for p in self.parts:
            if not (first or p.contract_with_previous):
                result += " "
            result += p.text
            first = False
        return result

    def __repr__(self) -> str:
        return str(vars(self))

class GrammaticalCategory(int, Enum):
    """
    Abstract base class for an enumeration of all values of a grammatical
    category. To be subclassed for each language.
    """
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__()
        self.id: str
        self.vname: str

    def __new__(cls, value: int, vname: str, id: str) -> 'GrammaticalCategory':
        obj = cast('GrammaticalCategory', int.__new__(cls, value)) # type: ignore
        obj._value_ = value # type: ignore
        obj.vname = vname
        obj.id = id
        return obj

    def get_name(self) -> str:
        return self.vname

    def get_id(self) -> str:
        return self.id

TConstituent = TypeVar('TConstituent', bound='Constituent')

class Constituent(metaclass=abc.ABCMeta):
    """
    A single node in the intermediate syntax-tree-like representation.

    This is an abstract base class that should be subclassed for things like
    verbs, nouns, ...
    """
    def __init__(self, ctx: 'TranslationContext',
                 dialects: Optional[Sequence[Dialect]],
                 post: Optional[Sequence[Any]]) -> None:
        self.language = ctx.language
        self.dialects = dialects
        self.meaning = 0

        self.post: List[Constituent] = []
        for p in post or []:
            self.post.append(Constituent.create(ctx, p))

    def set_meaning(self, meaning: int) -> None:
        self.meaning = meaning

    @classmethod
    def create(cls: Type[TConstituent], ctx: 'TranslationContext',
               source: Any) -> TConstituent:
        if isinstance(source, cls):
            return source
        elif not isinstance(source, dict):
            raise Exception("Expected a dict")

        ctype = source.pop("type", None)
        if isinstance(ctype, type) and issubclass(ctype, cls):
            if "literal" in source and not isinstance(source["literal"], str):
                raise Exception("literal must be a string")
            source["ctx"] = ctx
            res: TConstituent = create_from_json_dict(ctype, source)
            return res

        raise Exception("Not a Constituent")

    def generate(self, **kwargs: Any) -> Generator[Translation, None, None]:
        for t in self._generate():
            if self.meaning and not t.meaning:
                t.set_meaning(self.meaning)

            if self.dialects:
                word = self._word()
                t.add_dialect_constraint(
                    self.dialects,
                    (_("The expression '%s'") % word) if " " in word else (_("The word '%s'") % word))
            if self.post:
                result: List[TTranslationSource] = [t]
                for p in self.post:
                    result.append(p.generate())
                yield from Translation.create(self.language, result)
            else:
                yield t

    @abc.abstractmethod
    def _generate(self) -> Generator[Translation, None, None]:
        pass

    @abc.abstractmethod
    def _word(self) -> str:
        pass

    @staticmethod
    def json_object_hook(d: Dict[Any, Any]) -> None:
        pass

    @classmethod
    def word_list(cls, d: Dict[str, Any]) -> List[str]:
        return []

    def get_antecedent(self) -> Optional[str]:
        return None

    def __repr__(self) -> str:
        return "%s:%s" % (type(self).__name__, str(vars(self)))

TDictWordForm = TypeVar('TDictWordForm', bound='DictWordForm')
TDataDictWordForm = TypeVar('TDataDictWordForm', bound='DictWordForm')

class Inflection(Generic[TDictWordForm]):
    class Data(Generic[TDataDictWordForm]):
        def __init__(self,
                     forms: Optional[Sequence[TDataDictWordForm]] = None,
                     default_cb: Optional[Callable[[TDataDictWordForm], Sequence[TDataDictWordForm]]] = None,
                     default_from: Optional[Sequence[str]] = None) -> None:
            assert forms or default_from
            self.forms = forms
            self.default_cb = default_cb
            self.default_from = default_from

    def __init__(self, form_cls: Type[TDictWordForm]) -> None:
        self.forms: Dict[str, 'Inflection.Data'[TDictWordForm]] = {}
        self.form_cls = form_cls

    def add(self, form_id: str, definition_kwargs: Dict[str, Any],
            default_cb: Optional[Callable[[TDictWordForm], Sequence[TDictWordForm]]] = None,
            default_from: Optional[Union[str, Sequence[str]]] = None,
            form_create_opts: Dict[str, Any] = {}) -> None:

        if form_id in definition_kwargs:
            form: Any = definition_kwargs.pop(form_id)
            if not isinstance(form, list):
                form = [ form ]
            result: List[TDictWordForm] = []
            for f in form:
                result += self.form_cls.create(f, **form_create_opts)
            self.forms[form_id] = self.Data(forms=result)
        elif default_from:
            if isinstance(default_from, str):
                default_from = (default_from, )
            self.forms[form_id] = self.Data(default_cb=default_cb,
                                            default_from=default_from)
        else:
            raise JSONParseException(_("Missing key: '%s'") % form_id)

    def __getitem__(self, form_id: str) -> Sequence[TDictWordForm]:
        data = self.forms[form_id]
        if not data.forms:
            assert data.default_from
            df = list(data.default_from)
            while len(df) > 1 and self.forms[df[0]].default_from:
                df = df[1:]

            forms = copy.deepcopy(self[df[0]])
            result: List[TDictWordForm] = []
            if data.default_cb:
                for f in forms:
                    result += data.default_cb(f)
            else:
                result += forms
            data.forms = result
        return data.forms

    def get_forms(self) -> Set[str]:
        forms = set()
        for form_id in self.forms:
            for f in self[form_id]:
                forms.add(f.text)
        return forms

class DictWordForm:
    @abc.abstractmethod
    def __init__(self, text: str, **kwargs: Any) -> None:
        self.text = text

    @classmethod
    def create(cls: Type[TDictWordForm], definition: Any, **kwargs: Any) -> Sequence[TDictWordForm]:
        if isinstance(definition, str):
            return [cls(form=definition, **kwargs)]
        elif isinstance(definition, dict):
            return [create_from_json_dict(cls, { **definition, **kwargs })]
        elif isinstance(definition, list):
            result: List[TDictWordForm] = []
            for d in definition:
                result += cls.create(d, **kwargs)
            return result
        elif isinstance(definition, cls):
            return [copy.copy(definition)]
        else:
            raise JSONParseException("Definition '%r' must be string or dict" % definition)

class DictWordAudio:
    """
    Describes a single pronunciation recording
    """
    def __init__(self, url: str, dialects: Sequence[Dialect]) -> None:
        self.url = url
        self.dialects = Dialect.propagate_subdialects(set(dialects))

class DictWord:
    """
    Abstract base class for a word in the dictionary
    """
    def __init__(self, **kwargs: Dict[str, Any]) -> None:
        self.language: Language
        self.audio: List[DictWordAudio] = []
        self.uuid: UUID

    @abc.abstractmethod
    def dictionary_forms(self) -> List[Tuple[str, Union[str, Translation]]]:
        pass

    @classmethod
    def get_subclasses(cls) -> Generator[Type['DictWord'], None, None]:
        for subclass in cls.__subclasses__():
            yield from subclass.get_subclasses()
            yield subclass

    @staticmethod
    def expand_audio_shortcut(s: str) -> Any:
        if s.startswith("teanglann:"):
            word = s[10:]
            return  [
                { "dialects": [ "ULSTER_CO", "ULSTER_D" ], "url": "http://www.teanglann.ie/CanU/%s.mp3" % word },
                { "dialects": [ "CONNACHT_CO", "CONNACHT_D" ], "url": "http://www.teanglann.ie/CanC/%s.mp3" % word },
                { "dialects": [ "MUNSTER_CO", "MUNSTER_D" ], "url": "http://www.teanglann.ie/CanM/%s.mp3" % word },
            ]

        return s

    @classmethod
    def from_json(cls, uuid: UUID, json_str: str) -> 'DictWord':
        json_dict = json.loads(json_str)
        return cls.from_json_dict(uuid, json_dict)

    @classmethod
    def from_json_dict(cls, uuid: UUID, json_dict: Dict[str, Any]) -> 'DictWord':
        lang_str = json_dict.pop('language')
        language = Language.get_by_code(lang_str)

        audio_list = json_dict.pop('audio', [])
        if isinstance(audio_list, str):
            audio_list = cls.expand_audio_shortcut(audio_list)
        if not isinstance(audio_list, list):
            raise JSONParseException(_("'%s' must be a list"), "audio")

        audio: List[DictWordAudio] = [
            create_from_json_dict(DictWordAudio, a, { Dialect: language.dialects } )
            for a in audio_list
        ]

        word_type = json_dict.pop('type')
        for t in language.word_class.get_subclasses():
            if t.__name__ == word_type:
                dict_str_to_enum(json_dict, 'dialects', language.dialects)

                options = inspect.signature(t.__init__ ).parameters

                accept_kwargs = False
                for p in options.values():
                    if p.kind == p.VAR_KEYWORD:
                        accept_kwargs = True

                if not accept_kwargs:
                    for k in json_dict:
                        if not k in options:
                            raise JSONParseException(_("Invalid key: '%s'") % k)

                result = create_from_json_dict(t, json_dict)
                result.language = language
                result.audio = audio
                result.uuid = uuid
                return result
        raise JSONParseException(_("Invalid word type: '%s'") % word_type)

    def lemma(self) -> str:
        forms = self.dictionary_forms()
        return str(forms[0][1])

    def get_forms(self) -> Set[str]:
        return set()

    @abc.abstractmethod
    def inflection(self) -> Dict[str, Any]:
        pass

    def get_audio(self, dialect: Dialect) -> Optional[DictWordAudio]:
        best: Optional[DictWordAudio] = None
        best_num_dialects = len(self.language.dialects) + 1

        for a in self.audio:
            if dialect in a.dialects and len(a.dialects) < best_num_dialects:
                best_num_dialects = len(a.dialects)
                best = a

        return best

class Concept:
    """
    A concept is, er... something. Let's call it an entity of meaning?

    It describes the meaning that a sentence (or part of a sentence) is
    supposed to express. It is the input for the whole sentence generation
    code.

    Concepts can take arguments. For example, verb concepts want another
    concept as their subject.
    """
    def __init__(self, uuid: Union[UUID, str], args: List['Concept'] = [],
                 var: Dict[str, Any] = {}, **attr: Any) -> None:
        self._uuid = uuid
        self.args = args
        self.var = var
        self.attr = attr

    def get_uuid(self, ctx: 'TranslationContext') -> UUID:
        # The first time a string UUID is accessed, convert it to a UUID object
        if isinstance(self._uuid, str):
            self._uuid = UUID(self._uuid)

        return self._uuid

    def add_attributes(self, expansion: Dict[str, Any]) -> None:
        expansion.update(self.attr)

    def __repr__(self) -> str:
        return str(vars(self))

    @classmethod
    def get_var(cls, var: Dict[str, Any], var_name: str) -> Any:
        if not var_name in var:
            raise JSONParseException("Variable '%s' does not exist" % var_name)
        return var[var_name]

    @classmethod
    def handle_vars(cls, json_dict: Dict[str, Any],
                    var: Dict[str, Any]) -> None:
        if 'include' in json_dict:
            include = json_dict.pop('include')
            if isinstance(include, str) and include[0] == '$':
                value = cls.get_var(var, include[1:])
            else:
                value = include
            if not isinstance(value, dict):
                raise JSONParseException("Include variable must be a dict")
            json_dict.update(value)

        for key, value in json_dict.items():
            if isinstance(value, str) and value[0] == '$':
                json_dict[key] = Concept.get_var(var, value[1:])

    @classmethod
    def from_json_dict(cls, json_dict: Dict[str, Any],
                       var: Dict[str, Any]) -> 'Concept':
        cls.handle_vars(json_dict, var)

        concept = json_dict.pop('concept')
        assert isinstance(concept, (UUID, str))

        json_args = json_dict.pop('args', [])
        if not isinstance(json_args, list):
            raise JSONParseException(_("'%s' must be a list") % 'args')

        args: List[Any] = []
        for a in json_args:
            args.append(cls.from_json_dict(a, var))

        return cls(str(concept), args, var, **json_dict)

class ConceptArg:
    """
    Represents a concept argument in a concept translation rule. During the
    translation it is replaced with whatever arguments were actually given to
    the Concept object that is translated using the rule.
    """
    def __init__(self, index: int, expect_type: Type[Constituent],
                 **attr: Any) -> None:
        self.index = index
        self.expect_type = expect_type
        self.attr = attr

class ConceptTranslationRule:
    """
    Defines how to translate concepts into constituent trees. This is expressed
    in form of a dictionary that the Concept is expanded to. The dictionary is
    then fed to Constituent.create(), which turns it into actual Constituent
    objects.
    """
    def __init__(self, language: 'Language', num_args: int,
                 args: Dict[int, ConceptArg], expansion: Dict[str, Any],
                 ambiguity: Sequence[UUID] = ()) -> None:
        self.language = language
        self.num_args = num_args
        self.args = args
        self.expansion = expansion
        self.ambiguity = ambiguity

    def word_list(self, x: Any = None) -> List[str]:
        res: List[str] = []
        if x is None:
            x = self.expansion
        if isinstance(x, dict):
            if 'type' in x:
                if issubclass(x['type'], self.language.constituent_class):
                    y = x['type'].word_list(x)
                    res += y
            for key in x:
                res += self.word_list(x[key])
        elif isinstance(x, list):
            for element in x:
                res += self.word_list(element)
        return res

    @staticmethod
    def json_parse_objects(lang: 'Language', d: Dict[str, Any],
                           args: Dict[int, ConceptArg]) -> Any:
        def object_hook(d: Dict[str, Any]) -> Any:
            if 'type' in d:
                for t in lang.constituent_class.__subclasses__():
                    if t.__name__ == d['type']:
                        d['type'] = t
            if 'arg' in d:
                assert isinstance(d['arg'], int)
                assert issubclass(d['type'], lang.constituent_class)
                arg = ConceptArg(d.pop('arg'), d.pop('type'), **d)
                if arg.index in args:
                    assert arg == args[arg.index]
                else:
                    args[arg.index] = arg
                return arg
            else:
                lang.constituent_class.json_object_hook(d)
                return d

        def recurse(d: Any) -> Any:
            if isinstance(d, dict):
                for key in d:
                    d[key] = recurse(d[key])
                return object_hook(d)
            elif isinstance(d, list):
                return [ recurse(x) for x in d ]
            else:
                return d

        result = recurse(d)
        assert result == d or isinstance(result, ConceptArg)
        return d

    @classmethod
    def from_json(cls, lang: 'Language', num_args: int,
                  json_dict: Dict[Any, Any], ambiguity: Sequence[str]
                  ) -> 'ConceptTranslationRule':
        args: Dict[int, ConceptArg] = {}
        result = cls.json_parse_objects(lang, json_dict, args)
        uuid_ambiguity = [UUID(x) for x in ambiguity]
        return ConceptTranslationRule(lang, num_args, args,
                                      result, uuid_ambiguity)

class ConceptDefinition:

    uuid1 = uuid.uuid1

    class Translation:
        def __init__(self, language: str, data: Dict[str, Any],
                     ambiguity: Sequence[str] = ()) -> None:
            self.lang = Language.get_by_code(language)
            self.data = data
            self.ambiguity = ambiguity

    class Index:
        def __init__(self, aliases: Set[str],
                     words: Set[Tuple[UUID, 'Language']]) -> None:
            self.aliases = aliases
            self.words = words

    def __init__(self, translations: Sequence[Dict[str, Any]],
                 alias: Optional[str] = None, args: int = 0,
                 uuid: Optional[Union[UUID, str]] = None) -> None:

        self.num_args = args
        self.alias = alias
        self.rules: List[ConceptTranslationRule] = []
        if isinstance(uuid, str):
            uuid = UUID(uuid)
        self.uuid = uuid or ConceptDefinition.uuid1()

        for t in translations:
            trans = create_from_json_dict(ConceptDefinition.Translation, t)
            rule = ConceptTranslationRule.from_json(trans.lang,
                                                    args, trans.data,
                                                    trans.ambiguity)
            self.rules.append(rule)

    def generate_index(self, ctx: 'TranslationContext') -> 'ConceptDefinition.Index':
        aliases = set()
        words = set()

        if self.alias:
            aliases.add(self.alias)

        languages = set(r.language for r in self.rules)
        for l in languages:
            ctx.switch_language(l)
            translations = ctx.translate(Concept(self.uuid))
            for t in translations:
                aliases.add(str(t))
                for p in t.parts:
                    if p.word:
                        words.add((p.word.uuid, l))

        return ConceptDefinition.Index(aliases, words)

class Phrase:
    class Variant:
        def __init__(self, lang: 'Language', text: Union[str, Concept],
                     param_config_index: int) -> None:
            self.lang = lang
            self.text = text
            self.param_config_index = param_config_index

    class VariantIndex:
        def __init__(self, lang: 'Language', text: str,
                     words: Set[UUID], tags: Set[str],
                     use_as_question: bool) -> None:
            self.lang = lang
            self.text = text
            self.words = words
            self.tags = tags
            self.use_as_question = use_as_question

    def __init__(self, variants: Sequence['Phrase.Variant'],
                 max_param_config_index: int) -> None:
        self.variants = variants
        self.max_param_config_index = max_param_config_index

    def get_variants(self, lang: 'Language',
                     param_config_index: int) -> Sequence[Union[str, Concept]]:
        return [ v.text for v in self.variants if v.lang == lang
                 and v.param_config_index == param_config_index ]

    @classmethod
    def parse_var(cls, json_dict: Dict[str, Any], key: str = 'var'
                  ) -> Dict[str, Sequence[Any]]:
        var: Dict[str, Sequence[Any]] = {}

        if key not in json_dict:
            return var

        var_dict = json_dict[key]
        if not isinstance(var_dict, dict):
            raise JSONParseException(_("'%s' must be a dict") % key)

        for vname in var_dict:
            values = var_dict[vname]
            if not isinstance(values, list):
                raise JSONParseException(_("'%s' must be a list") % vname)
            var[vname] = values

        return var

    @classmethod
    def var_configs(cls, var: Dict[str, Sequence[Any]]
                    ) -> Generator[Dict[str, Any], None, None]:
        if not var:
            yield {}
            return

        key, values = var.popitem()
        for v in values:
            for x in cls.var_configs(var):
                yield { key: v, **x }
        var[key] = values

    @classmethod
    def from_json(cls, json_dict: Any) -> 'Phrase':

        if isinstance(json_dict, dict):
            json_list = json_dict['phrase']
            param = cls.parse_var(json_dict, 'param')
        else:
            json_list = json_dict
            param = {}
        assert isinstance(json_list, list)

        max_index = 0
        variants: List['Phrase.Variant'] = []
        for param_config in cls.var_configs(param):
            max_index += 1
            for v in json_list:
                assert isinstance(v, dict)
                assert isinstance(v['data'], dict) or isinstance(v['data'], str)

                var = cls.parse_var(v)
                if var.keys() & param.keys():
                    raise json_util.JSONParseException("Conflicting parameter and variable")
                var.update({ k: [v] for k, v in param_config.items() })

                if isinstance(v['language'], list):
                    lang_codes = v['language']
                else:
                    lang_codes = [ v['language'] ]

                for lang_code in lang_codes:
                    assert isinstance(lang_code, str)
                    lang = Language.get_by_code(lang_code)

                    if isinstance(v['data'], str):
                        variants.append(cls.Variant(lang, v['data'], max_index))
                    else:
                        for var_config in cls.var_configs(var):
                            data = copy.deepcopy(v['data'])
                            concept = Concept.from_json_dict(data, var_config)
                            variants.append(cls.Variant(lang, concept, max_index))

        return Phrase(variants, max_index)

    def generate_index(self, ctx: 'TranslationContext') -> Dict[int, List['Phrase.VariantIndex']]:
        variants: Dict[int, List['Phrase.VariantIndex']] = defaultdict(list)
        ambiguous: Dict[Tuple[int, int], List[Translation]] = defaultdict(list)

        for v in self.variants:
            if not isinstance(v.text, Concept):
                variants[v.param_config_index].append(
                    Phrase.VariantIndex(v.lang, str(v.text), set(), set(), True))
                continue

            ctx.switch_language(v.lang)

            for t in ctx.translate(v.text, include_ambiguous=True):
                if t.meaning != 0:
                    ambiguous[(v.param_config_index, t.meaning)].append(t)
                    continue

                twords: Set[UUID] = set()
                ttags: Set[str] = set()

                for tag in t.sentence_grammar_tags:
                    ttags.add(tag)

                for p in t.parts:
                    if p.word:
                        twords.add(p.word.uuid)
                    for tag in p.grammar_tags:
                        ttags.add(tag)

                variants[v.param_config_index].append(
                    Phrase.VariantIndex(v.lang, str(t), twords, ttags, True))

        i = max(index for index in variants) + 1
        for meaning in ambiguous.values():
            for t in meaning:
                variants[i].append(Phrase.VariantIndex(
                    v.lang, str(t), set(), set(),  False))

        return variants

class Language:
    """
    Represents a language and provides functions to translate concepts into
    actual sentences in that language.
    """

    languages: List['Language'] = []

    def __init__(self, code: str, name: str, word_class: Type[DictWord],
                 constituent_class: Type[Constituent], dialects: Type[Dialect],
                 default_dialect: Dialect) -> None:
        self.code = code
        self.name = name
        self.word_class = word_class
        self.constituent_class = constituent_class
        self.dialects = dialects
        self.default_dialect = default_dialect
        Language.languages.append(self)

    @staticmethod
    def get_by_code(code: str) -> 'Language':
        for l in Language.languages:
            if l.code == code:
                return l
        raise Exception("No such language (%s)" % code)

class TranslationContext(metaclass=abc.ABCMeta):
    def __init__(self, language: Optional[Language],
                 words: Dict[UUID, DictWord],
                 concepts: Dict[UUID, ConceptDefinition]) -> None:
        if language is None:
            self.language = Language.languages[0]
        else:
            self.language = language
        self.words = words
        self.concepts = concepts
        self.vocab_errors: Dict[UUID, UUID] = {}
        self.next_constituent_id = 0
        self.next_meaning_id = 0

    def generate_constituent_id(self) -> str:
        self.next_constituent_id += 1
        return "#%d" % self.next_constituent_id

    def generate_meaning_id(self) -> int:
        self.next_meaning_id += 1
        return self.next_meaning_id

    def switch_language(self, language: Language) -> None:
        self.language = language

    def find_words_by_lemma(self, lemma: Sequence[str]) -> Sequence[str]:
        result = []
        for w in self.words:
            if self.words[w].lemma() in lemma:
                result.append(str(w))
        return result

    def find_rule(self, uuid: UUID) -> Tuple[List[ConceptTranslationRule], List[UUID]]:
        rules = []
        ambiguities: List[UUID] = []

        for r in self.concepts[uuid].rules:
            if r.language == self.language:
                rules.append(r)
            if r.ambiguity:
                ambiguities += r.ambiguity

        return (rules, ambiguities)

    TDictWord = TypeVar('TDictWord', bound=DictWord)
    def get_word(self, uuid: Union[str, UUID], type: Type[TDictWord]) -> TDictWord:
        if not isinstance(uuid, UUID):
            uuid = UUID(uuid)
        if uuid in self.vocab_errors:
            uuid = self.vocab_errors[uuid]
        word = self.words[uuid]
        if isinstance(word, type):
            return word
        else:
            raise Exception("Word '%s' is not of type '%s'"
                            % (uuid, type.__name__))

    def do_replace_argument_list(self, l: List[Any], parent: Concept,
                                include_ambiguous: bool) -> Iterator[Any]:
        if len(l) == 0:
            yield []
            return

        for x in self.replace_arguments(l[0], parent, include_ambiguous):
            if len(l) == 1:
                yield [x]
            else:
                for y in self.do_replace_argument_list(l[1:], parent,
                                                       include_ambiguous):
                    yield [x] + y

    def do_replace_argument_dict(self, d: Dict[Any, Any], parent: Concept,
                                include_ambiguous: bool) -> Iterator[Any]:
        if not d:
            yield {}
            return

        it = iter(d.items())
        key, value = next(it)
        rest = dict(it)

        for x in self.replace_arguments(value, parent, include_ambiguous):
            if not rest:
                yield { key: x }
            else:
                for y in self.do_replace_argument_dict(rest, parent, include_ambiguous):
                    yield { key: x, **y }

    def replace_arguments(self, expansion: Any, parent: Concept,
                          include_ambiguous: bool) -> Generator[Any, None, None]:
        """
        Walk through the expanded form of a concept (given in @expansion) and
        replace any ConceptArg object or variable in the whole tree with its
        actual value as stores in @parent.

        Returns a new copy of mutable elemnts in @expansion so that the result
        can be modified without affecting the processing of other results.
        """
        d = expansion
        if isinstance(d, dict):
            for new_dict in self.do_replace_argument_dict(d, parent, include_ambiguous):
                Concept.handle_vars(new_dict, parent.var)
                if 'concept' in new_dict:
                    concept = Concept.from_json_dict(new_dict, parent.var)
                    yield from self.create_tree(concept, include_ambiguous)
                else:
                    yield new_dict
        elif isinstance(d, list):
            yield from self.do_replace_argument_list(d, parent, include_ambiguous)
        elif isinstance(d, ConceptArg):
            arg = copy.deepcopy(parent.args[d.index])
            arg.attr.update(d.attr)
            yield from self.create_tree(arg, include_ambiguous)
        elif isinstance(d, Concept):
            yield from self.create_tree(d, include_ambiguous)
        elif isinstance(d, str) and d[0] == '$':
            yield Concept.get_var(parent.var, d[1:])
        else:
            yield d

    def create_tree(self, concept: Concept,
                    include_ambiguous: bool) -> Iterator[Constituent]:
        concepts_orig = self.concepts

        concept_uuid = concept.get_uuid(self)
        rules, ambiguities = self.find_rule(concept_uuid)
        if not rules:
            raise Exception(f"No rule found for {concept.get_uuid(self)}")

        for rule in rules:
            # Missing args? Substitute placeholder letters.
            if rule.num_args and not concept.args:
                concept = copy.copy(concept)
                concept.args = [ Concept(uuid.uuid1()) for i in range(rule.num_args) ]
                self.concepts = { **concepts_orig, **{
                    c.get_uuid(self): ConceptDefinition([{
                        'language': rule.language.code,
                        'data': {
                            'type': rule.args[i].expect_type,
                            'literal': 'XYZABC'[i % 6]
                        }
                    }], uuid=c.get_uuid(self))
                    for i, c in enumerate(concept.args)
                }}

            if len(concept.args) != rule.num_args:
                raise Exception("Got %d arguments, expected %d" %
                      (len(concept.args), rule.num_args))

            expansion: Union[Dict[str, Any], Concept]
            if not rule.expansion and len(concept.args) == 1:
                expansion = copy.deepcopy(concept.args[0])
                concept.add_attributes(expansion.attr)
            else:
                ex = rule.expansion.copy()
                concept.add_attributes(ex)
                expansion = ex

            for x in self.replace_arguments(expansion, concept, include_ambiguous):
                yield self.language.constituent_class.create(self, x)

        self.concepts = concepts_orig

        if include_ambiguous:
            for a in ambiguities:
                related_concept = copy.deepcopy(concept)
                related_concept._uuid = a
                meaning_id = self.generate_meaning_id()
                for t in self.create_tree(related_concept, False):
                    t.set_meaning(meaning_id)
                    yield t

    def deduplicate(self, trees: Iterator[Translation]) -> Iterator[Translation]:
        d: Dict[Tuple[str, int], Translation] = {}
        for tree in trees:
            trans = (str(tree), tree.meaning)
            if trans in d:
                d[trans].merge(tree)
            else:
                d[trans] = tree
        return iter(d.values())

    def do_translate(self, concept: Concept, include_ambiguous: bool
                     ) -> Iterator[Translation]:
        for tree in self.create_tree(concept, include_ambiguous):
            yield from tree.generate()

    # TODO Take lang parameter instead of having self.lang, allow None for all
    def translate(self, concept: Concept,
                  vocab_errors: Optional[Dict[UUID, UUID]] = None,
                  include_ambiguous: bool = False) -> Iterator[Translation]:
        self.vocab_errors = vocab_errors or {}
        self.next_meaning_id = 0
        trees = self.do_translate(concept, include_ambiguous)
        return self.deduplicate(trees)

class TranslationContextStatic(TranslationContext):
    pass
