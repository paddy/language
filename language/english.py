from typing import *

import abc
import copy
import enum
from enum import Enum
import json_util
from uuid import UUID

from . import language
from .language import (
    Language,
    DictWord, DictWordForm, Inflection,
    Constituent,
    TranslationContext, Translation, TTranslationSource)

from translation import gettext as _

@enum.unique
class Number(language.GrammaticalCategory):
    SINGULAR    = (0, _("Singular"), "sg")
    PLURAL      = (1, _("Plural"), "pl")

@enum.unique
class Person(language.GrammaticalCategory):
    FIRST       = (1, _("First"), "1")
    SECOND      = (2, _("Second"), "2")
    THIRD       = (3, _("Third"), "3")

@enum.unique
class Tense(language.GrammaticalCategory):
    PRESENT     = (0, _("Present"), "pres")
    PAST        = (1, _("Simple Past"), "past")

@enum.unique
class Gender(language.GrammaticalCategory):
    MASC        = (0, _("masculine"), "m")
    FEM         = (1, _("feminine"), "f")
    NEUT        = (2, _("neuter"), "n")

@enum.unique
class Case(language.GrammaticalCategory):
    NOMINATIVE  = (0, _("Nominative"), "nom")
    GENITIVE    = (1, _("Genitive"), "gen")
    OBJECTIVE   = (2, _("Objective"), "obj")

@enum.unique
class Definiteness(language.GrammaticalCategory):
    BARE        = (1, _("Bare"), "bare")
    INDEFINITE  = (2, _("Indefinite"), "indef")
    DEFINITE    = (3, _("Definite"), "def")

@enum.unique
class Dialect(language.Dialect):
    BRITISH     = (0, 'Britain', 'B')
    AMERICAN    = (1, 'America', 'A')
    IRISH       = (2, 'Ireland', 'I')

class EnglishAgreementInfo(Translation.AgreementInfo):
    def __init__(self, person: Person, number: Number) -> None:
        self.person = person
        self.number = number

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, EnglishAgreementInfo):
            return False
        if (self.person != other.person) or (self.number != other.number):
            return False
        return True

class EnglishConstituent(Constituent):
    pass

class EnglishDictWord(DictWord):
    pass

class EnglishVerb(EnglishDictWord):

    name = _("verb")

    class EnglishVerbForm(DictWordForm):
        def __init__(self, form: str,  dialects: Sequence[Dialect] = [],
                     neg: bool = False, contract_with_pronoun: bool = False) -> None:
            super().__init__(form)
            self.neg = neg
            self.dialects = dialects or [d for d in Dialect]
            self.contract_with_pronoun = contract_with_pronoun

    def get_verb_form_id(self, tense: Tense, person: Person, number: Number,
                         neg: bool = False) -> str:
        form_id = "%s-%s%s" % (tense.get_id(), person.get_id(), number.get_id())
        if neg:
            form_id += "-neg"
        return form_id

    def guess_pres_3sg(self, stem: 'EnglishVerb.EnglishVerbForm'
                      ) -> Sequence['EnglishVerb.EnglishVerbForm']:
        stem.text += 's'
        return (stem, )

    def guess_past(self, stem: 'EnglishVerb.EnglishVerbForm'
                   ) -> Sequence['EnglishVerb.EnglishVerbForm']:
        stem.text += 'ed'
        return (stem, )

    def guess_neg(self, stem: 'EnglishVerb.EnglishVerbForm'
                  ) -> Sequence['EnglishVerb.EnglishVerbForm']:
        if not stem.neg:
            stem.text += " not"
            stem.neg = True
        return (stem, )

    def __init__(self, dialects: Sequence[Dialect] = [],
                 do_support: bool = True, **kwargs: Any) -> None:
        self.dialects = dialects
        self.do_support = do_support

        self.infl = Inflection(self.EnglishVerbForm)
        self.infl.add("root", kwargs)
        self.infl.add("pres", kwargs, None, "root")
        self.infl.add("past", kwargs, self.guess_past, "root")
        self.infl.add("past-part", kwargs, None, "past")

        default_cb = {
            "pres-3sg": self.guess_pres_3sg,
        }
        for t in Tense:
            stem = t.get_id()
            stem_neg = "%s-neg" % stem
            if not self.do_support:
                self.infl.add(stem_neg, kwargs, self.guess_neg, stem,
                              { 'neg': True })
            for n in Number:
                for p in Person:
                    form_id = self.get_verb_form_id(t, p, n)
                    self.infl.add(form_id, kwargs, default_cb.get(form_id), stem)
                    if not self.do_support:
                        self.infl.add("%s-neg" % form_id, kwargs, self.guess_neg,
                                      (form_id, stem_neg), { 'neg': True })

        if len(kwargs):
            raise json_util.JSONParseException(
                _("Invalid key: '%s'") % next(iter(kwargs.keys())))

    def get_forms(self) -> Set[str]:
        return self.infl.get_forms()

    def dictionary_forms(self) -> List[Tuple[str, Union[str, Translation]]]:
        # TODO Pick the most standard form instead of the first one
        return [
            (_("Lemma"), Translation(english, self.infl["root"][0].text)),
            (_("Present"), next(self.conjugate(Tense.PRESENT, Person.THIRD, Number.SINGULAR))[1]),
            (_("Past"), next(self.conjugate(Tense.PAST, Person.THIRD, Number.SINGULAR))[1]),
        ]

    def inflection(self) -> Dict[str, Any]:
        info: Dict[str, Any] = {
            _("General"): {
                _("Lemma"): [ Translation(english, x.text, dialects=x.dialects)
                              for x in self.infl["root"]],
                _("Past participle"): [ Translation(english, x.text, dialects=x.dialects)
                                        for x in self.infl["past-part"] ],
            }
        }

        for t in Tense:
            info[t.get_name()] = {
                Number.SINGULAR.get_name(): {
                    p.get_name(): form for p in Person
                                       for _, form in self.conjugate(t, p, Number.SINGULAR)
                },
                Number.PLURAL.get_name(): [
                    form for _, form in self.conjugate(t, Person.FIRST, Number.PLURAL)
                ]
            }

        return info

    def conjugate(self, tense: Tense, person: Person, number: Number,
                  neg: bool = False, after_pronoun: bool = False,
                  ) -> Generator[Tuple['EnglishVerb.EnglishVerbForm',Translation], None, None]:
        form_id = self.get_verb_form_id(tense, person, number, neg)
        for f in self.infl[form_id]:
            if f.contract_with_pronoun and not after_pronoun:
                continue
            form = f.text
            # FIXME Allow overriding
            t = Translation(english, form, word=self, dialects=self.dialects,
                            dialect_feature_name=_("The word '%s'") % f.text,
                            contract_with_previous=f.contract_with_pronoun)
            yield f, t

class EnglishVP(EnglishConstituent):
    class VPType(Enum):
        STATEMENT = 0
        INFINITIVE = 1

    def __init__(self, ctx: TranslationContext,
                 verb: Union[str, UUID, None] = None,
                 neg: bool = False,
                 dialects: Optional[List[Dialect]] = None,
                 post: Optional[List[Any]] = None,
                 literal: Optional[str] = None,
                 vptype: Optional[VPType] = None,
                 subject: Optional[Any] = None,
                 object: Optional[Any] = None,
                 comp: Optional[Any] = None,
                 adverbial: Optional[List[Any]] = None) -> None:
        super().__init__(ctx, dialects, post)
        self.literal = literal
        self.tense = Tense.PRESENT

        if vptype is None:
            self.vptype = self.VPType.STATEMENT if subject else self.VPType.INFINITIVE
        else:
            self.vptype = vptype

        self.verb = None
        self.neg = neg
        self.subject: Optional[EnglishNP] = None
        self.object = None
        self.comp = None

        if not literal:
            assert verb is not None
            self.verb = ctx.get_word(verb, EnglishVerb)
            if subject:
                c = EnglishConstituent.create(ctx, subject)
                assert isinstance(c, EnglishNP)
                self.subject = c
            if object:
                self.object = EnglishConstituent.create(ctx, object)
            if comp:
                c = EnglishConstituent.create(ctx, comp)
                assert isinstance(c, EnglishVP)
                self.comp = c

        self.adverbials: List[Constituent] = []
        for x in adverbial or ():
            self.adverbials.append(EnglishConstituent.create(ctx, x))

    @classmethod
    def word_list(cls, d: Dict[str, Any]) -> List[str]:
        return [ d["verb"] ]

    def _word(self) -> str:
        assert self.verb
        return self.verb.infl['root'][0].text

    def _generate_dont(self, p: Person, n: Number) -> Generator[Translation, None, None]:
        # TODO Use regular definition of "do"
        if p == Person.THIRD and n == Number.SINGULAR:
            yield Translation(english, "doesn't")
            yield Translation(english, "does not")
        else:
            yield Translation(english, "don't")
            yield Translation(english, "do not")

    def _generate(self, to: bool = False) -> Generator[Translation, None, None]:
        if self.literal:
            yield Translation(english, self.literal)
            return

        assert self.verb
        neg_inflection = (self.vptype == self.VPType.STATEMENT and
                          self.neg and not self.verb.do_support)

        subjects: Iterable[Optional[Translation]] = [None]
        subject_ref = None
        if self.subject:
            subject_ref = self.subject.get_antecedent()
            subjects = self.subject.generate()

        for subject in subjects:
            verb_after_pronoun = False
            if not subject:
                assert self.vptype == self.VPType.INFINITIVE
                p = Person.THIRD
                n = Number.PLURAL
            elif subject_ref:
                info = subject.agreement_info[subject_ref]
                assert isinstance(info, EnglishAgreementInfo)
                (p, n) = (info.person, info.number)
            else:
                assert self.subject
                verb_after_pronoun = self.subject.is_pronoun()
                p = self.subject.get_person()
                n = self.subject.get_number()

            for _, verb in self.verb.conjugate(self.tense, p, n,
                                               neg=neg_inflection,
                                               after_pronoun=verb_after_pronoun):
                t: List[TTranslationSource] = []

                if subject:
                    t.append(subject)

                if self.vptype == self.VPType.INFINITIVE:
                    if self.neg:
                        t.append(Translation(english, "not"))
                    if to:
                        t.append(Translation(english, "to"))
                elif self.neg and self.verb.do_support:
                    t.append(self._generate_dont(p, n))
                t.append(verb)

                if self.object:
                    t.append(self.object.generate())

                if self.comp:
                    # FIXME This skips Constituent.generate()
                    t.append(self.comp._generate(to=True))

                for adv in self.adverbials:
                    t.append(adv.generate())

                if self.vptype == self.VPType.STATEMENT:
                    t.append(Translation(english, ".", contract_with_previous=True))

                is_sentence = self.vptype == self.VPType.STATEMENT
                yield from Translation.create(english, t, capitalise=is_sentence)

class EnglishNoun(EnglishDictWord):

    name = _("noun")

    class EnglishNounForm(DictWordForm):
        def __init__(self, form: str,  dialects: Sequence[Dialect] = []) -> None:
            super().__init__(form)
            self.dialects = dialects or [d for d in Dialect]
            self.starts_with_vowel_sound = form[0] in 'aeiou'

    def __init__(self, dialects: Sequence[Dialect] = [], **kwargs: Any) -> None:
        self.dialects = dialects

        self.infl = Inflection(self.EnglishNounForm)
        self.infl.add("sg", kwargs)
        self.infl.add("pl", kwargs, self.guess_plural, "sg")

        for n in Number:
            for c in Case:
                self.infl.add(self.get_noun_form_id(c, n), kwargs,
                              None, n.get_id())

        if len(kwargs):
            raise json_util.JSONParseException(
                _("Invalid key: '%s'") % next(iter(kwargs.keys())))

    def get_noun_form_id(self, case: Case, number: Number) -> str:
        return "%s-%s" % (case.get_id(), number.get_id())

    def guess_plural(self, sg: 'EnglishNoun.EnglishNounForm'
                     ) -> Sequence['EnglishNoun.EnglishNounForm']:
        if sg.text.endswith(('s', 'x', 'z', 'sh', 'ch')):
            sg.text += 'es'
        else:
            sg.text += 's'
        return (sg, )

    def get_forms(self) -> Set[str]:
        return self.infl.get_forms()

    def dictionary_forms(self) -> List[Tuple[str, Union[str, Translation]]]:
        # TODO Pick the most standard form instead of the first one
        return [
            (_("Singular"), next(self.decline(Number.SINGULAR))[1]),
            (_("Plural"), next(self.decline(Number.PLURAL))[1]),
        ]

    def inflection(self) -> Dict[str, Any]:
        declension = {
            name: form for name, form in self.dictionary_forms()
        }
        return {
            _("Declension"): declension
        }

    def decline(self, number: Number) -> Generator[Tuple['EnglishNoun.EnglishNounForm',Translation], None, None]:
        form_id = self.get_noun_form_id(Case.NOMINATIVE, number)
        for f in self.infl[form_id]:
            form = f.text
            t = Translation(english, form, word=self, dialects=self.dialects,
                            dialect_feature_name=_("The word '%s'") % form)
            yield f, t

class EnglishPronoun(EnglishNoun):

    name = _("pronoun")

    def __init__(self, person: Person, number: Number,
                 dialects: Sequence[Dialect] = [], **kwargs: Any) -> None:
        super().__init__(dialects, **kwargs)
        self.person = person
        self.number = number

    def dictionary_forms(self) -> List[Tuple[str, Union[str, Translation]]]:
        return [
            (_("Lemma"), next(self.decline(self.number))[1]),
        ]

class EnglishNP(EnglishConstituent):
    """ Noun Phrase """
    def __init__(self, ctx: TranslationContext, noun: Optional[str] = None,
                 literal: Optional[str] = None,
                 definite: Definiteness = Definiteness.BARE,
                 number: Number = Number.SINGULAR,
                 dialects: Optional[List[Dialect]] = None,
                 post: Optional[List[Any]] = None) -> None:

        super().__init__(ctx, dialects, post)
        if literal:
            self.noun = EnglishNoun(sg=literal, pl=literal)
        else:
            assert noun
            self.noun = ctx.get_word(noun, EnglishNoun)
        self.definite = definite
        self.number = number

    @classmethod
    def word_list(cls, d: Dict[str, Any]) -> List[str]:
        return [ d["noun"] ]

    def get_person(self) -> Person:
        if isinstance(self.noun, EnglishPronoun):
            return self.noun.person
        return Person.THIRD

    def get_number(self) -> Number:
        return self.number

    def is_pronoun(self) -> bool:
        return isinstance(self.noun, EnglishPronoun)

    def _do_generate(self, number: Number, definite: Definiteness) -> Generator[Translation, None, None]:
        for form, noun in self.noun.decline(number):
            words: List[TTranslationSource] = []
            if definite == Definiteness.DEFINITE:
                words.append("the")
            elif definite == Definiteness.INDEFINITE:
                if form.starts_with_vowel_sound:
                    words.append("an")
                else:
                    words.append("a")
            words.append(noun)
            yield from Translation.create(english, words)

    def _generate(self) -> Generator[Translation, None, None]:
        return self._do_generate(self.number, self.definite)

    def _word(self) -> str:
        return str(next(self._do_generate(Number.SINGULAR, Definiteness.BARE)))


class EnglishParticle(EnglishDictWord):

    name = _("particle")

    class EnglishParticleForm(DictWordForm):
        def __init__(self, form: str) -> None:
            self.text = form

    def __init__(self, **kwargs: Any) -> None:
        self.word = self.EnglishParticleForm.create(kwargs.pop("word"))

    def get_forms(self) -> Set[str]:
        return { f.text for f in self.word }

    def dictionary_forms(self) -> List[Tuple[str, Union[str, Translation]]]:
        return [
            (_("Word"), self.word[0].text),
        ]

    def inflection(self) -> Dict[str, Any]:
        return {
            _("General"): {
                _("Lemma"): [ Translation(english, f.text) for f in self.word ],
            },
        }

class EnglishPar(EnglishConstituent):
    def __init__(self, ctx: TranslationContext,
                 particle: Union[str, UUID],
                 dialects: Optional[List[Dialect]] = None,
                 post: Optional[List[Any]] = None) -> None:
        super().__init__(ctx, dialects, post)
        self.particle = ctx.get_word(particle, EnglishParticle)

    def _word(self) -> str:
        return self.particle.word[0].text

    @classmethod
    def word_list(cls, d: Dict[str, Any]) -> List[str]:
        return [ d["particle"] ]

    def _generate(self) -> Generator[Translation, None, None]:
        for w in self.particle.word:
            yield Translation(english, w.text, word=self.particle)

english = Language("en", _("English"), EnglishDictWord, EnglishConstituent, Dialect, Dialect.BRITISH) # type: ignore
