from typing import *

import abc
import enum
from enum import Enum
import json_util
from uuid import UUID

from . import language
from .language import (
    Language,
    DictWord, DictWordForm, Inflection,
    Constituent,
    TranslationContext, TranslationContextStatic,
    Translation, TTranslationSource)

from translation import gettext as _

@enum.unique
class Number(language.GrammaticalCategory):
    SINGULAR    = (0, _("Singular"), "sg")
    PLURAL      = (1, _("Plural"), "pl")

@enum.unique
class Person(language.GrammaticalCategory):
    FIRST       = (1, _("First"), "1")
    SECOND      = (2, _("Second"), "2")
    THIRD       = (3, _("Third"), "3")

@enum.unique
class Tense(language.GrammaticalCategory):
    PRESENT     = (0, _("Present"), "pres")
    PRETERITE   = (1, _("Preterite"), "pt")
    PERFECT     = (2, _("Perfect"), "pf")
    FUTURE      = (3, _("Future"), "fut")
    SUBJ_PRES   = (4, _("Konjunktiv I"), "konj1")
    SUBJ_PAST   = (5, _("Konjunktiv II"), "konj2")
    IMPERATIVE  = (6, _("Imperative"), "imp")

@enum.unique
class Gender(language.GrammaticalCategory):
    MASC        = (0, _("masculine"), "m")
    FEM         = (1, _("feminine"), "f")
    NEUT        = (2, _("neuter"), "n")

@enum.unique
class Case(language.GrammaticalCategory):
    NOMINATIVE  = (0, _("Nominative"), "nom")
    GENITIVE    = (1, _("Genitive"), "gen")
    DATIVE      = (2, _("Dative"), "dat")
    ACCUSATIVE  = (3, _("Accusative"), "acc")

@enum.unique
class DeclensionType(language.GrammaticalCategory):
    STRONG      = (1, _("Strong"), "str")
    WEAK        = (2, _("Weak"), "weak")
    MIXED       = (3, _("Mixed"), "mixed")

@enum.unique
class Definiteness(language.GrammaticalCategory):
    BARE        = (1, _("Bare"), "bare")
    INDEFINITE  = (2, _("Indefinite"), "indef")
    DEFINITE    = (3, _("Definite"), "def")

@enum.unique
class Dialect(language.Dialect):
    DE_SUED     = (0, 'Süddt.', 'S')
    DE_NORD     = (1, 'Norddt.', 'N')
    AT          = (2, 'Österreich', 'Ö')
    CH          = (3, 'Schweiz', 'C')

def is_vowel(s: str) -> bool:
    return s in 'aeiouyäöü'

# TODO Deduplicate with Irish
def split_last_vowel_group(word: str) -> Tuple[str, str, str]:
    i = len(word) - 1
    while i >= 0 and not is_vowel(word[i]):
        i -= 1
    if i < 0:
        return ('', '', word)

    last_vowel = i

    while i >= 0 and is_vowel(word[i]):
        i -= 1

    first_vowel = i + 1

    return (word[:first_vowel], word[first_vowel:last_vowel+1], word[last_vowel+1:])

def plural_umlaut(s: str) -> str:
    c1, v, c2 = split_last_vowel_group(s)

    umlaut_map = {
        'a': 'ä',
        'o': 'ö',
        'u': 'ü',
        'au': 'äu',
    }
    if v in umlaut_map:
        return c1 + umlaut_map[v] + c2
    else:
        return s

def verb_present_umlaut(s: str) -> str:
    c1, v, c2 = split_last_vowel_group(s)

    umlaut_map = {
        'a': 'ä',
        'e': 'i',
        'au': 'äu',
    }
    if v in umlaut_map:
        return c1 + umlaut_map[v] + c2
    else:
        return s

class GermanAgreementInfo(Translation.AgreementInfo):
    def __init__(self, person: Person, number: Number) -> None:
        self.person = person
        self.number = number

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, GermanAgreementInfo):
            return False
        if (self.person != other.person) or (self.number != other.number):
            return False
        return True

class GermanConstituent(Constituent):
    pass

class GermanDictWord(DictWord):
    pass

class GermanVerb(GermanDictWord):

    name = _("verb")

    class GermanVerbForm(DictWordForm):
        def __init__(self, form: str,  dialects: Sequence[Dialect] = []) -> None:
            self.prefix = None
            if '|' in form:
                self.prefix, form = form.split('|', 1)
            super().__init__(form)
            self.dialects = dialects or [d for d in Dialect]

    def get_verb_form_id(self, tense: Tense, person: Person, number: Number) -> str:
        form_id = "%s-%s%s" % (tense.get_id(), person.get_id(), number.get_id())
        return form_id

    def guess_form(self, ending: str, umlaut: bool
                   ) -> Callable[['GermanVerb.GermanVerbForm'],
                                 Sequence['GermanVerb.GermanVerbForm']]:
        def guess(stem: 'GermanVerb.GermanVerbForm'
                  ) -> Sequence['GermanVerb.GermanVerbForm']:
            if umlaut:
                stem.text = verb_present_umlaut(stem.text)
            if stem.text.endswith("e") and ending.startswith("e"):
                stem.text += ending[1:]
            elif stem.text.endswith(("s", "ß")) and ending.startswith("s"):
                stem.text += ending[1:]
            else:
                stem.text += ending
            return (stem, )
        return guess

    def guess_past(self, stem: 'GermanVerb.GermanVerbForm'
                   ) -> Sequence['GermanVerb.GermanVerbForm']:
        stem.text += 'te'
        return (stem, )

    def guess_part_pt(self, stem: 'GermanVerb.GermanVerbForm'
                      ) -> Sequence['GermanVerb.GermanVerbForm']:
        if self.strong:
            stem.text = 'ge' + stem.text + 'en'
        else:
            stem.text = 'ge' + stem.text + 't'
        return (stem, )

    def guess_root(self, stem: 'GermanVerb.GermanVerbForm'
                   ) -> Sequence['GermanVerb.GermanVerbForm']:
        if stem.text.endswith("en"):
            stem.text = stem.text[:-2]
        elif stem.text.endswith("ern") or stem.text.endswith("eln"):
            stem.text = stem.text[:-1]
        return (stem, )

    def __init__(self, dialects: Sequence[Dialect] = [],
                 strong: bool = False,
                 **kwargs: Any) -> None:
        self.dialects = dialects
        self.strong = strong

        self.infl = Inflection(self.GermanVerbForm)
        self.infl.add("inf", kwargs)
        self.infl.add("root", kwargs, self.guess_root, "inf")
        self.infl.add("pres", kwargs, None, "root")
        self.infl.add("pt", kwargs, self.guess_past, "root")
        self.infl.add("part-pt", kwargs, self.guess_part_pt, "root")

        endings = {
            Tense.PRESENT:   [ 'e', 'st', 't', 'en', 't', 'en' ],
            Tense.PRETERITE: [ '', 'st', '', 'en', 't', 'en' ],
        }
        for t in [Tense.PRESENT, Tense.PRETERITE]:
            stem = t.get_id()
            for n in Number:
                for p in Person:
                    form_id = self.get_verb_form_id(t, p, n)
                    umlaut = (strong and t == Tense.PRESENT and
                              n == Number.SINGULAR and p.value in (2, 3))
                    guess_cb = self.guess_form(
                        endings[t][3 * n.value + p.value - 1], umlaut)
                    self.infl.add(form_id, kwargs, guess_cb, stem)

        if len(kwargs):
            raise json_util.JSONParseException(
                _("Invalid key: '%s'") % next(iter(kwargs.keys())))

    def get_forms(self) -> Set[str]:
        return self.infl.get_forms()

    def dictionary_forms(self) -> List[Tuple[str, Union[str, Translation]]]:
        # TODO Pick the most standard form instead of the first one
        inf = self.infl["inf"][0]
        return [
            (_("Lemma"), Translation(german, (inf.prefix or '') + inf.text)),
            (_("Present"), next(self.inflected_form(Tense.PRESENT, Person.THIRD, Number.SINGULAR))),
            (_("Preterite"), next(self.inflected_form(Tense.PRETERITE, Person.THIRD, Number.SINGULAR))),
            #(_("Past participle"), next(self.inflected_form(Tense.PERFECT, Person.THIRD, Number.SINGULAR))),
        ]

    def inflected_form(self, tense: Tense, person: Person, number: Number
                       ) -> Generator[Translation, None, None]:
        for f, text in self.conjugate(tense, person, number, False):
            if f.prefix is not None:
                prefix = Translation(german, f.prefix)
                yield from Translation.create(german, [text, prefix])
            else:
                yield text

    def inflection(self) -> Dict[str, Any]:
        info: Dict[str, Any] = {
            _("General"): {
                _("Lemma"): [ Translation(german, (x.prefix or '') + x.text,
                                          dialects=x.dialects)
                              for x in self.infl["inf"]],
                _("Past participle"): [ Translation(german,
                                                    (x.prefix or '') + x.text,
                                                    dialects=x.dialects)
                                        for x in self.infl["part-pt"] ],
            }
        }

        for t in [Tense.PRESENT, Tense.PRETERITE]:
            info[t.get_name()] = {
                n.get_name(): {
                    p.get_name(): form for p in Person
                                       for form in self.inflected_form(t, p, n)
                }
                for n in Number
            }

        return info

    def conjugate(self, tense: Tense, person: Person, number: Number,
                  include_prefix: bool
                  ) -> Generator[Tuple['GermanVerb.GermanVerbForm',Translation], None, None]:
        form_id = self.get_verb_form_id(tense, person, number)
        for f in self.infl[form_id]:
            if include_prefix and f.prefix is not None:
                form = f.prefix + f.text
            else:
                form = f.text
            t = Translation(german, form, word=self, dialects=self.dialects,
                            dialect_feature_name=_("The word '%s'") % f.text)
            yield f, t

class GermanVP(GermanConstituent):
    class VPType(Enum):
        STATEMENT = 0
        INFINITIVE = 1

    def __init__(self, ctx: TranslationContext,
                 verb: Union[str, UUID, None] = None,
                 neg: bool = False,
                 dialects: Optional[List[Dialect]] = None,
                 post: Optional[List[Any]] = None,
                 literal: Optional[str] = None,
                 vptype: Optional[VPType] = None,
                 subject: Optional[Any] = None,
                 object: Optional[Any] = None,
                 predicative: Optional[Any] = None,
                 comp: Optional[Any] = None,
                 adverbial: Optional[List[Any]] = None) -> None:
        super().__init__(ctx, dialects, post)
        self.literal = literal
        self.tense = Tense.PRESENT

        if vptype is None:
            self.vptype = self.VPType.STATEMENT if subject else self.VPType.INFINITIVE
        else:
            self.vptype = vptype

        self.verb = None
        self.neg = neg
        self.subject: Optional[GermanNP] = None
        self.object = None
        self.predicative = None
        self.comp = None

        if not literal:
            assert verb is not None
            self.verb = ctx.get_word(verb, GermanVerb)
            if subject:
                c = GermanConstituent.create(ctx, subject)
                assert isinstance(c, GermanNP)
                self.subject = c
            if predicative:
                c = GermanConstituent.create(ctx, predicative)
                self.predicative = c
                if isinstance(self.predicative, GermanNP):
                    self.predicative.set_case(Case.NOMINATIVE)
                    if self.neg:
                        try:
                            self.predicative.set_neg(True)
                            self.neg = False
                        except GermanNP.NegationError:
                            pass
            if object:
                c = GermanConstituent.create(ctx, object)
                assert isinstance(c, GermanNP)
                self.object = c
                self.object.set_case(Case.ACCUSATIVE)
                if self.neg:
                    try:
                        self.object.set_neg(True)
                        self.neg = False
                    except GermanNP.NegationError:
                        pass
            if comp:
                c = GermanConstituent.create(ctx, comp)
                assert isinstance(c, GermanVP)
                self.comp = c

        self.adverbials: List[Constituent] = []
        for x in adverbial or ():
            self.adverbials.append(GermanConstituent.create(ctx, x))

    @classmethod
    def word_list(cls, d: Dict[str, Any]) -> List[str]:
        # FIXME Move to base class
        if isinstance(d["verb"], language.ConceptArg):
            return []
        else:
            return [ d["verb"] ]

    def _word(self) -> str:
        assert self.verb
        return self.verb.infl['root'][0].text

    def _generate(self, zu: bool = False) -> Generator[Translation, None, None]:
        if self.literal:
            yield Translation(german, self.literal)
            return

        assert self.verb

        subjects: Iterable[Optional[Translation]] = [None]
        subject_ref = None
        if self.subject:
            subject_ref = self.subject.get_antecedent()
            subjects = self.subject.generate()

        v2 = self.vptype == self.VPType.STATEMENT

        for subject in subjects:
            if not subject:
                assert self.vptype == self.VPType.INFINITIVE
                p = Person.THIRD
                n = Number.PLURAL
            elif subject_ref:
                info = subject.agreement_info[subject_ref]
                assert isinstance(info, GermanAgreementInfo)
                (p, n) = (info.person, info.number)
            else:
                assert self.subject
                p = self.subject.get_person()
                n = self.subject.get_number()

            for verb_f, verb in self.verb.conjugate(self.tense, p, n, not v2):
                t: List[TTranslationSource] = []

                if subject:
                    t.append(subject)

                if v2:
                    t.append(verb)

                if self.object and self.object.is_definite():
                    t.append(self.object.generate())

                if self.neg:
                    # TODO Make the position of "nicht" more flexible
                    t.append(Translation(german, "nicht"))

                if self.object and not self.object.is_definite():
                    t.append(self.object.generate())
                if self.predicative:
                    t.append(self.predicative.generate())

                for adv in self.adverbials:
                    t.append(adv.generate())

                if not v2:
                    if self.vptype == self.VPType.INFINITIVE and zu:
                        t.append(Translation(german, "zu"))
                    t.append(verb)
                elif verb_f.prefix is not None:
                    t.append(Translation(german, verb_f.prefix))

                if self.comp:
                    # FIXME This skips Constituent.generate()
                    t.append(self.comp._generate(zu=True))

                if zu and self.vptype == self.VPType.INFINITIVE and len(t) > 0:
                    t.insert(0, Translation(german, ",", contract_with_previous=True))
                elif self.vptype == self.VPType.STATEMENT:
                    t.append(Translation(german, ".", contract_with_previous=True))

                is_sentence = self.vptype == self.VPType.STATEMENT
                yield from Translation.create(german, t, capitalise=is_sentence)

class GermanNoun(GermanDictWord):

    name = _("noun")

    class GermanNounForm(DictWordForm):
        def __init__(self, form: str, gender: Optional[Gender] = None,
                     dialects: Sequence[Dialect] = []) -> None:
            super().__init__(form)
            if gender is None:
                self.gender = self.guess_gender()
            else:
                self.gender = gender
            self.dialects = dialects or [d for d in Dialect]

        def guess_gender(self) -> Gender:
            nomsg = self.text

            # Guess based on endings
            if any(nomsg.endswith(ending)
                   for ending in ['ant', 'ast', 'aum', 'en', 'el', 'er', 'ich', 'ig',
                                  'smus', 'ling', 'or', 'us']):
                return Gender.MASC
            if any(nomsg.endswith(ending)
                   for ending in ['anz', 'ei', 'enz', 'heit', 'ik', 'in', 'keit',
                                  'schaft', 'sion', 'tät', 'tion', 'ung', 'ur' ]):
                return Gender.FEM
            if any(nomsg.endswith(ending)
                   for ending in ['al', 'an', 'ar', 'är', 'at', 'chen', 'ent',
                                  'ett', 'ier', 'iv', 'o', 'on', 'lein', 'ma',
                                  'ment', 'nis', 'sal', 'sel', 'tel', 'tum', 'um']):
                return Gender.NEUT

            # TODO Monosyllabic nouns tend to be masculine
            if nomsg[-1] in 'ae':
                return Gender.FEM
            else:
                return Gender.MASC

    def guess_gen_sg(self, sg: 'GermanNoun.GermanNounForm'
                    ) -> Sequence['GermanNoun.GermanNounForm']:
        if sg.gender != Gender.FEM:
            if sg.text.endswith(('s', 'z')):
                sg.text += 'es'
            else:
                sg.text += 's'
        return (sg, )

    def guess_dat_pl(self, pl: 'GermanNoun.GermanNounForm'
                    ) -> Sequence['GermanNoun.GermanNounForm']:
        if not pl.text.endswith('n'):
            pl.text += 'n'
        return (pl, )

    def guess_plural(self, sg: 'GermanNoun.GermanNounForm'
                     ) -> Sequence['GermanNoun.GermanNounForm']:
        if sg.gender == Gender.FEM:
            if sg.text.endswith('in'):
                sg.text += 'nen'
            elif sg.text.endswith('e'):
                sg.text += 'n'
            else:
                sg.text += 'en'
        elif sg.text.endswith(('a', 'o', 'u')):
            sg.text += 's'
        elif sg.text.endswith(('chen', 'lein', 'el', 'er')):
            sg.text = sg.text
        else:
            sg.text = plural_umlaut(sg.text) + 'e'
        return (sg, )

    def __init__(self, dialects: Sequence[Dialect] = [],
                genders: Sequence[Gender] = (), **kwargs: Any) -> None:
        self.dialects = dialects

        self.infl = Inflection(self.GermanNounForm)

        n = Number.SINGULAR
        self.infl.add("sg", kwargs)
        case_guess_fn_sg = {
            Case.GENITIVE: self.guess_gen_sg,
        }
        for c in Case:
            self.infl.add(self.get_noun_form_id(c, n), kwargs,
                          case_guess_fn_sg.get(c), n.get_id())

        n = Number.PLURAL
        self.infl.add("pl", kwargs, self.guess_plural, "sg")
        case_guess_fn_pl = {
            Case.DATIVE: self.guess_dat_pl,
        }
        for c in Case:
            self.infl.add(self.get_noun_form_id(c, n), kwargs,
                          case_guess_fn_pl.get(c), n.get_id())

        if len(kwargs):
            raise json_util.JSONParseException(
                _("Invalid key: '%s'") % next(iter(kwargs.keys())))

    def get_noun_form_id(self, case: Case, number: Number) -> str:
        return "%s-%s" % (case.get_id(), number.get_id())

    def get_forms(self) -> Set[str]:
        return self.infl.get_forms()

    def get_numbers(self) -> Sequence[Number]:
        return tuple(n for n in Number)

    def dictionary_forms(self) -> List[Tuple[str, Union[str, Translation]]]:
        # TODO Pick the most standard form instead of the first one
        return [
            (_("Singular"), next(self.decline(Case.NOMINATIVE, Number.SINGULAR))[1]),
            (_("Plural"), next(self.decline(Case.NOMINATIVE, Number.PLURAL))[1]),
        ]

    def inflection(self) -> Dict[str, Any]:
        ctx = TranslationContextStatic(german, {self.uuid: self}, {})

        def inflected_form(c: Case, n: Number) -> List[Translation]:
            np = GermanNP(ctx, noun=str(self.uuid), case=c, number=n,
                          definite=Definiteness.DEFINITE)
            return list(np._generate())

        declension = {
            n.get_name(): {
                c.get_name(): inflected_form(c, n)
                for c in Case
            }
            for n in self.get_numbers()
        }
        return {
            _("Declension"): declension
        }

    def decline(self, case: Case, number: Number) -> Generator[Tuple['GermanNoun.GermanNounForm',Translation], None, None]:
        form_id = self.get_noun_form_id(case, number)
        for f in self.infl[form_id]:
            form = f.text
            t = Translation(german, form, word=self, dialects=self.dialects,
                            dialect_feature_name=_("The word '%s'") % form)
            yield f, t

class GermanPronoun(GermanNoun):

    name = _("pronoun")

    def __init__(self, person: Person, number: Number,
                 dialects: Sequence[Dialect] = [], **kwargs: Any) -> None:
        super().__init__(dialects, **kwargs)
        self.person = person
        self.number = number

    def dictionary_forms(self) -> List[Tuple[str, Union[str, Translation]]]:
        return [
            (c.name, next(self.decline(c, self.number))[1])
            for c in Case
        ]

    def get_numbers(self) -> Sequence[Number]:
        return (self.number, )

class GermanNP(GermanConstituent):
    """ Noun Phrase """

    class NegationError(Exception):
        pass

    def __init__(self, ctx: TranslationContext, noun: Optional[str] = None,
                 literal: Optional[str] = None,
                 definite: Definiteness = Definiteness.BARE,
                 number: Number = Number.SINGULAR,
                 case: Optional[Case] = None,
                 dialects: Optional[List[Dialect]] = None,
                 attributes: Optional[List[Any]] = None,
                 post: Optional[List[Any]] = None) -> None:

        super().__init__(ctx, dialects, post)
        if literal:
            self.noun = GermanNoun(sg=literal, pl=literal)
        else:
            assert noun
            self.noun = ctx.get_word(noun, GermanNoun)
        self.definite = definite
        self.number = number
        self.case = case
        self.neg = False

        self.attributes: List[GermanConstituent] = []
        for x in attributes or []:
            self.attributes.append(GermanConstituent.create(ctx, x))

    @classmethod
    def word_list(cls, d: Dict[str, Any]) -> List[str]:
        return [ d["noun"] ]

    def set_case(self, case: Case) -> None:
        if self.case is None:
            self.case = case

    def get_case(self) -> Case:
        if self.case is None:
            return Case.NOMINATIVE
        return self.case

    def set_neg(self, neg: bool) -> None:
        if not neg:
            self.neg = False
        elif self.definite == Definiteness.INDEFINITE:
            self.neg = True
        else:
            raise self.NegationError()

    def get_person(self) -> Person:
        if isinstance(self.noun, GermanPronoun):
            return self.noun.person
        return Person.THIRD

    def get_number(self) -> Number:
        return self.number

    def is_definite(self) -> bool:
        return self.definite == Definiteness.DEFINITE

    def generate_article(self, case: Case, number: Number, gender: Gender,
                         definite: Definiteness) ->  Generator[Translation, None, None]:
        def_art = {
            Number.SINGULAR: {
                Case.NOMINATIVE: ['der', 'die', 'das'],
                Case.GENITIVE:   ['des', 'der', 'des'],
                Case.DATIVE:     ['dem', 'der', 'dem'],
                Case.ACCUSATIVE: ['den', 'die', 'das'],
            },
            Number.PLURAL: {
                Case.NOMINATIVE: ['die', 'die', 'die'],
                Case.GENITIVE:   ['der', 'der', 'der'],
                Case.DATIVE:     ['den', 'den', 'den'],
                Case.ACCUSATIVE: ['die', 'die', 'die'],
            },
        }

        indef_art = {
            Number.SINGULAR: {
                Case.NOMINATIVE: ['ein',   'eine',  'ein'],
                Case.GENITIVE:   ['eines', 'einer', 'eines'],
                Case.DATIVE:     ['einem', 'einer', 'einem'],
                Case.ACCUSATIVE: ['einen', 'eine',  'ein'],
            },
        }

        if definite == Definiteness.DEFINITE:
            yield Translation(german, def_art[number][case][gender])
        else:
            assert definite == Definiteness.INDEFINITE
            assert number == Number.SINGULAR
            form = indef_art[number][case][gender]
            if self.neg:
                form = "k" + form
            yield Translation(german, form)

    def _do_generate(self, case: Case, number: Number,
                     definite: Definiteness) -> Generator[Translation, None, None]:
        if definite == Definiteness.DEFINITE:
            dt = DeclensionType.WEAK
        elif definite == Definiteness.BARE:
            dt = DeclensionType.STRONG
        else:
            dt = DeclensionType.MIXED

        for form, noun in self.noun.decline(case, number):
            result: List[TTranslationSource] = []
            if (not isinstance(self.noun, GermanPronoun)
                and (definite == Definiteness.DEFINITE
                     or (definite == Definiteness.INDEFINITE
                         and number == Number.SINGULAR))):
                result += self.generate_article(
                    case, number, form.gender, definite)

            for attr in self.attributes:
                if isinstance(attr, GermanAP):
                    attr.set_parent_info(case, number, form.gender, dt, False)
                result.append(attr.generate())

            result.append(noun)
            yield from Translation.create(german, result)

    def _generate(self) -> Generator[Translation, None, None]:
        return self._do_generate(self.get_case(), self.number, self.definite)

    def _word(self) -> str:
        return str(next(self._do_generate(Case.NOMINATIVE, Number.SINGULAR,
                                          Definiteness.BARE)))

class GermanAdjective(GermanDictWord):

    name = _("adjective")

    class GermanAdjectiveForm(DictWordForm):
        def __init__(self, form: str, dialects: Sequence[Dialect] = []) -> None:
            super().__init__(form)
            self.dialects = dialects or [d for d in Dialect]

    def guess_form(self, ending: str
                   ) -> Callable[['GermanAdjective.GermanAdjectiveForm'],
                                 Sequence['GermanAdjective.GermanAdjectiveForm']]:
        def guess(stem: 'GermanAdjective.GermanAdjectiveForm'
                  ) -> Sequence['GermanAdjective.GermanAdjectiveForm']:
            stem.text += ending
            return (stem, )
        return guess

    def get_adj_form_id(self, case: Case, number: Number, gender: Gender,
                        declension_type: DeclensionType) -> str:
        if number == Number.SINGULAR:
            form_id = "%s-%s-%s-%s" % (case.get_id(), number.get_id(), gender.get_id(),
                                       declension_type.get_id())
        else:
            form_id = "%s-%s-%s" % (case.get_id(), number.get_id(),
                                    declension_type.get_id())
        return form_id

    def __init__(self, dialects: Sequence[Dialect] = [],
                genders: Sequence[Gender] = (), **kwargs: Any) -> None:
        self.dialects = dialects

        self.infl = Inflection(self.GermanAdjectiveForm)
        self.infl.add("root", kwargs)

        sg_endings = {
            Gender.MASC: {
                DeclensionType.STRONG:  [ 'er', 'en', 'em', 'en' ],
                DeclensionType.WEAK:    [ 'e', 'en', 'en', 'en' ],
                DeclensionType.MIXED:   [ 'er', 'en', 'en', 'en' ],
            },
            Gender.FEM: {
                DeclensionType.STRONG:  [ 'e', 'er', 'er', 'e' ],
                DeclensionType.WEAK:    [ 'e', 'en', 'en', 'e' ],
                DeclensionType.MIXED:   [ 'e', 'en', 'en', 'e' ],
            },
            Gender.NEUT: {
                DeclensionType.STRONG:  [ 'es', 'en', 'em', 'es' ],
                DeclensionType.WEAK:    [ 'e', 'en', 'en', 'e' ],
                DeclensionType.MIXED:   [ 'es', 'en', 'en', 'es' ],
            },
        }
        pl_endings = {
            DeclensionType.STRONG:  [ 'e', 'er', 'en', 'e' ],
            DeclensionType.WEAK:    [ 'en', 'en', 'en', 'en' ],
            DeclensionType.MIXED:   [ 'en', 'en', 'en', 'en' ],
        }

        n = Number.SINGULAR
        for g in Gender:
            for dt in DeclensionType:
                for c in Case:
                    self.infl.add(self.get_adj_form_id(c, n, g, dt), kwargs,
                                  self.guess_form(sg_endings[g][dt][c.value]),
                                  "root")

        n = Number.PLURAL
        for dt in DeclensionType:
            for c in Case:
                self.infl.add(self.get_adj_form_id(c, n, Gender.NEUT, dt), kwargs,
                              self.guess_form(pl_endings[dt][c.value]),
                              "root")

        if len(kwargs):
            raise json_util.JSONParseException(
                _("Invalid key: '%s'") % next(iter(kwargs.keys())))

    def get_forms(self) -> Set[str]:
        return self.infl.get_forms()

    def dictionary_forms(self) -> List[Tuple[str, Union[str, Translation]]]:
        root = self.infl["root"][0]
        return [
            (_("Lemma"), Translation(german, root.text)),
        ]

    def inflection(self) -> Dict[str, Any]:
        ctx = TranslationContextStatic(german, {self.uuid: self}, {})

        def inflected_form(c: Case, n: Number, g: Gender,
                           dt: DeclensionType) -> List[Translation]:
            ap = GermanAP(ctx, adjective=str(self.uuid))
            ap.set_parent_info(c, n, g, dt, False)
            return list(ap._generate())

        declension = {
            dt.get_name(): {
                Number.SINGULAR.get_name(): {
                    c.get_name(): {
                        g.get_name(): inflected_form(c, Number.SINGULAR, g, dt)
                        for g in Gender
                    }
                    for c in Case
                },
                Number.PLURAL.get_name(): {
                    c.get_name(): inflected_form(c, Number.PLURAL, Gender.NEUT, dt)
                    for c in Case
                }
            }
            for dt in DeclensionType
        }
        return {
            _("Declension"): declension
        }

    def get_form(self, form_id: str) -> Iterator[Tuple['GermanAdjective.GermanAdjectiveForm',Translation]]:
        for f in self.infl[form_id]:
            form = f.text
            t = Translation(german, form, word=self, dialects=self.dialects,
                            dialect_feature_name=_("The word '%s'") % form)
            yield f, t

    def decline(self, case: Case, number: Number, gender: Gender,
                dt: DeclensionType) -> Iterator[Tuple['GermanAdjective.GermanAdjectiveForm',Translation]]:
        form_id = self.get_adj_form_id(case, number, gender, dt)
        yield from self.get_form(form_id)

class GermanAP(GermanConstituent):
    """ Adjective Phrase """
    def __init__(self, ctx: TranslationContext,
                 adjective: Union[str, UUID, None] = None,
                 dialects: Optional[List[Dialect]] = None,
                 post: Optional[List[Any]] = None,
                 literal: Optional[str] = None) -> None:
        super().__init__(ctx, dialects, post)
        if literal:
            self.adjective = GermanAdjective(root=literal)
        else:
            assert adjective is not None
            self.adjective = ctx.get_word(adjective, GermanAdjective)

        self.set_parent_info(Case.NOMINATIVE, Number.SINGULAR, Gender.MASC,
                             DeclensionType.STRONG, True)

    def set_parent_info(self, c: Case, n: Number, g: Gender,
                        dt: DeclensionType, predicative: bool) -> None:
        self.case = c
        self.number = n
        self.gender = g
        self.declension_type = dt
        self.predicative = predicative

    @classmethod
    def word_list(cls, d: Dict[str, Any]) -> List[str]:
        return [ d["adjective"] ]

    def _word(self) -> str:
        return str(next(self.adjective.get_form('root'))[0])

    def _generate(self) -> Generator[Translation, None, None]:
        if self.predicative:
            forms = self.adjective.get_form('root')
        else:
            forms = self.adjective.decline(self.case, self.number, self.gender,
                                           self.declension_type)

        for f, t in forms:
            yield t

class GermanParticle(GermanDictWord):

    name = _("particle")

    class GermanParticleForm(DictWordForm):
        def __init__(self, form: str) -> None:
            self.text = form

    def __init__(self, **kwargs: Any) -> None:
        self.word = self.GermanParticleForm.create(kwargs.pop("word"))

    def get_forms(self) -> Set[str]:
        return { f.text for f in self.word }

    def dictionary_forms(self) -> List[Tuple[str, Union[str, Translation]]]:
        return [
            (_("Word"), self.word[0].text),
        ]

    def inflection(self) -> Dict[str, Any]:
        return {
            _("General"): {
                _("Lemma"): [ Translation(german, f.text) for f in self.word ],
            },
        }

class GermanPar(GermanConstituent):
    def __init__(self, ctx: TranslationContext,
                 particle: Union[str, UUID],
                 dialects: Optional[List[Dialect]] = None,
                 post: Optional[List[Any]] = None) -> None:
        super().__init__(ctx, dialects, post)
        self.particle = ctx.get_word(particle, GermanParticle)

    def _word(self) -> str:
        return self.particle.word[0].text

    @classmethod
    def word_list(cls, d: Dict[str, Any]) -> List[str]:
        return [ d["particle"] ]

    def _generate(self) -> Generator[Translation, None, None]:
        for w in self.particle.word:
            yield Translation(german, w.text, word=self.particle)

german = Language("de", _("German"), GermanDictWord, GermanConstituent, Dialect, Dialect.DE_SUED) # type: ignore
