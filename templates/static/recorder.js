function Recorder(stream) {
    this.enabled = false;
    this.playing = false;
    this.recording = false;
    this.recorded = [];
    this.totalDuration = 0;
    this.totalVolume = 0;
    this.recentVolumes = [];
    this.totalRecentDuration = 0;
    this.totalRecentVolume = 0;

    this.onRecordingStarted = function() {}
    this.onRecordingCompleted = function() {}
    this.onPlayCompleted = function() {}

    this.onaudioprocess = function(ev) {

        /* If we are in the process of playing the recorded buffer, complete
         * that before recording something new */
        if (this.playing) {
            if (this.recorded.length) {
                buffer = this.recorded.shift();
                this.totalDuration -= buffer.duration;
                this.totalVolume -= buffer.volume;
                ev.outputBuffer.copyToChannel(buffer.data, 0);
                return;
            } else {
                this.playing = false;
                this.onPlayCompleted();
            }
        }

        if (!this.enabled) {
            return;
        }

        /* Add new data to the list of recorded buffers */
        var inData = ev.inputBuffer.getChannelData(0);
        var outData = ev.outputBuffer.getChannelData(0);

        var volume = 0
        for (var i = 0; i < ev.inputBuffer.length; i++) {
            volume += Math.abs(inData[i]);
            outData[i] = 0;
        }

        this.recorded.push({
            data: inData,
            volume: volume,
            duration: ev.inputBuffer.duration,
        })
        this.recentVolumes.push({
            volume: volume,
            duration: ev.inputBuffer.duration,
        })

        this.totalDuration += ev.inputBuffer.duration;
        this.totalVolume += volume;
        this.totalRecentDuration += ev.inputBuffer.duration;
        this.totalRecentVolume += volume;

        /* Keep stats for 0.5 seconds in "recent" log */
        while (this.totalRecentDuration > 0.5) {
            buffer = this.recentVolumes.shift()
            this.totalRecentDuration -= buffer.duration;
            this.totalRecentVolume -= buffer.volume;
        }

        /* Silence means that we should stop recording */
        if (this.recording && this.totalRecentVolume < 5 * this.recentVolumes.length) {
            this.recording = false;
            this.enabled = false;

            /* Remove silence from the end */
            do {
                buffer = this.recorded.pop();
                this.totalDuration -= buffer.duration;
                this.totalVolume -= buffer.volume;
            } while (buffer.volume < 5);

            this.onRecordingCompleted();
            return;
        }

        /* If we have enough activity, start recording */
        if (!this.recording && this.totalRecentVolume > 25 * this.recentVolumes.length) {
            this.recording = true;
            this.onRecordingStarted();
        }

        /* Keep only the last 0.5 seconds if we're not recording yet */
        while (this.totalDuration > 0.5 && !this.recording) {
            buffer = this.recorded.shift()
            this.totalDuration -= buffer.duration;
            this.totalVolume -= buffer.volume;
        }
    }

    this.enableRecording = function() {
        this.enabled = true;
    }
    this.play = function() {
        this.playing = true;
    }

    var AudioContext = window.AudioContext || window.webkitAudioContext;
    var audioCtx = new AudioContext();
    var source = audioCtx.createMediaStreamSource(stream);
    var processor = audioCtx.createScriptProcessor(4096, 1, 1);

    var instance = this;
    processor.onaudioprocess = function(ev) {
        instance.onaudioprocess(ev);
    }

    source.connect(processor);
    processor.connect(audioCtx.destination);
}
