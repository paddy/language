#!/usr/bin/env python3

from typing import *
import re

import bottle
from bottle import route, run, template, request, static_file, hook
import database

from user import User, UserSession

from translation import gettext as _

@no_type_check
@route('/admin/users')
def users():
    session = UserSession.get_current()
    if not session or not session.user.is_admin():
        return template('error', session=session, msg=_("Permission denied."))

    users = User.select()
    return template('admin/users', session=session, users=users)


@no_type_check
@route('/admin/add_user', ['POST', 'GET'])
def add_user():
    session = UserSession.get_current()
    if not session or not session.user.is_admin():
        return template('error', session=session, msg=_("Permission denied."))

    username = request.forms.get("user")
    password = request.forms.get("password")

    error = None

    if not re.search("^[A-Za-z0-9.-]+$", username):
        error = _("Invalid user name")
    elif User.get_or_none(User.name == username):
        error = _("This user name is already taken")

    if not error:
        try:
            user = User(name=username, password=User.hash_password(password))
            user.save()
        except Exception as e:
            error = str(e)

    return template('admin/add_user', session=session, username=username, error=error)
