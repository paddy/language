from typing import *
import abc

import database
import peewee
from peewee import TextField, TimestampField

import dictionary
from dictionary import DictionaryDefinition, DictionaryEntry
from language.language import (
    Language, Dialect, DictWord, DictWordAudio, Concept,
    Translation, TranslationPart, TTranslationSource)
from repository import (DBStored, DBConcept, DBCourse, DBPhrase,
                        DBTranslationContext, DBWord,
                        Repository, UserGrammarTags, UserWords)
from user import User

from collections import defaultdict
from datetime import datetime, timedelta
from difflib import SequenceMatcher
import json
import json_util
from uuid import UUID
import string
import re
import random

from translation import gettext as _

def check_answer_str(correct: str, given: str) -> bool:
    correct = correct.lower().translate(str.maketrans('', '', string.punctuation))
    given = given.lower().translate(str.maketrans('', '', string.punctuation))
    return correct == given

class ExerciseResult:
    def __init__(self, given: Translation, correct: bool,
                 closest_correction: Optional[Translation],
                 best_correction: Optional[str], hints: List[str]) -> None:
        if correct:
            closest_correction = None
        elif str(closest_correction) == best_correction:
            best_correction = None

        self.given = given
        self.correct = correct
        self.closest_correction = closest_correction
        self.best_correction = best_correction
        self.hints = hints

class ExerciseQuestion:
    def __init__(self, question: Optional[Translation],
                 instruction: Optional[str] = None,
                 audio: Optional[str] = None) -> None:
        self.instruction = instruction
        self.question = question
        self.audio = audio

class ExerciseIntroduceWord(ExerciseQuestion):
    def __init__(self, word: DictWord, src_lang: Language,
                 defs: List[DictionaryDefinition],
                 audio: Optional[DictWordAudio] = None) -> None:
        super().__init__(None)
        self.word = word
        self.src_lang = src_lang
        self.defs = defs
        self.word_audio = audio

class ExerciseFreeformQuestion(ExerciseQuestion):
    def __init__(self, question: Translation,
                 instruction: Optional[str] = None,
                 audio: Optional[str] = None,
                 default: Optional[str] = None) -> None:
        super().__init__(question, instruction, audio=audio)
        self.default = default

class ExerciseMultipleChoiceQuestion(ExerciseQuestion):
    def __init__(self, question: Translation,
                 answers: Sequence[str],
                 instruction: Optional[str] = None,
                 audio: Optional[str] = None) -> None:
        super().__init__(question, instruction, audio=audio)
        self.answers = answers

class TranslationComparison:
    def __init__(self, repository: Repository, given: str,
                 correct: Translation) -> None:
        self.repository = repository
        self.correct = correct
        self.lang = correct.lang
        self.given = self.answer_str_to_translations(given, correct)

        self.wrong_words: Set[DictWord] = set()
        self.wrong_grammar: Set[str] = set()

        self.add_word_uuids()
        self.do_correction()

        self.correct_words = { p.word for p in self.correct.parts if p.word } - self.wrong_words
        self.correct_grammar = correct.all_grammar_tags() - self.wrong_grammar

        print(f"""
--- result ---
words:
    correct: {"; ".join(w.lemma() for w in self.correct_words)}
    wrong:   {"; ".join(w.lemma() for w in self.wrong_words)}

grammar:
    correct: {self.correct_grammar}
    wrong:   {self.wrong_grammar}
---
""")

    def find_word_split(self, text: str, p: TranslationPart) -> int:
        if p.contract_with_previous:
            return text.find(p.text)
        else:
            match = re.search('(\s|[.,:;\'"])+%s' % re.escape(p.text), text)
            if not match:
                return -1
            return match.start()

    def guess_word_split(self, text: str) -> List[Translation]:
        words = re.split('(\s|[.,:;\'"])+', text)
        translations = []

        contract = 0
        for i, w in enumerate(words):
            if w:
                if re.fullmatch('\s+', w):
                   continue
                elif re.fullmatch('(\s|[.,:;\'"])+', w):
                   contract = 2

                translations.append(
                    Translation(self.lang, w,
                                contract_with_previous=bool(contract)))
                if contract:
                    contract -= 1

        return translations

    def answer_str_to_translations(self, given: str,
                                   correct: Translation) -> Translation:
        """
        Convert a string (with a user-provided answer) to a Translation object
        that is split into individual words in a way that it resembles the
        TranslationPart structure of the given correct solution (including
        splitting of contracted forms).
        """

        # If True, given and correct have diverged, so in the next iteration
        # try to find a point where both match again
        recover = False

        given_parts: List[TTranslationSource] = []
        remaining = given.strip()

        parts = correct.parts
        for p, next_p in zip(parts, [*parts[1:], None]):
            if recover:
                # If the next correct word appears somewhere in the given answer,
                # continue there and guess the word split for anything in between
                # (word boundaries at whitespace and punctuation).
                index = self.find_word_split(remaining, p)
                if index != -1:
                    given_parts += self.guess_word_split(remaining[:index])
                    remaining = remaining[index:]
                recover = False

            # Mismatch between given and correct
            if not remaining.startswith(p.text):
                recover = True
                continue

            # correct starts with the right text, but it's not a full word yet,
            # so we have to count this as a mismatch, too
            new_remaining = remaining[len(p.text):]
            if next_p and not next_p.contract_with_previous and new_remaining[0:1] != ' ':
                recover = True
                continue

            # The next word in correct and given matches
            given_parts.append(Translation(correct.lang, p.text, word=p.word))
            remaining = new_remaining.strip()

        # If the correct solution is shorter than the given answer, guess the
        # word split for the rest
        if remaining:
            given_parts += self.guess_word_split(remaining)

        translations = list(Translation.create(correct.lang, given_parts or [""]))
        assert len(translations) == 1

        return translations[0]

    def add_word_uuids(self) -> None:
        for p in self.given.parts:
            if not p.word:
                words = DBWord.get_by_form(self.repository, p.text)
                # TODO Handle ambiguous forms
                if len(words) == 1:
                    p.word = words[0].dict_word()

    def find_part(self, parts: Sequence[TranslationPart], text: str,
                  word: Optional[DictWord]) -> Optional[TranslationPart]:
        for p in parts[::-1]:
            if check_answer_str(p.text, text):
                return p
            if word and p.word and word.uuid == p.word.uuid:
                return p
        return None

    def do_correction(self) -> None:
        given_parts = [p for p in reversed(self.given.parts)]
        correct_parts = [p for p in reversed(self.correct.parts)]
        extra_given: List[TranslationPart] = []

        print("--- COMPARING ---")
        while given_parts:
            print(f"given: {given_parts}, correct: {correct_parts}")
            top = given_parts.pop()
            assert not top.corrected

            # Extra word given
            if not correct_parts:
                print(f"    extra word given: {top.text}")
                top.corrected = True
                continue

            c_top = correct_parts[-1]

            # Exact match
            if check_answer_str(top.text, c_top.text):
                print(f"    exact match: {top.text}")
                correct_parts.pop()
                continue

            # Wrong word form, but the same word
            if top.word and c_top.word and top.word.uuid == c_top.word.uuid:
                top.corrected = True
                c_top.corrected = True
                self.wrong_grammar |= c_top.grammar_tags
                correct_parts.pop()
                print(f"    wrong form: {top.text} != {c_top.text}")
                continue

            # We already saw c_top in a different position
            g_part = self.find_part(extra_given, c_top.text, c_top.word)
            if g_part:
                print(f"    skip: {c_top.text}")
                g_part.corrected = True
                extra_given.remove(g_part)
                given_parts.append(top)
                self.wrong_grammar |= self.correct.sentence_grammar_tags
                correct_parts.pop()
                continue

            # Both given and correct word exist in the other translation, but
            # in different places (probably wrong order)
            c_part = self.find_part(correct_parts, top.text, top.word)
            g_part = self.find_part(given_parts, c_top.text, c_top.word)
            if c_part and g_part:
                print(f"    wrong position (xG): {top.text} == {c_part.text}")
                print(f"    wrong position (xC): {g_part.text} == {c_top.text}")
                top.corrected = True
                c_top.corrected = True
                extra_given.append(top)
                self.wrong_grammar |= self.correct.sentence_grammar_tags
                correct_parts.pop()
                continue

            # The given word occurs somewhere else in the correct solution
            # (maybe a missing word in the given answer)
            if c_part:
                extra_given.append(top)
                self.wrong_grammar |= self.correct.sentence_grammar_tags
                print(f"    wrong position (G): {top.text} == {c_part.text}")
                continue

            # The correct word occurs somewhere else in the given solution
            # (assume that an extra word was given)
            if g_part:
                top.corrected = True
                print(f"    wrong position (C): {g_part.text} == {c_top.text}")
                continue

            # Just two different words
            print(f"    wrong word: {top.text} == {c_top.text}")
            top.corrected = True
            c_top.corrected = True
            if c_top.word:
                self.wrong_words.add(c_top.word)
            correct_parts.pop()

        # Completely missing words
        while correct_parts:
            c_top = correct_parts.pop()
            c_top.corrected = True

            g_part = self.find_part(extra_given, c_top.text, c_top.word)
            if g_part:
                extra_given.remove(g_part)
                print(f"    wrong position (X): {g_part.text} == {c_top.text}")
            elif c_top.word:
                self.wrong_words.add(c_top.word)
                print(f"    extra word in correct: {c_top.text}")

        # Extra words that weren't picked up later
        while extra_given:
            top = extra_given.pop()
            self.wrong_grammar |= self.correct.sentence_grammar_tags
            top.corrected = True
            print(f"    extra word wasn't just in the wrong place: {top.text}")

class Exercise(metaclass=abc.ABCMeta):
    def __init__(self, id: int, q_dialect: Dialect, q_most_unique: bool,
                 a_dialect: Dialect, a_most_unique: bool = False,
                 phrase: Optional[str] = None,
                 phrase_param: int = 0) -> None:
        self.id = id
        assert type(id) is int
        self.q_dialect = q_dialect
        self.q_most_unique = q_most_unique
        self.a_dialect = a_dialect
        self.a_most_unique = a_most_unique
        self.phrase = phrase
        self.phrase_param = phrase_param

    @abc.abstractmethod
    def get_question(self) -> ExerciseQuestion:
        pass

    @abc.abstractmethod
    def check_answer(self, user: Optional[User], answer: str
                     ) -> Optional[ExerciseResult]:
        pass

    def select_variant(self, ctx: DBTranslationContext,
                       variants: Sequence[Union[str, Concept]],
                       known_words: Optional[Dict[UUID, float]]
                       ) -> Tuple[Translation, List[int]]:
        trans: List[Translation] = []
        bad_trans: List[Translation] = []
        meanings: Dict[str, List[int]] = defaultdict(list)

        for v in variants:
            if isinstance(v, str):
                trans.append(Translation(ctx.language, v))
            else:
                for t in ctx.translate(v, include_ambiguous=True):
                    meanings[str(t)].append(t.meaning)
                    if t.meaning != 0:
                        continue

                    if not known_words:
                        trans.append(t)
                    elif all(p.word.uuid in known_words
                           for p in t.parts
                           if p.word is not None):
                        trans.append(t)
                    else:
                        bad_trans.append(t)

        t = Translation.best_translation(iter(trans or bad_trans),
                                         self.q_dialect,
                                         self.q_most_unique,
                                         known_words)
        return t, meanings[str(t)]

    @staticmethod
    def update_score(user: Optional[User],
                     correct_words: Set[DictWord], wrong_words: Set[DictWord],
                     correct_grammar: Set[str], wrong_grammar: Set[str]) -> None:
        if user is None:
            return

        for w in correct_words:
            UserWords.practised(user, w.uuid, True, 10)
        for w in wrong_words:
            UserWords.practised(user, w.uuid, True, -10, 0.8)

        for tag in correct_grammar:
            UserGrammarTags.practised(user, tag, True, 10)
        for tag in wrong_grammar:
            UserGrammarTags.practised(user, tag, True, -10, 0.8)

    @classmethod
    def update_score_from_comparison(cls, user: Optional[User],
                                     tc: TranslationComparison) -> None:
        cls.update_score(user, tc.correct_words, tc.wrong_words,
                         tc.correct_grammar, tc.wrong_grammar)

    @classmethod
    def update_user_word_score(cls, user: Optional[User], t: Translation) -> None:
        if user is None:
            return

        correct_words = {p.word for p in t.parts if p.word}
        correct_grammar = set(t.sentence_grammar_tags)
        for p in t.parts:
            correct_grammar |= p.grammar_tags

        cls.update_score(user, correct_words, set(), correct_grammar, set())

class IntroduceWordExercise(Exercise):
    def __init__(self, id: int, src_lang: Language,
                 dbword: DBWord, dict_entry: DictionaryEntry,
                 src_dialect: Dialect, src_most_unique: bool,
                 q_dialect: Dialect, q_most_unique: bool) -> None:
        super().__init__(id, q_dialect, q_most_unique,
                         src_dialect, src_most_unique)
        self.src_lang = src_lang
        self.dbword = dbword
        self.dict_entry = dict_entry
        self.defs = dict_entry.exact_matches()
        self.word = dbword.dict_word()
        self.audio = self.word.get_audio(q_dialect)

    def get_question(self) -> ExerciseQuestion:
        return ExerciseIntroduceWord(self.word, self.src_lang, self.defs,
                                     self.audio)

    def check_answer(self, user: Optional[User], answer: str) -> None:
        return None

class MultipleChoiceExercise(Exercise):
    class Config:
        def __init__(self, correct_answer: str,
                     wrong_answer: Sequence[str],
                     instruction: Optional[str] = None,
                     question: Optional[str] = None,
                     question_audio: Optional[str] = None) -> None:
            self.instruction = instruction
            self.question = question
            self.question_audio = question_audio
            self.correct_answer = correct_answer
            self.wrong_answer = wrong_answer

    def __init__(self, id: int, q_ctx: DBTranslationContext,
                 cfg: 'MultipleChoiceExercise.Config',
                 src_dialect: Dialect, src_most_unique: bool,
                 q_dialect: Dialect, q_most_unique: bool) -> None:
        super().__init__(id, q_dialect, q_most_unique,
                         src_dialect, src_most_unique)
        self.cfg = cfg
        self.lang = q_ctx.language
        self.question = Translation(q_ctx.language, self.cfg.question or "")

    def get_question(self) -> ExerciseQuestion:
        answers = [self.cfg.correct_answer, *self.cfg.wrong_answer]
        random.shuffle(answers)
        return ExerciseMultipleChoiceQuestion(self.question, answers,
                                              self.cfg.instruction,
                                              audio=self.cfg.question_audio)

    def check_answer(self, user: Optional[User], answer: str) -> ExerciseResult:
        return ExerciseResult(Translation(self.lang, answer),
                              answer == self.cfg.correct_answer,
                              Translation(self.lang, self.cfg.correct_answer),
                              self.cfg.correct_answer, [])

class TranslationExercise(Exercise):
    def __init__(self, id: int, q_ctx: DBTranslationContext,
                 a_ctx: DBTranslationContext,
                 question: Sequence[Union[str, Concept]],
                 answer: Sequence[Union[str, Concept]],
                 target_is_question: bool,
                 known_words: Optional[Dict[UUID, float]],
                 q_dialect: Dialect, q_most_unique: bool,
                 a_dialect: Dialect, a_most_unique: bool,
                 phrase: Optional[str] = None,
                 phrase_param: int = 0) -> None:
        super().__init__(id, q_dialect, q_most_unique, a_dialect, a_most_unique,
                         phrase=phrase, phrase_param=phrase_param)
        self.ctx = a_ctx
        self.q_ctx = q_ctx
        self.lang = a_ctx.language
        self.question, self.meanings = self.select_variant(q_ctx, question, known_words)
        self.answer = answer
        self.target_is_question = target_is_question

    def get_question(self) -> ExerciseQuestion:
        return ExerciseFreeformQuestion(self.question)

    def check_answer_update_score(self, user: Optional[User],
                                  answer: Optional[Translation]) -> None:
        if self.target_is_question:
            self.update_user_word_score(user, self.question)
        elif answer is not None:
            self.update_user_word_score(user, answer)

    def check_answer(self, user: Optional[User], answer: str) -> ExerciseResult:
        correct = False
        closest_correction = None
        closest_correction_score = -1.0
        best_correction = None
        best_correction_score = -1.0
        hints = cast(List[str], [])

        for a in self.answer:
            if isinstance(a, Concept):
                p_dialects = Dialect.propagate_subdialects(set([self.a_dialect]))

                # Check if the question is ambiguous and find the valid answers
                # for all possible meanings
                meanings = defaultdict(list)

                a_translations = self.ctx.translate(a, include_ambiguous=True)
                for t in a_translations:
                    # FIXME 'a' could be from a different variant than
                    # 'self.question', so this could allow invalid translations
                    if t.meaning in self.meanings:
                        meanings[t.meaning].append(t)

                assert 0 in self.meanings
                translations = [t for m in meanings.values() for t in m]
                assert all(t.lang == self.lang for t in translations)

                # Check against all found possible translations
                for x in translations:
                    if check_answer_str(str(x), answer):
                        self.check_answer_update_score(user, x)
                        correct = True
                        if len(x.dialects()) == 0 or (p_dialects and not p_dialects & x.dialects()):
                            hints += x.dialect_hints()

                    s = SequenceMatcher(a=str(x), b=answer)
                    score = s.ratio()
                    if score > closest_correction_score:
                        closest_correction = x
                        closest_correction_score = score
                    if len(x.dialects()) != 0:
                        dialect_score = score + len(p_dialects & x.dialects())
                        if dialect_score > best_correction_score:
                            best_correction = str(x)
                            best_correction_score = dialect_score
                    # TODO Rate uniqueness like Translation.best_translation()
            else:
                if check_answer_str(a, answer):
                    self.check_answer_update_score(user, None)
                    correct = True
                else:
                    closest_correction = Translation(self.lang, a)

        if best_correction and check_answer_str(best_correction, answer):
            best_correction = None

        if not correct and closest_correction:
            tc = TranslationComparison(self.ctx.repository, answer,
                                       closest_correction)
            answer_trans = tc.given
            # TODO Figure out the incorrectly translated words for target -> source
            if not self.target_is_question:
                self.update_score_from_comparison(user, tc)
        else:
            answer_trans = Translation(self.lang, answer)

        return ExerciseResult(answer_trans, correct, closest_correction,
                              best_correction, hints)

class TranslationCorrectionExercise(TranslationExercise):
    def get_question(self) -> ExerciseQuestion:
        answer = random.choice(self.answer)
        assert isinstance(answer, Concept)

        words: Dict[UUID, DictWord] = {}
        best_words: Dict[UUID, DictWord] = {}

        translations = self.ctx.translate(answer)
        best = Translation.best_translation(translations, self.q_dialect,
                                            self.q_most_unique)
        for p in best.parts:
            if p.word:
                best_words[p.word.uuid] = p.word

        translations = self.ctx.translate(answer)
        for t in translations:
            for p in t.parts:
                if p.word:
                    words[p.word.uuid] = p.word

        # TODO Prefer words that are often confused; don't iterate over all words in the course
        word = random.choice(tuple(best_words.values()))

        default: Optional[str] = None
        replacements = [w for w in self.ctx.words.values()
                          if type(w) == type(word) and w.uuid not in words]
        if replacements:
            wrong_word = random.choice(replacements)
            translations = self.ctx.translate(answer, vocab_errors={
                word.uuid: wrong_word.uuid
            })
            default = str(Translation.best_translation(translations, self.q_dialect,
                                                       self.q_most_unique))

        return ExerciseFreeformQuestion(self.question, default=default,
                                        instruction=_("Correct any errors in the answer") if default else None)

def exercise_expire_timestamp() -> datetime:
    return datetime.now() + timedelta(hours=1)

class DBExerciseSet(database.DBObject):
    json = TextField()
    expires = TimestampField(default=exercise_expire_timestamp)

    @classmethod
    def cleanup(cls) -> None:
        q = cls.delete()
        q = q.where(cls.expires < datetime.now())
        q.execute()

class ExerciseSet:
    class Config:
        def __init__(self,
                     source: Language, source_dialect: Dialect,
                     target: Language, target_dialect: Dialect,
                     phrases: Sequence[str] = (),
                     phrases_random: int = 0,
                     concepts: Sequence[str] = (),
                     concepts_random: int = 0,
                     translate_to_target: int = 0,
                     translate_from_target: int = 0,
                     concept_to_target: int = 0,
                     concept_from_target: int = 0,
                     correct_translation: int = 0,
                     exercises: Sequence[Any] = (),
                     focus_skill: int = 0) -> None:
            self.source = source
            self.source_dialect = source_dialect
            self.target = target
            self.target_dialect = target_dialect
            self.phrases = phrases
            self.phrases_random = phrases_random
            self.concepts = concepts
            self.concepts_random = concepts_random
            self.translate_to_target = translate_to_target
            self.translate_from_target = translate_from_target
            self.concept_to_target = concept_to_target
            self.concept_from_target = concept_from_target
            self.correct_translation = correct_translation
            self.exercises = exercises
            self.focus_skill = focus_skill

        def count_phrases(self) -> int:
            return (self.translate_to_target
                    + self.translate_from_target
                    + self.correct_translation)

        def count_concepts(self) -> int:
            return (self.concept_to_target
                    + self.concept_from_target)

    def __init__(self, db: DBExerciseSet, repo: Repository, user_id: Optional[int],
                 source: Language, source_dialect: Dialect, source_most_unique: bool,
                 target: Language, target_dialect: Dialect, target_most_unique: bool,
                 exercises: Sequence[Dict[str, Any]]) -> None:
        self.db = db
        self.repo = repo
        self.user_id = user_id
        self.source = source
        self.source_dialect = source_dialect
        self.source_most_unique = source_most_unique
        self.target = target
        self.target_dialect = target_dialect
        self.target_most_unique = target_most_unique
        self.exercises = exercises

    def get_phrase_exercise(self, i: int, e: Dict[str, Any],
                            source_ctx: DBTranslationContext,
                            target_ctx: DBTranslationContext) -> Exercise:
        t = e.pop('type')
        p_uuid = e.pop('phrase')
        param_config_index = e.pop('phrase-param')
        db_p = DBPhrase.get(self.repo, p_uuid)
        source_variants = db_p.phrase.get_variants(self.source, param_config_index)
        target_variants = db_p.phrase.get_variants(self.target, param_config_index)

        if t == 'translate-to-target':
            ex = TranslationExercise(i, source_ctx, target_ctx,
                                     source_variants,
                                     target_variants,
                                     False,
                                     None,
                                     self.source_dialect,
                                     self.source_most_unique,
                                     self.target_dialect,
                                     self.target_most_unique,
                                     phrase=p_uuid, phrase_param=param_config_index)
        elif t == 'translate-from-target':
            user = User.get_by_id(self.user_id) if self.user_id else None
            ex = TranslationExercise(i, target_ctx, source_ctx,
                                     target_variants,
                                     source_variants,
                                     True,
                                     db_p.known_user_words(user),
                                     self.target_dialect,
                                     self.target_most_unique,
                                     self.source_dialect,
                                     self.source_most_unique,
                                     phrase=p_uuid, phrase_param=param_config_index)
        elif t == 'correct-translation':
            ex = TranslationCorrectionExercise(i, source_ctx, target_ctx,
                                               source_variants,
                                               target_variants,
                                               False,
                                               None,
                                               self.source_dialect,
                                               self.source_most_unique,
                                               self.target_dialect,
                                               self.target_most_unique,
                                               phrase=p_uuid,
                                               phrase_param=param_config_index)
        else:
            raise Exception("Unknown type '%s'" % t)

        return ex

    def get_concept_exercise(self, i: int, e: Dict[str, Any],
                             source_ctx: DBTranslationContext,
                             target_ctx: DBTranslationContext) -> Exercise:
        t = e.pop('type')
        c_uuid = e.pop('concept')
        source_concept = Concept(UUID(c_uuid))
        target_concept = Concept(UUID(c_uuid))

        if t == 'concept-to-target':
            ex = TranslationExercise(i, source_ctx, target_ctx,
                                     [source_concept], [target_concept], False,
                                     None,
                                     self.source_dialect,
                                     self.source_most_unique,
                                     self.target_dialect,
                                     self.target_most_unique)
        elif t == 'concept-from-target':
            ex = TranslationExercise(i, target_ctx, source_ctx,
                                     [target_concept], [source_concept], True,
                                     None,
                                     self.target_dialect,
                                     self.target_most_unique,
                                     self.source_dialect,
                                     self.source_most_unique)
        else:
            raise Exception("Unknown type '%s'" % t)

        return ex

    def get_exercise(self, i: int) -> Exercise:
        e = self.exercises[i]

        # TODO Avoid querying all the words/concepts all the time
        source_ctx = DBTranslationContext.get_by_repo(self.repo, self.source)
        target_ctx = DBTranslationContext.get_by_repo(self.repo, self.target)

        if 'phrase' in e:
            return self.get_phrase_exercise(i, e, source_ctx, target_ctx)
        elif 'concept' in e:
            return self.get_concept_exercise(i, e, source_ctx, target_ctx)

        t = e.pop('type')
        if t == 'multiple-choice':
            mc_config = json_util.create_from_json_dict(
                MultipleChoiceExercise.Config, e)
            return MultipleChoiceExercise(i, source_ctx, mc_config,
                                          self.source_dialect,
                                          self.source_most_unique,
                                          self.target_dialect,
                                          self.target_most_unique)
        elif t == 'introduce-word':
            word = e.pop('word')
            dbword = DBWord.get(self.repo, word)
            dict_entry = dictionary.lookup(self.repo, dbword)
            return IntroduceWordExercise(i, self.source, dbword, dict_entry,
                                         self.source_dialect,
                                         self.source_most_unique,
                                         self.target_dialect,
                                         self.target_most_unique)
        else:
            raise Exception("Unknown type '%s'" % t)

    def count(self) -> int:
        return len(self.exercises)

    @classmethod
    def from_json(cls, repo: Repository, db: DBExerciseSet,
                  json_dict: Dict[Any, Any]) -> 'ExerciseSet':
        user_id = json_dict.pop('user')

        source = Language.get_by_code(json_dict.pop('source'))
        source_dialect = source.dialects[json_dict.pop('source-dialect')]
        source_most_unique = json_dict.pop('source-most-unique')

        target = Language.get_by_code(json_dict.pop('target'))
        target_dialect = target.dialects[json_dict.pop('target-dialect')]
        target_most_unique = json_dict.pop('target-most-unique')

        json_list = json_dict.pop('exercises')
        assert(isinstance(json_list, list))

        return cls(db, repo, user_id,
                   source, source_dialect, source_most_unique,
                   target, target_dialect, target_most_unique,
                   json_list)

    @classmethod
    def get(cls, repo: Repository, id: int) -> 'ExerciseSet':
        x = DBExerciseSet.get_by_id(id)
        json_dict = json.loads(x.json)
        assert(isinstance(json_dict, dict))
        return cls.from_json(repo, x, json_dict)

    @classmethod
    def exercises_from_new_words(cls, new_words: Set[UUID]) -> List[Dict[str, str]]:
        return [{'type': 'introduce-word',
                 'word': str(w)}
                for w in new_words]

    @classmethod
    def exercise_from_phrase(cls, phrases: List[DBPhrase], name: str,
                             n: int) -> Sequence[Dict[str, Any]]:
        # TODO If the exercise was selected for a specific word, use that word
        # (e.g. don't ask for "madadh" when we want to practise "gadhar")
        exercises: List[Dict[str, Any]] = []
        for i in range(0, n):
            p = phrases.pop()
            param = p.phrasetranslationindex.param_config_index
            exercises.append({'type': name,
                              'phrase': str(p.uuid),
                              'phrase-param': param})
        return exercises

    @classmethod
    def exercise_from_concept(cls, concepts: List[DBConcept], name: str,
                              n: int) -> Sequence[Dict[str, Any]]:
        # TODO If the exercise was selected for a specific word, use that word
        exercises: List[Dict[str, Any]] = []
        for i in range(0, n):
            c = concepts.pop()
            exercises.append({'type': name,
                              'concept': str(c.concept.uuid)})
        return exercises

    @classmethod
    def create(cls, repo: Repository, course: DBCourse, config: Config,
               user: Optional[User] = None) -> 'ExerciseSet':
        assert config.source == course.content.src_lang
        assert config.target == course.content.lang

        num_phrases = config.count_phrases()
        num_concepts = config.count_concepts()

        concepts: List[DBConcept] = []
        phrases : List[DBPhrase] = []

        target_ctx = DBTranslationContext(config.target, {}, {}, repo)
        for c in config.concepts:
            concepts.append(DBConcept.get(repo, c))

        random_concepts = config.concepts_random
        if random_concepts == 0 and len(concepts) == 0:
            random_concepts = num_concepts

        if random_concepts:
            concepts += DBConcept.get_exercises(
                repo, course, random_concepts,
                skill_id=config.focus_skill, user=user)

        for p in config.phrases:
            p = DBStored.resolve_alias(repo, p)
            phrases += DBPhrase.get_exercises(
                repo, course, 1,
                uuid=UUID(p), skill_id=config.focus_skill, user=user)

        random_phrases = config.phrases_random
        if random_phrases == 0 and len(phrases) == 0:
            random_phrases = num_phrases

        if random_phrases:
            phrases += DBPhrase.get_exercises(
                repo, course, random_phrases,
                skill_id=config.focus_skill, user=user)

        if (num_phrases and not phrases) or (num_concepts and not concepts):
            raise Exception(_("No matching exercise available"))

        new_words: Set[UUID] = set()
        for ph in phrases:
            param = ph.phrasetranslationindex.param_config_index
            new_words |= ph.unknown_user_words(param, course, user)

        for co in concepts:
            new_words |= co.unknown_user_words(course, user)

        while len(phrases) < num_phrases:
            phrases *= 2
        while len(concepts) < num_concepts:
            concepts *= 2

        random.shuffle(phrases)
        random.shuffle(concepts)

        exercises: List[Any] = []
        exercises += cls.exercise_from_phrase(phrases, 'translate-to-target',
                                              config.translate_to_target)
        exercises += cls.exercise_from_phrase(phrases, 'translate-from-target',
                                              config.translate_from_target)
        exercises += cls.exercise_from_phrase(phrases, 'correct-translation',
                                              config.correct_translation)
        exercises += cls.exercise_from_concept(concepts, 'concept-to-target',
                                              config.concept_to_target)
        exercises += cls.exercise_from_concept(concepts, 'concept-from-target',
                                              config.concept_from_target)

        exercises += config.exercises

        random.shuffle(exercises)

        exercises = cls.exercises_from_new_words(new_words) + exercises

        json_dict = {
            'user': user.id if user else None,
            'source': config.source.code,
            'source-dialect': config.source_dialect.name,
            'source-most-unique': False,
            'target': config.target.code,
            'target-dialect': config.target_dialect.name,
            'target-most-unique': False,
            'exercises': exercises,
        }

        DBExerciseSet.cleanup()
        db = DBExerciseSet(json=json.dumps(json_dict))
        db.save()

        return cls.from_json(repo, db, json_dict)

database.register_tables([DBExerciseSet])
