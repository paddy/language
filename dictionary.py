from typing import *

import uuid

from language.language import Concept, ConceptTranslationRule, Language, Translation
from repository import Repository, DBWord, DBConcept, DBTranslationContext


class DictionaryDefinition:
    def __init__(self, headword: Translation, synonyms: List[Translation]) -> None:
        self.headword = headword
        self.synonyms: List[Translation] = []
        self.translations: Dict[Language, List[Translation]] = {}

        for s in synonyms:
            if s.lang == headword.lang:
                self.synonyms.append(s)
            else:
                if s.lang not in self.translations:
                    self.translations[s.lang] = []
                self.translations[s.lang].append(s)

class DictionaryEntry:
    def __init__(self, word: DBWord, defs: List[DictionaryDefinition]) -> None:
        self.word = word
        self.defs = defs

    def exact_matches(self) -> List[DictionaryDefinition]:
        return [ d for d in self.defs
                 if str(d.headword) == str(self.word.word.lemma()) ]

def lookup(repository: Repository, word: DBWord) -> DictionaryEntry:
    concepts = DBConcept.get_multiple(repository, word)

    defs: List[DictionaryDefinition] = []
    for c in concepts:
        headword: Optional[Translation] = None
        synonyms: List[Translation] = []
        ctx = DBTranslationContext.get_by_concept_uuid(repository, c)

        orig_rules = ctx.concepts[c.concept.uuid].rules
        for r in c.concept.rules:
            ctx.switch_language(r.language)
            ctx.concepts[c.concept.uuid].rules = [r]
            translations = ctx.translate(Concept(c.concept.uuid))
            t = Translation.best_translation(translations, r.language.default_dialect)

            if not headword and str(word.word.uuid) in r.word_list():
                headword = t
            else:
                synonyms.append(t)
        ctx.concepts[c.concept.uuid].rules = orig_rules

        assert headword
        defs.append(DictionaryDefinition(headword, synonyms))

    return DictionaryEntry(word, defs)
