from typing import *
import peewee
from bottle import hook

db = peewee.SqliteDatabase('language.db')
tables = cast(List[Type['DBObject']], [])

noaccent_trans = str.maketrans(
    "áéíóúäöüß",
    "aeiouaous",
)

def collate_normalise(s: str) -> str:
    return s.lower().translate(noaccent_trans)

@no_type_check
@db.collation('noaccent')
def collate_noaccent(s1, s2):
    s1 = s1.collate_normalise()
    s2 = s2.collate_normalise()
    return (s1 > s2) - (s1 < s2)

class DBObject(peewee.Model): # type: ignore
    class Meta:
        database = db

@no_type_check
@hook('before_request')
def _connect_db() -> None:
    db.connect()

@no_type_check
@hook('after_request')
def _close_db() -> None:
    if not db.is_closed():
        db.close()

def init_tables() -> None:
    db.connect()
    db.create_tables(tables)
    for t in tables:
        if 'init_table' in t.__dict__:
            t.init_table()
    db.close()

def register_tables(new: Sequence[Type[DBObject]]) -> None:
    global tables
    tables += new
