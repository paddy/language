#!/usr/bin/env python3

import gettext
gettext.translation('messages', "./po/", languages=['de'], fallback=True).install()

from typing import *

import json
import sys
import web
import database
from repository import (
    create_dbstored,
    Repository, DBStored, DBWord, DBConcept, DBPhrase, DBLesson, DBCourse,
    DBTranslationContext)

print("Creating database tables...")
database.init_tables()

if len(sys.argv) == 2:
    filename = sys.argv[1]
else:
    filename = "data/content.json"

print("Importing %s..." % filename)
with open(filename) as f:
    data = f.read()

json_dict = json.loads(data)
if not isinstance(json_dict, dict):
    print("Error: The file doesn't contain a JSON dict")
    sys.exit(1)

types: Dict[str, Type[DBStored]] = {
    'words': DBWord,
    'concepts': DBConcept,
    'phrases': DBPhrase,
    'lessons': DBLesson,
    'courses': DBCourse,
}
for key in json_dict:
    dbstored_type = types[key]
    dbstored_list = json_dict[key]
    if not isinstance(dbstored_list, list):
        print("Error: '%s' doesn't contain a JSON list" % key)
        sys.exit(1)

    with database.db.atomic():
        for x in dbstored_list:
            create_dbstored(json.dumps(x), "Data imported", dbstored_type)
