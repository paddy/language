from typing import *

import database
import peewee
from peewee import (
    fn,
    CharField, ForeignKeyField, IntegerField, Select,
    TextField, TimestampField, UUIDField)

import course
import versioning
from versioning import VersionedText, VersionedTextRev, ObjectType
from user import User
from language.course import Course, CourseSkillType, Lesson
from language.language import (
    Concept, ConceptDefinition, ConceptTranslationRule,
    Dialect, DictWord, Language, Phrase,
    Translation, TranslationContext)

import copy
from collections import defaultdict
import json
import json_util
from json_util import AliasResolver
import random
import uuid
from enum import Enum
from uuid import UUID
from datetime import datetime, timedelta

from translation import gettext as _
import languagedata

LanguageData = languagedata.LanguageData

class Repository:

    lessons_by_id: Dict[int, 'DBLesson'] = {}
    courses_by_id: Dict[int, 'DBCourse'] = {}

    def __init__(self, id: int, ld: LanguageData) -> None:
        self.id = id
        self.ld = ld
        self.name = "Default repository"

        self.word_forms: Dict[str, List[DBWord]] = defaultdict(list)
        self.concepts_by_word: Dict[UUID, List[DBConcept]] = defaultdict(list)

        self.words: Dict[UUID, DBWord] = {
            uuid: DBWord(self, word)
            for uuid, word in ld.words.items()
        }
        self.concepts: Dict[UUID, DBConcept] = {
            uuid: DBConcept(self, c)
            for uuid, c in ld.concepts.items()
        }
        self.phrases: Dict[UUID, DBPhrase] = {
            uuid: DBPhrase(self, uuid, p)
            for uuid, p in ld.phrases.items()
        }
        self.lessons: Dict[UUID, DBLesson] = {
            uuid: DBLesson(self, uuid, l)
            for uuid, l in ld.lessons.items()
        }
        self.courses: Dict[UUID, DBCourse] = {
            uuid: DBCourse(self, c)
            for uuid, c in ld.courses.items()
        }

        Repository.lessons_by_id.update({
            c.content.id: c for c in self.lessons.values()
        })
        Repository.courses_by_id.update({
            c.id: c for c in self.courses.values()
        })

    class DoesNotExist(Exception):
        pass

    def get_words(self) -> Dict[UUID, DictWord]:
        sorted_words = sorted(self.words.items(),
                              key=lambda w: database.collate_normalise(w[1].word.lemma()))
        return { uuid: w.word for uuid, w in sorted_words }

    @classmethod
    def get_by_id(cls, id: int) -> 'Repository':
        # TODO
        if id != 1:
            raise cls.DoesNotExist()
        return default_repo

ld = LanguageData()
default_repo = Repository(1, ld)

def load_repo() -> None:
    global ld, default_repo
    print("--- loading repo ---")

    # FIXME Make the filename dynamic
    ld = LanguageData.import_file("data/content.json")
    print("%d courses" % len(ld.courses))
    default_repo = Repository(1, ld)

class DBStored:
    TDBStored = TypeVar('TDBStored', bound='DBStored')

    @classmethod
    def get_type_dict(cls: Type[TDBStored],
                      repo: Optional[Repository] = None) -> Dict[UUID, TDBStored]:
        if repo is None:
            repo = default_repo

        if cls is DBWord:
            return repo.words # type: ignore
        elif cls is DBConcept:
            return repo.concepts # type: ignore
        elif cls is DBPhrase:
            return repo.phrases # type: ignore
        elif cls is DBLesson:
            return repo.lessons # type: ignore
        elif cls is DBCourse:
            return repo.courses # type: ignore

        raise Exception("Invalid subclass type")

    @classmethod
    def get_all(cls: Type[TDBStored],
                repo: Optional[Repository] = None) -> List[TDBStored]:
        return list(cls.get_type_dict(repo).values())

    @classmethod
    def get(cls: Type[TDBStored], repo: 'Repository', uuid: str,
            rev_id: Optional[int] = None) -> TDBStored:
        return cls.get_type_dict(repo)[UUID(uuid)]

    @classmethod
    def get_by_id(cls: Type[TDBStored], i: int) -> TDBStored:
        try:
            if cls is DBCourse:
                return Repository.courses_by_id[i] # type: ignore
            elif cls is DBLesson:
                return Repository.lessons_by_id[i] # type: ignore
            # TODO
            raise Exception("Invalid subclass type")
        except KeyError:
            raise VersionedText.DoesNotExist()

    @staticmethod
    def resolve_alias(repo: 'Repository', s: str) -> str:
        return ld.resolve_alias(s)

    @staticmethod
    def replace_by_alias(repo: 'Repository', s: str) -> str:
        return s

    @classmethod
    def replace_by_aliases(cls, repo: 'Repository',
                           json: Union[List[Any], Dict[str, Any]]) -> None:
        json_util.modify_json_str(repo, json, cls.replace_by_alias)

class DBAliasResolver(AliasResolver):
    def __init__(self, repo: 'Repository') -> None:
        self.repo = repo

    def resolve_alias(self, alias: str) -> str:
        return DBStored.resolve_alias(self.repo, alias)

class DBWord(DBStored):
    def __init__(self, repo: Repository, data: DictWord) -> None:
        self.word = data
        self.update_index(repo)

        # Optional attributes to return user progress
        # TODO Should return UserWords rather than a copy with an additional attribute
        self.uw: UserWords
        self.priority: float
        self.target_score: float

    def update_index(self, repo: Repository) -> None:
        for form in self.word.get_forms():
            repo.word_forms[form].append(self)

    def dict_word(self) -> DictWord:
        return self.word

    @staticmethod
    def get_by_form(repo: 'Repository', form: str) -> List['DBWord']:
        return repo.word_forms[form]

class DBConcept(DBStored):
    def __init__(self, repo: Repository, data: ConceptDefinition) -> None:
        self.repo = repo
        self.concept = data
        self.words: Set[Tuple[UUID, Language]] = set()
        self.languages: Set[Language] = set()
        self.update_index(repo)

    def update_index(self, repo: Repository) -> None:
        # Using DBTranslationContext.get_by_repo() here assumes that the
        # translations done with ctx don't use the aliases the we only create
        # below. But since this would be an infinite recursion anyway, the
        # assumption should be fine.
        ctx = DBTranslationContext.get_by_repo(repo, None, True)

        index = self.concept.generate_index(ctx)
        self.words = index.words
        for uuid, lang in index.words:
            self.languages.add(lang)
            self.repo.concepts_by_word[uuid].append(self)

    @staticmethod
    def get_multiple(repository: 'Repository',
                     word: Optional[DBWord] = None) -> List['DBConcept']:
        if word:
            return repository.concepts_by_word[word.word.uuid]
        else:
            return list(repository.concepts.values())

    def unknown_user_words(self, dbcourse: 'DBCourse',
                           user: Optional[User]) -> Set[UUID]:
        if not user:
            return set()

        known_words: Set[UUID] = set()

        config = course.CourseUserConfig.get_for_course(dbcourse.content, user)
        course_words = dbcourse.get_words(config)

        for uuid, uw in UserWords.for_user(user).items():
            if uw.status == UserWords.Status.KNOWN.value:
                known_words.add(uuid)

        concept_words: Set[UUID] = { w[0] for w in self.words
                                     if w[1] == dbcourse.content.lang }

        return concept_words - known_words

    @staticmethod
    def get_exercises(repository: 'Repository', dbcourse: 'DBCourse',
                      num: int, skill_id: int = 0,
                      user: Optional[User] = None) -> List['DBConcept']:

        source = dbcourse.content.src_lang
        target = dbcourse.content.lang
        skill = dbcourse.content.skill_by_id(skill_id) if skill_id else None
        result: List[DBConcept] = []
        scores: Dict[DBConcept, float] = {}

        if user:
            user_words = UserWords.for_user(user)
        else:
            user_words = {}

        if skill is not None:
            config = course.CourseUserConfig.get_for_course(dbcourse.content, user)
            skill_words = {w.uuid for w in skill.get_words(config)}
        else:
            skill_words = set()

        for c in repository.concepts.values():
            if source not in c.languages or target not in c.languages:
                continue

            # Consider only concepts that contain a relevant word for the skill
            concept_words: Set[UUID] = { w[0] for w in c.words if w[1] == target }
            if skill is not None:
                relevant_words = concept_words & skill_words
                if not relevant_words:
                    continue
                scores[c] = len(relevant_words) / len(skill_words)
            else:
                scores[c] = 0.0

            max_priority = 0.0
            for uuid in concept_words:
                if uuid in user_words:
                    max_priority = max(max_priority, user_words[uuid].priority)
            scores[c] += max_priority

            result.append(c)

        result = sorted(result, key=lambda c: (scores[c], random.random()))

        if num:
            return result[0:num]
        else:
            return result

class MatchingPhrases:
    def __init__(self, total: int, current: int, new: int) -> None:
        self.total = total or 0
        self.current = current or 0
        self.new = new or 0

    @property
    def added(self) -> int:
        return self.new - self.current

class PhraseTranslationIndex:
    def __init__(self, param_config_index: int, lang: str, text: str) -> None:
        self.param_config_index = param_config_index
        self.lang = lang
        self.text = text
        self.words: Set[UUID] = set()
        self.tags: Set[str] = set()

class DBPhrase(DBStored):
    def __init__(self, repo: Repository, uuid: UUID, data: Phrase) -> None:
        self.repo = repo
        self.uuid = uuid
        self.phrase = data
        self.translations: Set[PhraseTranslationIndex] = set()
        self.languages: Dict[int, Set[Language]] = defaultdict(set)
        self.update_index()

        # Optional attribute to disambiguate variants when selecting a phrase
        # TODO Should return a pair (DBPhrase, int) rather than a copy with an
        # additional attribute
        self.phrasetranslationindex: PhraseTranslationIndex

    def update_index(self) -> None:
        ctx = DBTranslationContext.get_by_repo(self.repo, None, True)
        index = self.phrase.generate_index(ctx)

        for param_config_index in index:
            for v in index[param_config_index]:
                if not v.use_as_question:
                    continue

                self.languages[param_config_index].add(v.lang)
                pt = PhraseTranslationIndex(param_config_index, v.lang.code, v.text)
                for w in v.words:
                    pt.words.add(w)
                for tag in v.tags:
                    pt.tags.add(tag)
                self.translations.add(pt)

    @classmethod
    def get_available(cls, dbcourse: 'DBCourse', user: Optional[User],
                      skill: Optional['Course.Skill']
                      ) -> Iterator[Tuple['DBPhrase', PhraseTranslationIndex, int, float]]:
        repo = dbcourse.repo
        config = course.CourseUserConfig.get_for_course(dbcourse.content, user)
        course_tags = dbcourse.get_tags(config)

        if skill is not None:
            skill_words = {w.uuid for w in skill.get_words(config)}
            skill_tags = set(skill.tags)
            grammar_skill = skill.skill_type == CourseSkillType.GRAMMAR
        else:
            skill_words = set()
            skill_tags = set()
            grammar_skill = False

        if user:
            user_words: Dict[UUID, UserWords] = UserWords.for_user(user)
            known_words: Set[UUID] = set(UserWords.for_user(user).keys())
            words_with_new: Set[UUID] = known_words | skill_words

            known_tags: Set[str] = set(UserGrammarTags.for_user(user).keys())
            tags_with_new: Set[str] = known_tags | skill_tags

        for p in repo.phrases.values():
            configs_seen = set()
            for t in p.translations:
                # We need a source language translation...
                if dbcourse.content.src_lang not in p.languages[t.param_config_index]:
                    continue

                # ...and yield translations that are for the target language
                if t.lang != dbcourse.content.lang.code:
                    continue

                # Yield every (phrase, param_config_index) pair only once
                if t.param_config_index in configs_seen:
                    continue

                # Tags that aren't taught in any skill are always considered known
                tags = t.tags & course_tags

                # Consider only phrases that contain something relevant for the skill.
                # Select only phrases that use at least one word or one grammar tag
                # from the skill. However, if it's a grammar skill, practising a
                # random word used in an example doesn't really contribute to the
                # skill, so consider only grammar tags there.
                score = 0.0
                if skill is not None:
                    if grammar_skill:
                        if not tags & skill_tags:
                            continue
                    else:
                        if not (t.words & skill_words or tags & skill_tags):
                            continue

                    score += len(tags & skill_tags)
                    score += len(t.words & skill_words) / len(t.words)

                configs_seen.add(t.param_config_index)

                # Guests are assumed to know everything
                if not user:
                    yield (p, t, 1, 0.0)
                    continue

                # TODO Consider priority of grammar tags
                max_priority = 0.0
                for uuid in t.words & known_words:
                    max_priority = max(max_priority, user_words[uuid].priority)
                score += max_priority

                if t.words.issubset(known_words) and tags.issubset(known_tags):
                    yield (p, t, 1, score)
                elif t.words.issubset(words_with_new) and tags.issubset(tags_with_new):
                    yield (p, t, 2, score)
                else:
                    yield (p, t, 0, 0.0)

    @classmethod
    def count_available(cls, dbcourse: 'DBCourse', user: Optional[User],
                        skill: 'Course.Skill') -> MatchingPhrases:

        total = 0
        current = 0
        new = 0

        for p, t, status, score in cls.get_available(dbcourse, user, skill):
            total += 1
            if status == 1:
                current += 1
                new += 1
            elif status == 2:
                new += 1

        return MatchingPhrases(total, current, new)

    # FIXME uuid only identifies the phrase, but not the variant
    @classmethod
    def get_exercises(cls, repository: 'Repository', course: 'DBCourse',
                      num: int, uuid: Optional[UUID] = None, skill_id: int = 0,
                      user: Optional[User] = None) -> List['DBPhrase']:

        source = course.content.src_lang
        target = course.content.lang
        skill = course.content.skill_by_id(skill_id) if skill_id else None
        result = []
        scores: Dict[DBPhrase, float] = {}

        selected_phrase = None
        if uuid is not None:
            selected_phrase = repository.phrases[uuid]

        for p, t, status, score in cls.get_available(course, user, skill):
            if status == 0:
                continue
            if selected_phrase and p != selected_phrase:
                continue

            # TODO Better interface
            phrase = copy.copy(p)
            phrase.phrasetranslationindex = t
            result.append(phrase)

            scores[phrase] = score

        result = sorted(result, key=lambda p: (scores[p], random.random()))

        if num:
            return result[0:num]
        else:
            return result

    # FIXME need the target language to skip other translations
    def known_user_words(self, user: Optional[User]) -> Optional[Dict[UUID, float]]:
        if not user:
            return None

        phrase_words: Set[UUID] = set(w for t in self.translations for w in t.words)

        return { uuid: uw.priority
                 for uuid, uw in UserWords.for_user(user).items() }

    # TODO Ignore words from not selected variant for questions
    def unknown_user_words(self, param_config_index: int, dbcourse: 'DBCourse',
                           user: Optional[User]) -> Set[UUID]:
        if not user:
            return set()

        known_words: Set[UUID] = set()
        phrase_words: Set[UUID] = set()

        config = course.CourseUserConfig.get_for_course(dbcourse.content, user)
        course_words = dbcourse.get_words(config)

        for uuid, uw in UserWords.for_user(user).items():
            if uw.status == UserWords.Status.KNOWN.value:
                known_words.add(uuid)

        for t in self.translations:
            if t.param_config_index != param_config_index:
                continue
            if t.lang != dbcourse.content.lang.code:
                continue
            phrase_words |= t.words

        return (phrase_words & course_words) - known_words


class DBLesson(DBStored):
    def __init__(self, repo: Repository, uuid: UUID, data: Lesson) -> None:
        self.repo = repo
        self.uuid = uuid
        self.content = data

# TODO Rename UserSkill
# TODO Maybe this shoudn't store the skill, but just its ID (like UserWords)
class CourseSkill:
    def __init__(self, dbcourse: 'DBCourse', skill: Course.Skill,
                 config: Course.Config, user: Optional[User],
                 user_words: Dict[UUID, 'UserWords'],
                 user_tags: Dict[str, 'UserGrammarTags']) -> None:
        self.dbcourse = dbcourse
        self.course = dbcourse.content

        self.skill = skill
        self.total_words = len(skill.get_words(config))
        self.total_tags = len(skill.tags)

        self.phrases = DBPhrase.count_available(self.dbcourse, user, self.skill)

        if user:
            skill_words = { w.uuid for w in skill.words }
            skill_tags = { tag for tag in skill.tags }

            cs_user_words = set(user_words.keys()).intersection(skill_words)
            self.user_words = len(cs_user_words)
            self.words_score = sum(user_words[w].score for w in cs_user_words)
            self.words_target_score = sum(user_words[w].target_score for w in cs_user_words)

            cs_user_tags = set(user_tags.keys()).intersection(skill_tags)
            self.user_tags = len(cs_user_tags)
            self.grammar_score = sum(user_tags[t].score for t in cs_user_tags)
            self.grammar_target_score = sum(user_tags[t].target_score for t in cs_user_tags)

            if skill.skill_type == CourseSkillType.WORDS:
                self.score = self.words_score
                self.target_score = self.words_target_score
            else:
                self.score = self.grammar_score
                self.target_score = self.grammar_target_score

class DBCourse(DBStored):
    def __init__(self, repo: Repository, data: Course) -> None:
        self.repo = repo
        self.content = data
        self.id = self.content.id

    def get_user_skills(self, user: Optional[User],
                        skill_id: Optional[int] = None) -> Sequence['CourseSkill']:
        result = []
        config = course.CourseUserConfig.get_for_course(self.content, user)

        if user:
            user_words = UserWords.for_user(user)
            user_tags = UserGrammarTags.for_user(user)
        else:
            user_words = {}
            user_tags = {}

        for s in self.content.skills:
            if skill_id is None or skill_id == s.id:
                result.append(CourseSkill(self, s, config, user,
                                          user_words, user_tags))

        return result

    def get_lessons(self) -> Dict[UUID, Lesson]:
        lessons: Dict[UUID, Lesson] = {}
        for l in self.content.lessons:
            lesson = DBLesson.get(self.repo, str(l))
            lessons[l] = lesson.content

        return lessons

    def get_words(self, config: 'Course.Config', skill_id: int = 0) -> Set[UUID]:
        result = set()
        for s in self.content.skills:
            if skill_id and s.id != skill_id:
                continue
            result |= { w.uuid for w in s.get_words(config) }
        return result

    def get_tags(self, config: 'Course.Config', skill_id: int = 0) -> Set[str]:
        result = set()
        for s in self.content.skills:
            if skill_id and s.id != skill_id:
                continue
            result |= { t for t in s.tags }
        return result

    def get_user_words(self, user: Optional[User], skill_id: int = 0) -> Sequence[DBWord]:
        config = course.CourseUserConfig.get_for_course(self.content, user)
        words = self.get_words(config, skill_id)
        user_words = UserWords.for_user(user) if user else {}
        result = []

        for uuid in words:
            # FIXME Return just the UserWord instead of copying and modifying the DBWord
            dbword = DBWord(self.repo, self.repo.words[uuid].word)
            if uuid in user_words:
                dbword.uw = user_words[uuid]
                dbword.priority = dbword.uw.priority
                dbword.target_score = dbword.uw.target_score
            result.append(dbword)

        return result

    def activate_skill(self, user: User, skill_id: int) -> None:
        skill = self.content.skill_by_id(skill_id)
        config = course.CourseUserConfig.get_for_course(self.content, user)
        words = { w.uuid for w in skill.words
                  if not w.dialects or config.target_dialect in w.dialects }
        tags = { tag for tag in skill.tags }

        user_words = set(UserWords.for_user(user).keys())
        user_tags = set(UserGrammarTags.for_user(user).keys())

        for w in words - user_words:
            UserWords.practised(user, w, False, 0)

        for t in tags - user_tags:
            UserGrammarTags.practised(user, t, False, 0)


class UserPracticeData(database.DBObject):
    user = ForeignKeyField(User, backref="courses")
    date_added = TimestampField()
    score = IntegerField()
    status = IntegerField()

    class Status(Enum):
        NEW = 0
        KNOWN = 1

    def max_score(self) -> int:
        now = datetime.now().replace(microsecond=0).timestamp()
        days = 1 + (now - self.date_added.timestamp()) / (3600.0 * 24)
        target_score = (20 * days) + ((50 * days) / (1 + days))
        return int(1.2 * target_score)

    @classmethod
    def _target_score(cls) -> peewee.Expression:
        now = datetime.now().replace(microsecond=0).timestamp()
        days = 1 + (now - cls.date_added) / (3600.0 * 24)
        target_score = (20 * days) + ((50 * days) / (1 + days))
        return target_score

    @classmethod
    def _calc_priority(cls) -> peewee.Expression:
        priority = 2.0 - (cls.score / cls._target_score())
        return priority

    @classmethod
    def _practised(cls, user: User, update: bool, score: int,
                   factor: float, **kwargs: Any) -> None:
        now = datetime.now().replace(microsecond=0)
        uw, created = cls.get_or_create(
            user=user, **kwargs, defaults={
                'date_added': now,
                'score': score,
                'status': cls.Status.NEW.value,
            })
        if not created and update:
            uw.score = int(max(0, min(uw.score * factor + score, uw.max_score())))
            if score > 0:
                uw.status = cls.Status.KNOWN.value
            uw.save()

class UserWords(UserPracticeData):
    word = UUIDField()

    class Meta:
        # (user, word) must be unique
        indexes = (
            (('user', 'word'), True),
        )

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.priority: float
        self.target_score: float

    @classmethod
    def practised(cls, user: User, uuid: UUID, update: bool,
                  score: int, factor: float = 1.0) -> None:
        cls._practised(user, update, score, factor, word=uuid)

    @staticmethod
    def for_user(user: User) -> Dict[UUID, 'UserWords']:
        user_words = (UserWords
            .select(UserWords,
                    UserWords._calc_priority().alias('priority'),
                    UserWords._target_score().alias('target_score'))
            .where(UserWords.user == user.id))

        return { uw.word: uw for uw in user_words }

# FIXME Needs to be separate per language (words are ok because UUID)
class UserGrammarTags(UserPracticeData):
    tag = CharField()

    class Meta:
        # (user, tag) must be unique
        indexes = (
            (('user', 'tag'), True),
        )

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.priority: float
        self.target_score: float

    @classmethod
    def practised(cls, user: User, tag: str, update: bool,
                  score: int, factor: float = 1.0) -> None:
        cls._practised(user, update, score, factor, tag=tag)

    @staticmethod
    def for_user(user: User) -> Dict[str, 'UserGrammarTags']:
        user_tags = (UserGrammarTags
            .select(UserGrammarTags,
                    UserGrammarTags._calc_priority().alias('priority'),
                    UserGrammarTags._target_score().alias('target_score'))
            .where(UserGrammarTags.user == user.id))

        return { ut.tag: ut for ut in user_tags }

class DBTranslationContext(TranslationContext):

    def __init__(self, language: Optional[Language],
                 words: Dict[UUID, DictWord],
                 concepts: Dict[UUID, ConceptDefinition],
                 repo: 'Repository') -> None:
        super().__init__(language, words, concepts)
        self.repository = repo

    @classmethod
    def get_by_repo(cls, repo: 'Repository',
                    lang: Optional[Language],
                    all_languages: bool = False) -> 'DBTranslationContext':

        if not lang:
            assert all_languages

        return cls(lang, repo.ld.words, repo.ld.concepts, repo)

    @classmethod
    def get_by_concept_uuid(cls, repo: 'Repository', concept: Union[DBConcept, UUID],
                            ) -> 'DBTranslationContext':
        return cls.get_by_repo(repo, None, True)

    @classmethod
    def get_by_phrase_uuid(cls, repo: 'Repository', uuids: Sequence[UUID],
                           lang: Language,
                           all_languages: bool = False) -> 'DBTranslationContext':
        return cls.get_by_repo(repo, None, True)

def create_dbstored(data: str, changelog: str = "Wort hinzugefügt",
                    dbstored_type: Type[DBStored] = DBWord) -> str:
    return ''

database.register_tables([UserWords, UserGrammarTags])
