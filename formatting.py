from typing import *
import abc

from language.language import (
    Concept, Dialect, Language, Translation, TranslationContext)
from repository import (
    DBAliasResolver, DBStored, DBTranslationContext, Repository)
from json_util import AliasResolver

import json
import re

from translation import gettext as _

class PageContext:
    def __init__(self, alias_resolver: AliasResolver, tctx: TranslationContext,
                 dialect: Dialect, most_unique: bool) -> None:
        self.alias_resolver = alias_resolver
        self.tctx = tctx
        self.dialect = dialect
        self.most_unique = most_unique

class DocContent(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def to_html(self, ctx: PageContext) -> str:
        pass

class DocParagraph(DocContent):
    def __init__(self, content: Sequence[DocContent]) -> None:
        self.content = content

    def to_html(self, ctx: PageContext) -> str:
        return '<p>%s</p>' % "".join(x.to_html(ctx) for x in self.content)

class DocText(DocContent):
    templates = {
        'b': '<b>%s</b>',
        'word': '<i>%s</i>',
        'translation': '<b>%s</b>',
    }

    def __init__(self, text: str, template: Optional[str] = None) -> None:
        self.text = text
        self.template = template

    def to_html(self, ctx: PageContext) -> str:
        template = self.templates[self.template] if self.template else '%s'
        return template % self.text

class GeneratedText(DocText):
    def __init__(self, concept_json: str) -> None:
        self.concept_json = concept_json
        self.template = 'translation'

    def to_html(self, ctx: PageContext) -> str:
        try:
            json_dict = json.loads(self.concept_json)
            ctx.alias_resolver.resolve_aliases(json_dict)
            concept = Concept.from_json_dict(json_dict, {})
            translations = ctx.tctx.translate(concept)
            self.text = str(Translation.best_translation(translations, ctx.dialect, ctx.most_unique))
        except Exception as e:
            self.text = _("(ERROR: %s)") % e

        return super().to_html(ctx)

class DocList(DocContent):
    def __init__(self, items: Sequence[DocContent],
                 ordered: bool = False) -> None:
        self.items = items
        self.ordered = ordered

    def to_html(self, ctx: PageContext) -> str:
        content= "".join("<li>%s</li>\n" % i.to_html(ctx) for i in self.items)
        if self.ordered:
            return "<ol>\n%s</ol>" % content
        else:
            return "<ul>\n%s</ul>" % content

class Document:
    class ParseError(Exception):
        pass

    def __init__(self, title: str, content: Sequence[DocContent] = (),
                 sections: Sequence['Document'] = ()) -> None:
        self.title = title
        self.content = content
        self.sections = sections

    def format_html(self, ctx: PageContext, level: int = 0) -> str:
        level += 1
        result = '<h%d>%s</h%d>\n' % (level, self.title, level)

        for c in self.content:
            result += c.to_html(ctx)

        for s in self.sections:
            result += s.format_html(ctx, level)

        return result

    @staticmethod
    def parse_tag(parts: List[str], i: int) -> Tuple[Optional[str], Optional[str], List[str]]:
        match = re.fullmatch(r"\[([a-z]+)(?:=([^]]+))?\]", parts[i])
        if not match:
            content = [ parts[i] ]
            parts[i:i+1] = []
            return None, None, content

        name = match.group(1)
        param = match.group(2)
        start = i

        while i < len(parts) - 1:
            i += 1
            if parts[i] == f'[/{name}]':
                content = parts[start + 1:i]
                parts[start:i+1] = []
                return name, param, content

        content = [ parts[start] ]
        parts[start:start+1] = []
        return None, None, content

    @classmethod
    def parse_content(cls, tag: Optional[str], param: Optional[str],
                      parts: List[str]) -> Sequence['DocContent']:
        if tag in ['b', 'word']:
            return [ DocText(x, tag) for x in parts ]
        elif tag == 't':
            return [ GeneratedText(''.join(parts)) ]
        elif tag is not None:
            raise cls.ParseError(_("No such tag: '%s'" % tag))
        return [ DocText(x) for x in parts ]

    @classmethod
    def parse_section(cls, title: str, parts: List[str]) -> 'Document':
        subsections: List[Document] = []
        paragraphs: List[DocParagraph] = []
        cur_paragraph: List[DocContent] = []
        while parts:
            tag, param, content = cls.parse_tag(parts, 0)
            if tag == "section":
                if param is None:
                    raise cls.ParseError(_("Section needs a title"))
                subsections.append(cls.parse_section(param, content))
            elif tag is None and content == ["\n\n"]:
                if cur_paragraph:
                    paragraphs.append(DocParagraph(cur_paragraph))
                    cur_paragraph = []
            else:
                cur_paragraph += cls.parse_content(tag, param, content)

        if cur_paragraph:
            paragraphs.append(DocParagraph(cur_paragraph))

        return cls(title, paragraphs, subsections)

    @classmethod
    def parse(cls, title: str, text: str) -> 'Document':
        parts = re.split(r'(\[[^]]+\]|\n\n)', text)
        return cls.parse_section(title, parts)

if __name__ == '__main__':
    from language import irish, german
    repo = Repository.get_by_id(1)
    ctx = PageContext(DBAliasResolver(repo),
                      DBTranslationContext(Language.get_by_code('ga'),
                                           repo.ld.words,
                                           repo.ld.concepts,
                                           repo),
                      irish.Dialect.MUNSTER_D,
                      True)

    doc = Document('Testseite', content=[
        DocText("""
    Das ist ein bisschen einleitendes Blabla.
    """),
    ], sections=[
        Document('Das Verb', sections=[
            Document('bí', content=[
                DocText("Das Verb "),
                DocText("bí", template='word'),
                DocText(" ist etwas anderes als die Kopula"),
            ]),
            Document('Zeitformen', content=[
                DocList([
                    DocText('Präsens'),
                    DocText('Präteritum'),
                    DocText('Futur'),
                ], ordered=True)
            ]),
        ]),
        Document('Wörter', content=[
            GeneratedText('{"concept":"%s"}' % list(ctx.tctx.concepts.keys())[2]),
        ]),
    ])
    print(doc.format_html(ctx))

    doc = Document.parse("Ein Testdokument", """
    Einleitender Text mit [word]Tag[/word].

    [section=Kapitel]
    Das hier ist ein [word]Kapitel[/word] im Dokument. Es enthält das Wort [t]{"concept":"%sionnach"}[/t].
    [/section]
    """)

    print("\n\n\n===============================\n\n")
    print(doc.format_html(ctx))
