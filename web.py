#!/usr/bin/env python3

from typing import *

import translation
from translation import gettext as _
from translation import TranslationBottlePlugin

if __name__ == '__main__':
    translation.use_bottle = True

import bottle
from bottle import route, run, template, request, response, static_file, hook
from collections import defaultdict
import database

from versioning import VersionedText, VersionedTextRev, ObjectType
from user import User, UserSession
from course import (
    CourseUserConfig,
    LessonPageText, LessonPageWords, LessonPageExercise)
from exercise import ExerciseSet, DBExerciseSet, ExerciseResult
from formatting import Document, PageContext
from repository import (
    CourseSkill,
    DBAliasResolver,
    DBTranslationContext, DBWord, DBConcept, DBCourse, DBLesson, DBPhrase,
    MatchingPhrases, Repository, UserWords,
    load_repo, default_repo)
from language import *
from language.irish import Dialect
from language.course import Course, CourseSkillType
from language.language import (
    Language, TranslationContext, Translation, Concept, ConceptTranslationRule,
    DictWord)
import dictionary

import web_admin

from uuid import UUID

class Position:
    def __init__(self, name: str, link: Optional[str] = None) -> None:
        self.name = name
        self.link = link

    def get_html(self) -> str:
        if not self.link:
            return self.name
        else:
            return '<a href="%s">%s</a>' % (self.link, self.name)

@route('/login', ['POST', 'GET'])
def login() -> Any:
    result = None
    submit = request.forms.get("submit")
    if submit:
        username = request.forms.get("user")
        password = request.forms.get("password")
        result = UserSession.login(username, password)

    session = UserSession.get_current()
    return template('login', session=session, result=result)

@route('/logout')
def logout() -> Any:
    session = UserSession.get_current()
    if session:
        session.logout()
        session = None
    return template('logout', session=session)

@route('/repositories')
def repositories() -> Any:
    session = UserSession.get_current()

    repositories = [default_repo]
    position = [ Position(_("Repositories"), "/repositories") ]

    return template('repositories', session=session, position=position,
                    repositories=repositories)

@route('/repository/<id:int>')
def repository(id: int) -> Any:
    session = UserSession.get_current()

    try:
        repository = Repository.get_by_id(id)
    except Repository.DoesNotExist:
        return template('error', session=session, msg=_("This repository does not exist."))

    dbcourses = DBCourse.get_all(repository)
    courses = [ c.content for c in dbcourses ]

    position = [ Position(_("Repositories"), "/repositories"),
                 Position(repository.name, "/repository/%s" % id) ]
    return template('repository', session=session, position=position,
                    repository=repository, courses=courses)

@route('/repository/<id:int>/words')
def repository_words(id: int) -> Any:
    session = UserSession.get_current()

    try:
        repository = Repository.get_by_id(id)
    except Repository.DoesNotExist:
        return template('error', session=session, msg=_("This repository does not exist."))

    position = [ Position(_("Repositories"), "/repositories"),
                 Position(repository.name, "/repository/%s" % id),
                 Position(_("Words"), "/repository/%s/words" % id) ]
    return template('repository_words', session=session, position=position,
                    repository=repository, words=repository.get_words())

@route('/repository/<id:int>/words/<word_uuid>')
@route('/repository/<id:int>/words/<word_uuid>/revs/<rev_id:int>')
def repository_word(id: int, word_uuid: str, rev_id: Optional[int] = None,
                    dbcourse: Optional[DBCourse] = None) -> Any:
    session = UserSession.get_current()

    try:
        repository = Repository.get_by_id(id)
        word = DBWord.get(repository, word_uuid, rev_id)
    except Repository.DoesNotExist:
        return template('error', session=session, msg=_("This repository does not exist."))
    except VersionedTextRev.DoesNotExist:
        return template('error', session=session, msg=_("This revision does not exist for the given word."))
    except VersionedText.DoesNotExist:
        return template('error', session=session, msg=_("This word does not exist in the given course."))

    dict_entry = dictionary.lookup(repository, word)

    course = dbcourse.content if dbcourse else None
    if course:
        position = [ Position(_("Courses"), "/courses"),
                     Position(course.name, "/course/%d" % course.id),
                     Position(_("Words"), "/course/%d/words" % course.id),
                     Position(word.dict_word().lemma(),
                              "/course/%d/words/%s" % (course.id, word_uuid)) ]
    else:
        position = [ Position(_("Repositories"), "/repositories"),
                     Position(repository.name, "/repository/%s" % id),
                     Position(_("Words"), "/repository/%s/words" % id),
                     Position(word.dict_word().lemma(),
                              "/repository/%s/words/%s" % (id, word_uuid)) ]
    if rev_id:
        position.append(Position(_("Version history"), "/repository/%s/words/%s/history" % (id, word_uuid)))
        position.append(Position(_("Revision %d") % rev_id, "/repository/%s/words/%s/revs/%d" % (id, word_uuid, rev_id)))

    return template('word', session=session, position=position, course=course,
                    repository=repository, word=word, dict_entry=dict_entry)

@route('/course/<id:int>/words/<word_uuid>')
def course_word(id: int, word_uuid: str) -> Any:
    try:
        dbcourse = DBCourse.get_by_id(id)
    except VersionedText.DoesNotExist:
        session = UserSession.get_current()
        return template('error', session=session, msg=_("This course does not exist."))

    return repository_word(dbcourse.repo.id, word_uuid, dbcourse=dbcourse)

@route('/repository/<id:int>/words/<word_uuid>/pronunciation/<audio_id:int>')
def pronunciation(id: int, word_uuid: str, audio_id: int) -> Any:
    session = UserSession.get_current()

    try:
        repository = Repository.get_by_id(id)
        word = DBWord.get(repository, word_uuid)
    except Repository.DoesNotExist:
        return template('error', session=session, msg=_("This repository does not exist."))
    except VersionedTextRev.DoesNotExist:
        return template('error', session=session, msg=_("This revision does not exist for the given word."))
    except VersionedText.DoesNotExist:
        return template('error', session=session, msg=_("This word does not exist in the given course."))

    position = [ Position(_("Repositories"), "/repositories"),
                 Position(repository.name, "/repository/%s" % id),
                 Position(_("Words"), "/repository/%s/words" % id),
                 Position(word.dict_word().lemma(),
                          "/repository/%s/words/%s" % (id, word_uuid)),
                 Position("%s %d" % (_("Pronunciation"), audio_id + 1),
                          "/repository/%s/words/%s/pronunciation/%d" % (id, word_uuid, audio_id)) ]

    return template('pronunciation', session=session, position=position,
                    repository=repository, word=word, audio_id=audio_id)

@route('/')
@route('/courses')
def courses() -> Any:
    session = UserSession.get_current()

    languages = Language.languages
    dbcourses: Dict[Language, List[DBCourse]] = {}
    for c in DBCourse.get_all():
        l = c.content.lang
        if not l in dbcourses:
            dbcourses[l] = []
        dbcourses[l].append(c)

    return template('courses', session=session, languages=languages,
                    dbcourses=dbcourses)

@route('/course/<id:int>', ['POST', 'GET'])
def course(id: int) -> Any:
    session = UserSession.get_current()
    user = session.user if session else None

    try:
        dbcourse = DBCourse.get_by_id(id)
        course = dbcourse.content
    except VersionedText.DoesNotExist:
        return template('error', session=session, msg=_("This course does not exist."))

    if user and 'activate_skill' in request.query:
        skill_id = request.query.get('activate_skill', type=int)
        dbcourse.activate_skill(user, skill_id)

    if user and 'update_config' in request.forms:
        source_dialect_name = request.forms.get('source_dialect')
        target_dialect_name = request.forms.get('target_dialect')
        try:
            src_d = course.src_lang.dialects[source_dialect_name]
            tgt_d = course.lang.dialects[target_dialect_name]
        except KeyError:
            return template('error', session=session, msg=_("Invalid request"))
        CourseUserConfig.set_for_course(course, user, src_d, tgt_d)

    config = CourseUserConfig.get_for_course(course, user)

    skills = dbcourse.get_user_skills(user)

    suggested_practise_wg = None
    suggested_practise_gg = None
    suggested_new_wg = None
    suggested_new_gg = None

    words_total = sum(s.total_words for s in skills)
    words_active = sum(s.user_words for s in skills
                       if hasattr(s, "user_words"))
    words_score = sum(s.words_score for s in skills
                      if hasattr(s, "words_score"))
    words_target_score = sum(s.words_target_score for s in skills
                             if hasattr(s, "words_target_score"))

    gg_total = sum(s.total_tags for s in skills)
    gg_active = sum(s.user_tags for s in skills
                    if hasattr(s, "user_tags"))
    gg_score = sum(s.grammar_score for s in skills
                   if hasattr(s, "grammar_score"))
    gg_target_score = sum(s.grammar_target_score for s in skills
                          if hasattr(s, "grammar_target_score"))

    lessons = dbcourse.get_lessons()

    # FIXME Suggest only skills where there is at least one phrase for each
    # word and tag
    for s in skills:
        if s.skill.skill_type == CourseSkillType.WORDS:
            if hasattr(s, "user_words"):
                if not suggested_new_wg and s.user_words < s.total_words:
                    suggested_new_wg = s

                if s.phrases.new > s.phrases.current:
                    suggested_new_wg = max(
                        suggested_new_wg, s,
                        key=lambda s: s.phrases.added if s else 0)

                if s.user_words and s.phrases.current and (s.score < s.target_score):
                    suggested_practise_wg = min(
                        suggested_practise_wg, s,
                        key=lambda s: (s.score / s.target_score) if s else 2)
        else:
            if hasattr(s, "user_tags"):
                if not suggested_new_gg:
                    if s.phrases.total == 0 and s.user_tags == 0:
                        suggested_new_gg = s
                    elif s.user_tags < s.total_tags:
                        suggested_new_gg = s

                if s.phrases.new > s.phrases.current:
                    suggested_new_gg = max(
                        suggested_new_gg, s,
                        key=lambda s: s.skill.priority + s.phrases.added if s else 0)

                if s.user_tags and s.phrases.current and (s.score < s.target_score):
                    suggested_practise_gg = min(
                        suggested_practise_gg, s,
                        key=lambda s: (s.score / s.target_score) if s else 2)

    if (suggested_practise_wg is not None and
        suggested_practise_wg.score / suggested_practise_wg.target_score < 0.7):
        suggested_new_wg = None

    if (suggested_practise_gg is not None and
        suggested_practise_gg.score / suggested_practise_gg.target_score < 0.7):
        suggested_new_gg = None

    if (suggested_new_gg is not None and
        suggested_new_wg is not None and
        suggested_new_gg.skill.priority >= 2 * suggested_new_wg.skill.priority):
        suggested_new_wg = None

    suggest_general_practice = not user or (
        (suggested_practise_wg is not None and
         suggested_practise_wg.score < suggested_practise_wg.target_score)
        or
        (suggested_practise_gg is not None and
         suggested_practise_gg.score < suggested_practise_gg.target_score))

    position = [ Position(_("Courses"), "/courses"),
                 Position(course.name, "/course/%d" % id) ]
    return template('course', session=session, position=position,
                    course=course, config=config,
                    words_total=words_total, words_active=words_active,
                    words_score=words_score, words_target_score=words_target_score,
                    gg_total=gg_total, gg_active=gg_active,
                    gg_score=gg_score, gg_target_score=gg_target_score,
                    suggest_general_practice=suggest_general_practice,
                    suggested_practise_wg=suggested_practise_wg,
                    suggested_practise_gg=suggested_practise_gg,
                    suggested_new_wg=suggested_new_wg,
                    suggested_new_gg=suggested_new_gg)

@route('/course/<id:int>/skills')
def course_skills(id: int) -> Any:
    session = UserSession.get_current()
    user = session.user if session else None

    try:
        dbcourse = DBCourse.get_by_id(id)
        course = dbcourse.content
    except VersionedText.DoesNotExist:
        return template('error', session=session, msg=_("This course does not exist."))

    if user and 'activate_skill' in request.query:
        skill_id = request.query.get('activate_skill', type=int)
        dbcourse.activate_skill(user, skill_id)

    lessons = dbcourse.get_lessons()

    skills = dbcourse.get_user_skills(user)
    skill_sections: Dict[str, List[CourseSkill]] = defaultdict(list)
    skill_secion_mapping = course.skill_section_mapping()

    for s in skills:
        skill_sections[skill_secion_mapping[s.skill.name]].append(s)

    position = [ Position(_("Courses"), "/courses"),
                 Position(course.name, "/course/%d" % id),
                 Position(_("Skills"), "/course/%d/skills" % id) ]
    return template('course_skills', session=session, position=position,
                    course=course, skill_sections=skill_sections)

def word_list(dbcourse: DBCourse, user: Optional[User],
              skill_id: int) -> List[Dict[str, Any]]:
    repository = dbcourse.repo
    course = dbcourse.content

    words = []
    for w in sorted(dbcourse.get_user_words(user, skill_id),
                    key=lambda w: (-w.uw.priority if hasattr(w, "uw") else 0.0, w.word.lemma())):
        dict_entry = dictionary.lookup(repository, w)
        translations = [str(t)
                        for d in dict_entry.exact_matches()
                        if course.src_lang in d.translations
                        for t in d.translations[course.src_lang]]
        words.append({
            'word': w,
            'translation': ", ".join(translations),
        })

    return words

@route('/course/<id:int>/skill/<skill_id:int>')
def course_skill(id: int, skill_id: int) -> Any:
    session = UserSession.get_current()
    user = session.user if session else None
    origin = request.query.get('origin', default='skills')

    try:
        dbcourse = DBCourse.get_by_id(id)
        course = dbcourse.content
    except VersionedText.DoesNotExist:
        return template('error', session=session, msg=_("This course does not exist."))

    lessons = dbcourse.get_lessons()
    skill = dbcourse.get_user_skills(user, skill_id)[0]

    lesson = None
    if skill.skill.lesson:
        lesson = lessons[UUID(skill.skill.lesson)]

    words = word_list(dbcourse, user, skill_id)
    tags = dbcourse.get_user_words

    if user:
        has_inactive_elements = (
            skill.user_tags < skill.total_tags or
            skill.user_words < skill.total_words)
    else:
        has_inactive_elements = False

    description = None
    if skill.skill.description:
        doc = Document.parse(_("Description"), skill.skill.description)
        repo = dbcourse.repo
        ctx = PageContext(DBAliasResolver(repo),
                          DBTranslationContext.get_by_repo(repo, course.lang),
                          course.lang.default_dialect, True)
        description = doc.format_html(ctx)

    if origin == "course":
        url = f"/course/{course.id}"
    else:
        url = f"/course/{course.id}/skills"

    position = [ Position(_("Courses"), "/courses"),
                 Position(course.name, "/course/%d" % id),
                 Position(_("Skills"), "/course/%d/skills" % id),
                 Position(skill.skill.name, "/course/%d/skills/%d" % (id, skill_id)) ]
    return template('skill', session=session, position=position,
                    course=course, skill=skill, words=words, tags=tags,
                    description=description, lesson=lesson,
                    has_inactive_elements=has_inactive_elements,
                    url=url)

@route('/course/<id:int>/config')
def course_config(id: int) -> Any:
    session = UserSession.get_current()
    user = session.user if session else None

    if not user:
        return template('error', session=session, msg=_("Permission denied."))

    try:
        dbcourse = DBCourse.get_by_id(id)
        course = dbcourse.content
    except VersionedText.DoesNotExist:
        return template('error', session=session, msg=_("This course does not exist."))

    config = CourseUserConfig.get_for_course(course, user)

    position = [ Position(_("Courses"), "/courses"),
                 Position(course.name, "/course/%d" % id),
                 Position(_("Settings"), "/course/%d/config" % id) ]
    return template('course_config', session=session, position=position,
                    course=course, config=config,
                    source_dialects=course.src_dialects,
                    target_dialects=course.dialects)

def course_lesson_exercise(user: Optional[User], dbcourse: DBCourse,
                           config: ExerciseSet.Config, url: str,
                           prev_url: Optional[str] = None,
                           next_url: Optional[str] = None) -> Any:
    ex_set = request.forms.get("ex_set", type=int)
    repository = dbcourse.repo

    if ex_set:
        try:
            es = ExerciseSet.get(repository, ex_set)
        except DBExerciseSet.DoesNotExist:
            raise Exception("This exercise set does not exist (any more). "
                            "Please start a new one.")
    else:
        es = ExerciseSet.create(repository, dbcourse, config, user)

    answer = request.forms.get("answer")
    old_ex = None
    feedback = None
    new_ex = None
    question = None

    if answer is not None:
        old_id = request.forms.get("id", type=int)
        old_ex = es.get_exercise(old_id)
        feedback = old_ex.check_answer(user, answer)
        new_id = old_id + 1
    else:
        new_id = 0

    if new_id < es.count():
        new_ex = es.get_exercise(new_id)
        question = new_ex.get_question()

    return template('lesson_page_exercise', url=url,
                    course=dbcourse.content, repository=repository,
                    feedback=feedback, feedback_exercise=old_ex,
                    exercise=new_ex,
                    question=question, exercise_set=es,
                    prev_url=prev_url, next_url=next_url)

def course_lesson_words(dbcourse: DBCourse, words: Sequence[str], url: str,
                        prev_url: Optional[str],
                        next_url: Optional[str]) -> Any:
    index = request.params.get("index", type=int, default=1)
    if not 1 <= index <= len(words):
        raise Exception(_("This word does not exist."))

    prev_info: Dict[str, str] = {}
    next_info: Dict[str, str] = {}

    if index > 1:
        prev_url = url
        prev_info["index"] = str(index - 1)
    if index < len(words):
        next_url = url
        next_info["index"] = str(index + 1)

    repository = dbcourse.repo

    dbword = DBWord.get(repository, words[index - 1])
    dict_entry = dictionary.lookup(repository, dbword)

    # FIXME Don't hardcode the dialect
    word = dbword.dict_word()
    next_info["add_word"] = str(word.uuid)
    audio = word.get_audio(Dialect.ULSTER_CO)

    return template('lesson_page_words', word=word,
                    src_lang=dbcourse.content.src_lang,
                    defs=dict_entry.exact_matches(), audio=audio,
                    prev_url=prev_url, prev_info=prev_info,
                    next_url=next_url, next_info=next_info)

@route('/course/<id:int>/lesson/<lesson_id:int>')
@route('/course/<id:int>/lesson/<lesson_id:int>/<page_number:int>', ['POST', 'GET'])
def course_lesson(id: int, lesson_id: int, page_number: int = 1) -> Any:
    session = UserSession.get_current()
    user = session.user if session else None

    try:
        dbcourse = DBCourse.get_by_id(id)
        course = dbcourse.content
    except VersionedText.DoesNotExist:
        return template('error', session=session, msg=_("This course does not exist."))

    try:
        dblesson = DBLesson.get_by_id(lesson_id)
    except VersionedText.DoesNotExist:
        return template('error', session=session, msg=_("This lesson does not exist."))

    lesson = dblesson.content
    if (dblesson.repo != dbcourse.repo
            or dblesson.uuid not in course.lessons):
        return template('error', session=session,
                        msg=_("This lesson does not belong to this course."))

    if not 1 <= page_number <= len(lesson.pages):
        return template('error', session=session, msg=_("This page does not exist."))
    page = lesson.pages[page_number - 1]

    add_word = request.params.get("add_word")
    if user and add_word:
        UserWords.practised(user, UUID(add_word), False, 0)

    if user and 'activate_skill' in request.query:
        skill_id = request.query.get('activate_skill', type=int)
        dbcourse.activate_skill(user, skill_id)

    try:
        url = f"/course/{course.id}/lesson/{lesson_id}/{page_number}"
        prev_url = next_url = None
        if page_number > 1:
            prev_url = f"/course/{course.id}/lesson/{lesson_id}/{page_number - 1}"
        if page_number < len(lesson.pages):
            next_url = f"/course/{course.id}/lesson/{lesson_id}/{page_number + 1}"
        else:
            next_url = f"/course/{course.id}"

        if isinstance(page, LessonPageText):
            content = template('lesson_page_text', page=page, prev_url=prev_url,
                               next_url=next_url)
        elif isinstance(page, LessonPageWords):
            content = course_lesson_words(dbcourse, page.words, url, prev_url,
                                          next_url)
        elif isinstance(page, LessonPageExercise):
            content = course_lesson_exercise(user, dbcourse, page.config, url,
                                             prev_url, next_url)
        else:
            raise Exception(_("Unknown page type."))
    except Exception as e:
        return template('error', session=session, msg=str(e))

    position = [ Position(_("Courses"), "/courses"),
                 Position(course.name, "/course/%d" % id) ]
    return template('lesson', session=session, position=position,
                    course=course, lesson=lesson, content=content)

@route('/course/<id:int>/words')
def course_words(id: int) -> Any:
    session = UserSession.get_current()
    user = session.user if session else None

    try:
        dbcourse = DBCourse.get_by_id(id)
        course = dbcourse.content
    except VersionedText.DoesNotExist:
        return template('error', session=session, msg=_("This course does not exist."))

    words = word_list(dbcourse, user, 0)

    position = [ Position(_("Courses"), "/courses"),
                 Position(course.name, "/course/%d" % id),
                 Position(_("Words"), "/course/%d/words" % id) ]

    return template('course_words', session=session, position=position,
                    course=course, words=words)

@route('/course/<id:int>/phrases')
def course_phrases(id: int) -> Any:
    session = UserSession.get_current()
    user = session.user if session else None

    try:
        dbcourse = DBCourse.get_by_id(id)
        repository = dbcourse.repo
        course = dbcourse.content
    except VersionedText.DoesNotExist:
        return template('error', session=session, msg=_("This course does not exist."))

    phrases = DBPhrase.get_exercises(repository, dbcourse, 0)

    result = []
    for db_p in phrases:
        param = db_p.phrasetranslationindex.param_config_index
        t = db_p.phrasetranslationindex.text

        result.append({
            'phrase': db_p,
            'param': param,
            'translation': t,
        })

    result = sorted(result, key=lambda x: str(x['translation']).lower())

    position = [ Position(_("Courses"), "/courses"),
                 Position(course.name, "/course/%d" % id),
                 Position(_("Phrases"), "/course/%d/phrases" % id) ]

    return template('course_phrases', session=session, position=position,
                    course=course, result=result)

@route('/course/<id:int>/phrase/<uuid>/<param:int>')
def course_phrase(id: int, uuid: str, param: int) -> Any:
    session = UserSession.get_current()
    user = session.user if session else None

    try:
        dbcourse = DBCourse.get_by_id(id)
        repository = dbcourse.repo
        course = dbcourse.content
    except VersionedText.DoesNotExist:
        return template('error', session=session, msg=_("This course does not exist."))

    try:
        phrase = DBPhrase.get(repository, uuid)
    except VersionedText.DoesNotExist:
        return template('error', session=session, msg=_("This phrase does not exist."))

    ctx = DBTranslationContext.get_by_phrase_uuid(
        repository, [UUID(uuid)], course.src_lang, True)

    result = []
    ambiguous = defaultdict(list)

    for v in phrase.phrase.variants:
        if v.param_config_index != param:
            continue

        if v.lang in (course.lang, course.src_lang):
            ctx.switch_language(v.lang)
        else:
            continue

        if isinstance(v.text, str):
            translations: Iterable[Translation] = [Translation(v.lang, v.text)]
        else:
            translations = ctx.translate(v.text, include_ambiguous=True)

        for t in translations:
            info = {
                'variant': v,
                'translation': t,
            }
            if t.meaning == 0:
                result.append(info)
            else:
                ambiguous[t.meaning].append(info)

    position = [ Position(_("Courses"), "/courses"),
                 Position(course.name, "/course/%d" % id),
                 Position(_("Phrases"), "/course/%d/phrases" % id) ]

    return template('course_phrase', session=session, position=position,
                    course=course, result=result, ambiguous=ambiguous)

@route('/course/<id:int>/exercise', ['POST', 'GET'])
def course_exercise(id: int, ex_set: Optional[int] = None) -> Any:
    session = UserSession.get_current()
    user = session.user if session else None

    try:
        dbcourse = DBCourse.get_by_id(id)
        repository = dbcourse.repo
        course = dbcourse.content
    except VersionedText.DoesNotExist:
        return template('error', session=session, msg=_("This course does not exist."))

    # TODO Use a single parameter 'skill'
    wg_id = request.params.get('wg', type=int, default=0)
    gg_id = request.params.get('gg', type=int, default=0)
    skill_id = request.params.get('skill', type=int, default=0)
    skill_id = skill_id or wg_id or gg_id

    if user and 'activate_skill' in request.query:
        skill_id = request.query.get('activate_skill', type=int)
        dbcourse.activate_skill(user, skill_id)

    course_cfg = CourseUserConfig.get_for_course(course, user)

    # FIXME Let the user configure the dialects
    config = ExerciseSet.Config(course.src_lang, course_cfg.source_dialect,
                                course.lang, course_cfg.target_dialect,
                                translate_to_target=3,
                                translate_from_target=1,
                                concept_to_target=0,
                                concept_from_target=0,
                                correct_translation=2,
                                focus_skill=skill_id)
    try:
        content = course_lesson_exercise(user, dbcourse, config,
                                         f"/course/{dbcourse.id}/exercise",
                                         next_url=f"/course/{dbcourse.id}")
    except Exception as e:
        return template('error', session=session, msg=str(e))

    return template('exercise', session=session, course=course,
                    content=content)

@route('/set_language/<lang>')
def set_language(lang: str) -> Any:
    session = UserSession.get_current()
    if not translation.validate_language(lang):
        return template('error', session=session, msg=_("Invalid language code."))
    elif request.request_language != lang:
        response.set_cookie('lang', lang, max_age=30*24*3600, path="/")
        redir = request.query.get('redirection', None)
        if redir and redir[0] == '/':
            bottle.redirect(redir)
        else:
            bottle.redirect(request.fullpath)
    return template('set_language', session=session, lang=lang)


@route('/static/<filename:path>')
def static(filename: str) -> Any:
    return static_file(filename, root="./templates/static")

if __name__ == '__main__':
    if True:
        import logging
        logger = logging.getLogger("peewee")
        logger.addHandler(logging.StreamHandler())
        logger.setLevel(logging.DEBUG)

    load_repo()
    bottle.install(TranslationBottlePlugin())
    bottle.TEMPLATE_PATH=["./templates/"]
    run(host='0.0.0.0', port=8080, reloader=True, debug=True)
