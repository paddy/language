from typing import *

from enum import Enum

class ObjectType(Enum):
    OTHER   = 0
    WORD    = 1
    CONCEPT = 2
    PHRASE  = 3
    LESSON  = 4
    COURSE  = 5

import database
from user import User

from peewee import CharField, ForeignKeyField, IntegrityError
from peewee import IntegerField, TextField, UUIDField
from peewee import TimestampField, BooleanField
from peewee import DoesNotExist
from peewee import JOIN
from bottle import request, response

import hashlib
import json
import datetime
from uuid import UUID

class Repository(database.DBObject):
    name = CharField()
    owner = ForeignKeyField(User, backref="repositories")
    parent = ForeignKeyField('self', backref="forks", null=True)

class VersionedTextData(database.DBObject):
    hash = CharField(unique=True)
    data = TextField()

    @staticmethod
    def generate_hash(s: str) -> str:
        return hashlib.sha256(s.encode('utf-8')).hexdigest()

    def __str__(self) -> str:
        return str(self.data)

def current_timestamp() -> datetime.datetime:
    return datetime.datetime.now().replace(microsecond=0)

class VersionedTextRev(database.DBObject):
    hash = CharField(unique=True)
    prev_rev = ForeignKeyField('self', backref="next_rev", null=True)
    date = TimestampField(default=current_timestamp)
    author = ForeignKeyField(User, backref="versioned_text_revs")
    committer = ForeignKeyField(User, backref="versioned_text_revs")
    changelog = TextField()
    data = ForeignKeyField(VersionedTextData, backref="revs")

    def generate_hash(self) -> str:
        json_text = json.dumps({
            'prev_rev':     self.prev_rev.hash if self.prev_rev else None,
            'date':         int(self.date.timestamp()),
            'changelog':    self.changelog,
            'data':         self.data.hash,
        })
        return hashlib.sha256(json_text.encode('utf-8')).hexdigest()

    def __str__(self) -> str:
        return str(self.data.data)

    @staticmethod
    def create(**kwargs: Any) -> 'VersionedTextRev':
        if 'data' in kwargs and isinstance(kwargs['data'], str):
            hash = VersionedTextData.generate_hash(kwargs['data'])
            data, created = VersionedTextData.get_or_create(hash=hash, data=kwargs['data'] )
            kwargs['data'] = data
        rev = VersionedTextRev(**kwargs)
        rev.hash = rev.generate_hash()
        rev.save()
        return rev

    @staticmethod
    def get_select(id: Optional[int] = None,
                   repository: Optional[Repository] = None,
                   objtype: Optional[ObjectType] = None) -> Any:
        select = (VersionedTextRev.select(VersionedTextRev, VersionedText,
                                          VersionedTextData)
                                  .join(VersionedText, JOIN.LEFT_OUTER)
                                  .switch(VersionedTextRev)
                                  .join(VersionedTextData))

        if id:
            select = select.where(VersionedTextRev.id == id)
        if repository:
            select = select.where(VersionedText.repository == repository)
        if objtype:
            select = select.where(VersionedText.type == objtype.value)

        return select

class VersionedText(database.DBObject):
    uuid = UUIDField()
    repository = ForeignKeyField(Repository, backref="versioned_texts")
    type = IntegerField(default=ObjectType.OTHER.value)
    current = ForeignKeyField(VersionedTextRev, backref="objects")
    obsoleted_by = ForeignKeyField('self', backref="obsoletes", null=True)
    rejected_rev = IntegerField(default=0)
    inherit = BooleanField(default=True)

    class Meta:
        # (uuid, repository) must be unique
        indexes = (
            (('uuid', 'repository'), True),
        )

    def __str__(self) -> str:
        return str(self.current.data.data)

    @staticmethod
    def get_select(uuid: Optional[UUID] = None,
                   repository: Optional[Repository] = None,
                   objtype: Optional[ObjectType] = None,
                   extra_select: Optional[List[Type[database.DBObject]]] = None) -> Any:

        tables = [VersionedText, VersionedTextRev, VersionedTextData]
        if extra_select:
            tables += extra_select
        select = (VersionedText.select(*tables)
                               .join(VersionedTextRev)
                               .join(VersionedTextData))

        if uuid:
            select = select.where(VersionedText.uuid == uuid)
        if repository:
            select = select.where(VersionedText.repository == repository)
        if objtype:
            select = select.where(VersionedText.type == objtype.value)

        return select

    @staticmethod
    def get_by_uuid(repository: Repository, uuid: UUID) -> 'VersionedText':
        obj = VersionedText.get_select(repository=repository, uuid=uuid).get()
        return cast(VersionedText, obj)

    def is_editable(self, user: User, rev: VersionedTextRev) -> bool:
        if self.current != rev:
            return False
        if user != self.repository.owner:
            return False
        return True

database.register_tables([VersionedTextData, VersionedTextRev, VersionedText])
