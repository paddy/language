from typing import *

import json
from json_util import create_from_json_dict
from uuid import UUID
import uuid

from language import *

from language.course import (
    Course,
    Lesson,
    LessonContext
)
from language.language import (
    Concept,
    ConceptDefinition,
    DictWord,
    Language,
    Phrase,
    TranslationContext,
    TranslationContextStatic,
)
from json_util import AliasResolver
import versioning

class ParseError(Exception):
    pass

class LanguageData(LessonContext, AliasResolver):
    def __init__(self) -> None:
        self.words: Dict[UUID, DictWord] = {}
        self.concepts: Dict[UUID, ConceptDefinition] = {}
        self.phrases: Dict[UUID, Phrase] = {}
        self.lessons: Dict[UUID, Lesson] = {}
        self.courses: Dict[UUID, Course] = {}
        self.aliases: Dict[str, Optional[str]] = {}

        self.concept_index: List[Any] = []
        self._phrase_index: List[Any]

        self.alias_resolver = self

    def add_alias(self, alias: str, uuid: str) -> None:
        if alias in self.aliases and self.aliases[alias] != uuid:
            self.aliases[alias] = None
            return
        self.aliases[alias] = uuid

    def resolve_alias(self, alias: str) -> str:
        if alias in self.aliases:
            result = self.aliases[alias]
            if result:
                return result
        return alias

    def get_translation_context(self, lang: Optional[Language]) -> TranslationContext:
        return TranslationContextStatic(lang, self.words, self.concepts)

    def parse_words(self, words: List[Any]) -> None:
        if not isinstance(words, list):
            raise ParseError("'words' must be a list")

        self.resolve_aliases(words)

        for w in words:
            word_uuid = UUID(w.pop('uuid')) if 'uuid' in w else uuid.uuid1()
            word = DictWord.from_json_dict(word_uuid, w)
            self.words[word_uuid] = word
            self.add_alias(f'@{word.lemma()}', str(word_uuid))
            self.add_alias(f'@{word.language.code}:{word.lemma()}',
                           str(word_uuid))

    def parse_concepts(self, concepts: List[Any]) -> None:
        if not isinstance(concepts, list):
            raise ParseError("'concepts' must be a list")

        ctx = self.get_translation_context(None)

        for c in concepts:
            self.resolve_aliases(c)
            concept = create_from_json_dict(ConceptDefinition, c)
            self.concepts[concept.uuid] = concept

            index = concept.generate_index(ctx)
            self.concept_index.append({
                'uuid': str(concept.uuid),
                'aliases': list(index.aliases),
                'words': list(str(w) for w, l in index.words),
            })

            for a in index.aliases:
                self.add_alias(f'%{a}', str(concept.uuid))

    def parse_phrases(self, phrases: List[Any]) -> None:
        if not isinstance(phrases, list):
            raise ParseError("'phrases' must be a list")

        self.resolve_aliases(phrases)

        for p in phrases:
            if isinstance(p, dict) and 'uuid' in p:
                phrase_uuid = UUID(p.pop('uuid'))
            else:
                phrase_uuid = uuid.uuid1()

            phrase = Phrase.from_json(p)
            self.phrases[phrase_uuid] = phrase

        # FIXME Move alias calculation to Phrase, do it only once
        for i in self.phrase_index:
            phrase_uuid = i['uuid']
            for c in i['configs']:
                for v in c:
                    self.add_alias(f'#{v["text"]}', str(phrase_uuid))

    def parse_lessons(self, lessons: List[Any]) -> None:
        if not isinstance(lessons, list):
            raise ParseError("'lessons' must be a list")

        self.resolve_aliases(lessons)

        for i, l in enumerate(lessons):
            l['id'] = i
            l['lctx'] = self
            lesson_uuid = UUID(l.pop('uuid')) if 'uuid' in l else uuid.uuid1()
            lesson = create_from_json_dict(Lesson, l)
            self.lessons[lesson_uuid] = lesson
            self.add_alias(f'&{lesson.alias}', str(lesson_uuid))

    def parse_courses(self, courses: List[Any]) -> None:
        if not isinstance(courses, list):
            raise ParseError("'courses' must be a list")

        self.resolve_aliases(courses)

        for i, c in enumerate(courses, 1):
            c['id'] = i
            course_uuid = UUID(c.pop('uuid')) if 'uuid' in c else uuid.uuid1()
            course = create_from_json_dict(Course, c)
            self.courses[course_uuid] = course

    @property
    def word_index(self) -> List[Any]:
        return [
            {
                'uuid': str(uuid),
                'language': w.language.code,
                'lemma': w.lemma(),
                'forms': [form for form in w.get_forms()]
            }
            for uuid, w in self.words.items()
        ]

    @property
    def phrase_index(self) -> List[Any]:
        if hasattr(self, "_phrase_index"):
            return self._phrase_index

        result = []
        ctx = self.get_translation_context(None)

        for uuid, phrase in self.phrases.items():
            index = phrase.generate_index(ctx)

            variants: List[List[Any]] = [
                [
                    {
                        'lang': v.lang.code,
                        'text': v.text,
                        'words': [str(uuid) for uuid in v.words],
                        'tags': list(v.tags),
                    }
                    for v in index[param_config_index]
                ]
                for param_config_index in index
            ]

            result.append({
                'uuid': str(uuid),
                'configs': variants,
            })

        self._phrase_index = result
        return result

    @property
    def lesson_index(self) -> List[Any]:
        return [
            {
                'uuid': str(uuid),
                'alias': l.alias,
            }
            for uuid, l in self.lessons.items()
        ]

    @property
    def course_index(self) -> List[Any]:
        result = []

        for uuid, c in self.courses.items():
            skills = []
            for s in c.skills:
                skills.append({
                    'name': s.name,
                    'skill-type': s.skill_type.name,
                    'lesson': s.lesson,
                    'emoji': s.emoji,
                    'words': [ str(w.uuid) for w in s.words ],
                    'tags': [ t for t in s.tags ],
                })

            result.append({
                'uuid': str(uuid),
                'skills': skills,
            })

        return result

    @staticmethod
    def import_file(filename: str) -> 'LanguageData':
        with open(filename) as f:
            data = f.read()

        json_dict = json.loads(data)
        if not isinstance(json_dict, dict):
            raise ParseError("The file doesn't contain a JSON dict")

        ld = LanguageData()
        ld.parse_words(json_dict.pop('words', []))
        ld.parse_concepts(json_dict.pop('concepts', []))
        ld.parse_phrases(json_dict.pop('phrases', []))
        ld.parse_lessons(json_dict.pop('lessons', []))
        ld.parse_courses(json_dict.pop('courses', []))

        if json_dict:
            raise ParseError("Unknown key '%s'" % next(iter(json_dict)))

        return ld
