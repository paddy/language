#!/usr/bin/env python3

from typing import cast, Any, Dict

from translation import gettext as _

from uuid import UUID

from language import irish
from language.irish import (
    Dialect, Number, Person, Gender, IrishNoun, IrishVerb, IrishAdjective,
    IrishVP, IrishNP, IrishPronounNP, IrishAP, IrishPP, IrishCopP,
    IrishPronoun, IrishPrep, IrishParticle, IrishPar)
from language.language import (
    Concept, ConceptArg, ConceptDefinition, ConceptTranslationRule,
    Translation, TranslationContextStatic)

bi = IrishVerb("bí", **{
    "vn": "bheith",
    "present": [ { "form": "tá", "no-analytic-ending": True, "dialects":  [ "ULSTER_D", "ULSTER_CO", "CONNACHT_D", "CONNACHT_CO", "MUNSTER_CO" ] },
                 { "form": "tá", "dialects": [ "MUNSTER_D" ] } ],
    "present-dep": [ { "form": "fuil", "no-analytic-ending": True, "dialects":  [ "ULSTER_D", "ULSTER_CO", "CONNACHT_D", "CONNACHT_CO", "MUNSTER_CO" ] },
                     { "form": "fuil", "dialects": [ "MUNSTER_D" ] } ],
    "ní+present-dep": [ { "form": "níl", "no-analytic-ending": True, "dialects":  [ "ULSTER_D", "ULSTER_CO", "CONNACHT_D", "CONNACHT_CO", "MUNSTER_CO" ] },
                        { "form": "níl", "dialects": [ "MUNSTER_D" ] },
                        { "form": "ní fhuil", "no-analytic-ending": True, "dialects": [ "ULSTER_D" ] } ],
    "pres-1sg": [ { "form": "tá", "analytic": True, "dialects": [ "ULSTER_D", "ULSTER_CO", "CONNACHT_D", "CONNACHT_CO" ] },
                  { "form": "táim", "dialects": [ "MUNSTER_D", "MUNSTER_CO" ] }  ],
    "pres-1sg-dep": [ { "form": "fuil", "analytic": True, "dialects": [ "ULSTER_D", "ULSTER_CO", "CONNACHT_D", "CONNACHT_CO" ] },
                      { "form": "fuilim", "dialects": [ "MUNSTER_D", "MUNSTER_CO" ] }  ],
    "ní+pres-1sg-dep": [ { "form": "níl", "analytic": True, "dialects": [ "ULSTER_D", "ULSTER_CO", "CONNACHT_D", "CONNACHT_CO" ] },
                         { "form": "nílim", "dialects": [ "MUNSTER_D", "MUNSTER_CO" ] }  ],
    "pres-2sg": [ { "form": "tá", "analytic": True, "dialects": [ "ULSTER_D", "ULSTER_CO", "CONNACHT_D", "CONNACHT_CO", "MUNSTER_CO" ] },
                  { "form": "tánn", "analytic": True, "dialects": [ "MUNSTER_D" ] },
                  { "form": "taoi", "dialects": [ "MUNSTER_D" ] } ],
    "pres-sb": "táthar",
    "past-dep": [ { "form": "raibh", "past-particles": False, "with-ending": False, "dialects": [ "MUNSTER_D", "MUNSTER_CO", "ULSTER_CO", "CONNACHT_CO" ] },
                  { "form": "rabh", "past-particles": False, "with-ending": True, "dialects": [ "MUNSTER_D", "MUNSTER_CO", "ULSTER_CO", "CONNACHT_CO" ] },
                  { "form": "rabh", "past-particles": False, "dialects": [ "ULSTER_D", "CONNACHT_D" ] } ],
    "past-sb": "bhíothas",
    "past-sb-dep": { "form": "rabhthas", "past-particles": False },
    "fut": { "form": "be", "use-tense-marker": False },
})
ag = IrishPrep(**{
    "word": "ag",
    "1sg": "agam",
    "2sg": "agat",
    "3sg-m": "aige",
    "3sg-f": "aici",
    "1pl": [ "againn", { "form": "aghainn", "dialects": [ "TEILEANN" ] } ],
    "2pl": "agaibh",
    "3pl": "acu",
})

arg0 = ConceptArg(0, IrishNP)
arg1 = ConceptArg(1, IrishNP)

irish_tctx = TranslationContextStatic(irish.irish, {
        UUID(int=0): IrishNoun(**{ # type: ignore
            "nom-sg": "madadh",
            "gen-sg": "madaidh",
            "nom-pl": "madaí",
            "gen-pl": "madaí",
            "dialects": [Dialect.ULSTER, Dialect.CONNACHT],
        }),
        UUID(int=1): IrishNoun(**{ # type: ignore
            "nom-sg": "madra",
            "gen-sg": "madra",
            "nom-pl": "madraí",
            "gen-pl": "madraí",
            "dialects": [Dialect.ULSTER_CO, Dialect.CONNACHT_CO, Dialect.MUNSTER],
        }),
        UUID(int=2): IrishNoun(**{ # type: ignore
            "nom-sg": "gadhar",
            "gen-sg": "gadhair",
            "nom-pl": "gadhair",
            "gen-pl": "gadhar"
        }),
        UUID(int=3): bi,
        UUID(int=4): IrishNoun(**{ # type: ignore
            "nom-sg": "sionnach",
            "gen-sg": "sionnaigh",
            "nom-pl": "sionnaigh",
            "gen-pl": "sionnach"
        }),
        UUID(int=5): IrishAdjective(root="rua"),
        UUID(int=6): IrishNoun(**{ # type: ignore
            "nom-sg": "liathróid",
            "gen-sg": "liathróide",
            "nom-pl": "liathróidí",
            "gen-pl": "liathróidí"
        }),
        UUID(int=7): ag,
        UUID(int=8): IrishNoun(**{ # type: ignore
            "nom-sg": [ { "form": "leabhar", "genders": ["MASC"], "dialects": ["MUNSTER","CONNACHT","ULSTER_CO"] },
                        { "form": "leabhar", "genders": ["FEM"], "dialects": ["ULSTER_D"] } ],
            "gen-sg": "leabhair",
            "nom-pl": [ { "form": "leabhair" },
                        { "form": "leabharthaí", "dialects": ["ULSTER"] } ],
            "gen-pl": [ { "form": "leabhar" },
                        { "form": "leabharthach", "dialects": ["ULSTER"] } ],
        }),
        UUID(int=9): IrishAdjective(root="maith"),
        UUID(int=10): IrishNoun(**{ # type: ignore
            "nom-sg": { "form": "bean", "genders": ["FEM"] },
            "gen-sg": "mná",
            "nom-pl": "mná",
            "gen-pl": "ban",
        }),
        UUID(int=11): IrishAdjective(root="mór"),
        UUID(int=12): IrishParticle(word="agus"),
        UUID(int=13): IrishPronoun(**cast(Dict[str, Any], {
            "person": Person.FIRST,
            "number": Number.SINGULAR,
            "subj": "mé",
            "subj-emph": "mise"
        })),
        UUID(int=14): IrishPronoun(**cast(Dict[str, Any], {
            "person": Person.THIRD,
            "number": Number.SINGULAR,
            "subj": "sé",
            "obj": "é",
            "subj-emph": "seisean"
        })),
        UUID(int=15): IrishPronoun(**cast(Dict[str, Any], {
            "person": Person.THIRD,
            "number": Number.SINGULAR,
            "gender": Gender.FEM,
            "subj": "sí",
            "obj": "í",
            "subj-emph": "ise"
        })),
    }, {
        #'vc:1() -> v:1(#2; prep:ag(#1))'
        UUID(int=1): ConceptDefinition([{"language": "ga", "data": {
            "type": "IrishVP",
            "verb": "%s" % UUID(int=3),
            "subject": { "arg": 1, "type": "IrishNP" },
            "adverbial": [{
                "type": "IrishPP",
                "preposition": "%s" % UUID(int=7),
                "object": { "arg": 0, "type": "IrishNP" }
            }]
        }}], args=2),
        UUID(int=2): ConceptDefinition([{"language": "ga", "data": { "type": IrishNP, "noun": "%s" % UUID(int=0) }} ]),
        UUID(int=2): ConceptDefinition([{"language": "ga", "data": { "type": IrishNP, "noun": "%s" % UUID(int=1) }} ]),
        UUID(int=2): ConceptDefinition([{"language": "ga", "data": { "type": IrishNP, "noun": "%s" % UUID(int=2), "dialects": [Dialect.CONNACHT_D, Dialect.MUNSTER_D] }} ]),
        UUID(int=3): ConceptDefinition([{"language": "ga", "data": { "type": IrishPronounNP, "pronoun": "%s" % UUID(int=13) }}]),
        UUID(int=4): ConceptDefinition([{"language": "ga", "data": { "type": IrishNP, "noun": "%s" % UUID(int=4) }} ]),
        UUID(int=4): ConceptDefinition([{"language": "ga", "data": { "type": IrishNP, "noun": "%s" % UUID(int=0), "attributes": [{ "type": IrishAP, "adjective": UUID(int=5) }] }} ]),
        UUID(int=4): ConceptDefinition([{"language": "ga", "data": { "type": IrishNP, "noun": "%s" % UUID(int=1), "attributes": [{ "type": IrishAP, "adjective": UUID(int=5) }] }} ]),
        UUID(int=5): ConceptDefinition([{"language": "ga", "data": { "type": IrishNP, "noun": UUID(int=6) }}  ]),
        UUID(int=6): ConceptDefinition([{"language": "ga", "data": { "type": IrishNP, "noun": UUID(int=8) }}  ]),
        UUID(int=7): ConceptDefinition([{"language": "ga", "data": { "type": IrishAP, "adjective": UUID(int=9) }} ]),
        UUID(int=8): ConceptDefinition([{"language": "ga", "data": { "type": IrishNP, "noun": UUID(int=10) }} ]),
        UUID(int=9): ConceptDefinition([{"language": "ga", "data": {
            "type": IrishVP,
            "verb": UUID(int=3),
            "subject": { "arg": 0, "type": "IrishNP" },
            "object": { "arg": 1, "type": "IrishNP" },
        }}], args=2),
        UUID(int=10): ConceptDefinition([{"language": "ga", "data": { "type": IrishAP, "adjective": UUID(int=11) }} ]),
        UUID(int=11): ConceptDefinition([{"language": "ga", "data": { "type": IrishPar, "particle": UUID(int=12) }} ]),
        UUID(int=12): ConceptDefinition([{"language": "ga", "data": { "type": IrishCopP }} ]),
        UUID(int=13): ConceptDefinition([{"language": "ga", "data": { "type": IrishPronounNP, "pronoun": ["%s" % UUID(int=14), "%s" % UUID(int=15)] }}]),
        UUID(int=14): ConceptDefinition([{"language": "ga", "data": { "type": IrishPronounNP, "pronoun": "%s" % UUID(int=14) }}]),
    }
)

dog = Concept(UUID(int=2), number=irish.Number.PLURAL)
i = Concept(UUID(int=3))
have = Concept(UUID(int=1), [i, dog])
result = list(irish_tctx.translate(have))
for x in result:
    print("[%s] %s" % (x.dialect_str(), x))
    grammar_tags = (
        x.sentence_grammar_tags |
        set(f"<{x}>" for p in x.parts for x in p.grammar_tags))
    print("         grammar: %s" % grammar_tags)
    for h in x.dialect_hints():
        print("         # %s" % h)

print("")
for d in Dialect:
    x = Translation.best_translation(iter(result), d, True)
    y = Translation.best_translation(iter(result), d, False)
    print("%20s: %-50s (%s)" % (d.dname, x, y))

print("---")
fox = Concept(UUID(int=4), definite=irish.Definiteness.DEFINITE)
have = Concept(UUID(int=1), [fox, i])
result = list(irish_tctx.translate(have))
for x in result:
    print("[%s] %s" % (x.dialect_str(), x))
    grammar_tags = (
        x.sentence_grammar_tags |
        set(f"<{x}>" for p in x.parts for x in p.grammar_tags))
    print("         grammar: %s" % grammar_tags)
    for h in x.dialect_hints():
        print("         # %s" % h)

print("")
for d in Dialect:
    x = Translation.best_translation(iter(result), d, True)
    y = Translation.best_translation(iter(result), d, False)
    print("%20s: %-50s (%s)" % (d.dname, x, y))

print("---")
print("")

woman = Concept(UUID(int=8), number=irish.Number.SINGULAR,
                definite=irish.Definiteness.DEFINITE,
                attributes=[Concept(UUID(int=7))])
have = Concept(UUID(int=1), [dog, woman])
result = list(irish_tctx.translate(have))
for x in result:
    print("[%s] %s" % (x.dialect_str(), x))
    grammar_tags = (
        x.sentence_grammar_tags |
        set(f"<{x}>" for p in x.parts for x in p.grammar_tags))
    print("         grammar: %s" % grammar_tags)
    for h in x.dialect_hints():
        print("         # %s" % h)

print("---")
print("")

agus = Concept(UUID(int=11))
it = Concept(UUID(int=13), ref='book')
big = Concept(UUID(int=10))
ta = Concept(UUID(int=9), [it, big], neg=True)

book = Concept(UUID(int=6), id='book', number=irish.Number.SINGULAR, attributes=[Concept(UUID(int=7))])
have = Concept(UUID(int=1), [i, book], post=[agus, ta])
result = list(irish_tctx.translate(have))
for x in result:
    print("[%s] %s" % (x.dialect_str(), x))
    grammar_tags = (
        x.sentence_grammar_tags |
        set(f"<{x}>" for p in x.parts for x in p.grammar_tags))
    print("         grammar: %s" % grammar_tags)
    for h in x.dialect_hints():
        print("         # %s" % h)

print("---")
print("")

it = Concept(UUID(int=14))
book = Concept(UUID(int=6), definite=irish.Definiteness.INDEFINITE, number=irish.Number.SINGULAR, attributes=[Concept(UUID(int=7))])
cop = Concept(UUID(int=12), subject=it, predicate=book, neg=True)

result = list(irish_tctx.translate(cop))
for x in result:
    print("[%s] %s" % (x.dialect_str(), x))
    grammar_tags = (
        x.sentence_grammar_tags |
        set(f"<{x}>" for p in x.parts for x in p.grammar_tags))
    print("         grammar: %s" % grammar_tags)
    for h in x.dialect_hints():
        print("         # %s" % h)

print("---")
print("")

for verb in [bi,
             irish.IrishVerb("rith"),
             irish.IrishVerb("siúil", **{"stem":"siúl"})]:
    for number in irish.Number:
        persons = [ irish.Person.FIRST, irish.Person.SECOND, irish.Person.THIRD ]
        if number == irish.Number.SINGULAR:
            persons.append(irish.Person.IMPERSONAL)
        for person in persons:
            print("%s. " % person.value, end='')
            for form, synthetic in verb.conjugate(irish.Tense.PRESENT, person, number, True):
                print("[%s] %-12s" % (form.dialect_str(), str(form) + (" *" if not synthetic else "  ")), end=' ')
            print("")
    print("")
