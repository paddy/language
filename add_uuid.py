#!/usr/bin/env python3

import gettext
gettext.translation('messages', "./po/", languages=['de'], fallback=True).install()

from typing import *

import json
import sys
import uuid


def main() -> None:
    if len(sys.argv) == 2:
        filename = sys.argv[1]
    else:
        filename = "data/content.json"

    with open(filename) as f:
        data = f.read()

    json_dict = json.loads(data)
    if not isinstance(json_dict, dict):
        sys.exit("The file doesn't contain a JSON dict")

    for section in json_dict:
        assert section in ['words', 'concepts', 'phrases', 'lessons', 'courses']

        for i, entry in enumerate(json_dict[section]):
            if isinstance(json_dict[section][i], list):
                json_dict[section][i] = { 'phrase': json_dict[section][i] }
            assert isinstance(json_dict[section][i], dict)

        for entry in json_dict[section]:
            if 'uuid' not in entry:
                entry['uuid'] = str(uuid.uuid1())

    print(json.dumps(json_dict, indent=4, ensure_ascii=False))

if __name__ == '__main__':
    main()

