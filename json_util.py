from typing import *
from enum import Enum

import abc
import inspect
import typing

class JSONParseException(Exception):
    pass

def dict_str_to_enum(d: Dict[str, Any], key: str, enum: Type[Enum]) -> None:
    str_dict = d.pop(key, [])
    if str_dict:
        enum_dict = []
        for s in str_dict:
            if isinstance(s, enum):
                enum_dict.append(s)
                continue
            if not isinstance(s, str):
                raise JSONParseException("'%s' can only contain strings" % key)
            enum_dict.append(enum[s])
        d[key] = enum_dict

def check_and_convert_type(key: str, input: Dict[str, Any], t: Type[Any],
                           subclass_map: Dict[Type[Any], Type[Any]]) -> None:
    if t is Any:
        return

    if inspect.isclass(t) and issubclass(t, Enum) and isinstance(input[key], str):
        input[key] = t[input[key]]

    value = input[key]
    generic_origin = t.__origin__ if hasattr(t, "__origin__") else None

    if generic_origin == typing.Union:
        for variant in t.__args__:
            try:
                # Change input only on success
                d = { key: value }
                check_and_convert_type(key, d, variant, subclass_map)
                input[key] = d[key]
                return
            except:
                pass
        else:
            raise JSONParseException("Invalid type for key '%s' (expected 'Union[%s]', got '%s')" %
                                     (key, ", ".join(x.__name__ for x in t.__args__), type(value).__name__))

    elif generic_origin and issubclass(generic_origin, Sequence):
        item_type = t.__args__[0]
        if item_type in subclass_map:
            item_type = subclass_map[item_type]
        if inspect.isclass(item_type) and issubclass(item_type, Enum):
            dict_str_to_enum(input, key, item_type)
            value = input[key]

        for x in value:
            d = { key: x }
            check_and_convert_type(key, d, item_type, subclass_map)
            assert d[key] == x

    elif generic_origin and issubclass(generic_origin, Dict):
        key_type = t.__args__[0]
        value_type = t.__args__[1]

        if not isinstance(value, dict):
            raise JSONParseException("Invalid type for key '%s' (expected '%s', got '%s')" %
                                     (key, t.__name__, type(value).__name__))

        for x in value.keys():
            d = { key: x }
            check_and_convert_type(key, d, key_type, subclass_map)
            assert d[key] == x
        for x in value.values():
            d = { key: x }
            check_and_convert_type(key, d, value_type, subclass_map)
            assert d[key] == x

    elif not isinstance(value, t):
        raise JSONParseException("Invalid type for key '%s' (expected '%s', got '%s')" %
                                 (key, t.__name__, type(value).__name__))

def call_from_json_dict(fn: Callable[..., Any], input: Dict[str, Any],
                        subclass_map: Dict[Type[Any], Type[Any]] = {},
                        sig: Optional[inspect.Signature] = None) -> Any:
    if not sig:
        sig = inspect.signature(fn)

    for p in sig.parameters.values():

        name = p.name .replace("_", "-")
        t = p.annotation

        if name == "self" or name not in input:
            continue

        check_and_convert_type(name, input, t, subclass_map)

        if name != p.name:
            input[p.name] = input[name]
            del input[name]

    return fn(**input)

T = TypeVar('T')
def create_from_json_dict(cls: Type[T], input: Dict[str, Any],
                          subclass_map: Dict[Type[Any], Type[Any]] = {}) -> T:
    sig = inspect.signature(cls.__init__)
    obj = call_from_json_dict(cls.__call__, input, subclass_map, sig)
    assert isinstance(obj, cls)
    return obj

def modify_json_str(opaque: T,
                    json: Union[List[Any], Dict[str, Any]],
                    fn: Callable[[T, str], str]) -> None:
    items: Sequence[Tuple[Any, Any]]
    if isinstance(json, dict):
        items = list(json.items())
    elif isinstance(json, list):
        items = list(enumerate(json))
    else:
        return

    for key, value in items:
        if isinstance(value, str):
            json[key] = fn(opaque, value)
        elif isinstance(value, (dict, list)):
            modify_json_str(opaque, json[key], fn)

class AliasResolver(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def resolve_alias(self, alias: str) -> str:
        pass

    def resolve_aliases(self, json: Union[List[Any], Dict[str, Any]]) -> None:
        modify_json_str(self, json, self.__class__.resolve_alias)
