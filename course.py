from typing import *

import database
import peewee
from peewee import CharField, ForeignKeyField, TextField, UUIDField, Value, fn

import json_util
from json_util import AliasResolver
import language
from exercise import (Exercise, TranslationExercise,
                      TranslationCorrectionExercise, ExerciseSet)
from formatting import Document, PageContext
from language.language import (
    Language, Dialect, DictWord, ConceptTranslationRule, TranslationContext)
from language.course import (
    Course, CourseSkillType,
    Lesson, LessonContext, LessonPage
)
from repository import (
    DBAliasResolver, DBCourse, DBLesson, DBPhrase, DBTranslationContext, DBWord,
    Repository, UserWords, UserGrammarTags)
import versioning
from versioning import VersionedText, ObjectType
from user import User

import random
import json
import re
from uuid import UUID

from translation import gettext as _


class DBLessonContext(LessonContext):
    def __init__(self, repo: Repository) -> None:
        super().__init__(DBAliasResolver(repo))
        self.repo = repo

    def get_translation_context(self, lang: Language) -> TranslationContext:
        return DBTranslationContext.get_by_repo(self.repo, lang)


class LessonPageText(LessonPage):
    def __init__(self, lesson: Lesson, text: str, title: Optional[str] = None) -> None:
        super().__init__(lesson, title)
        self.doc = Document.parse(self.title or "", text)

    def get_html(self) -> str:
        lang = self.lesson.lang

        # FIXME Pass CourseUserConfig to get_html() instead of default dialect
        ctx = PageContext(self.lesson.lctx.alias_resolver,
                          self.lesson.lctx.get_translation_context(lang),
                          lang.default_dialect, True)

        return self.doc.format_html(ctx)

class LessonPageWords(LessonPage):
    def __init__(self, lesson: Lesson, words: Sequence[str],
                 title: Optional[str] = None) -> None:
        super().__init__(lesson, title)
        self.words = [self.lesson.lctx.alias_resolver.resolve_alias(w)
                      for w in words]

class LessonPageExercise(LessonPage):
    def __init__(self, lesson: Lesson, title: Optional[str] = None,
                 **kwargs: Any) -> None:
        super().__init__(lesson, title)

        source_lang = lesson.src_lang
        target_lang = lesson.lang

        # FIXME Use CourseUserConfig instead of default dialects
        kwargs.update({
            'source': source_lang,
            'source-dialect': source_lang.default_dialect,
            'target': target_lang,
            'target-dialect': target_lang.default_dialect,
        })
        self.config = json_util.create_from_json_dict(ExerciseSet.Config,
                                                      kwargs)

Lesson.page_types = {
    'text': LessonPageText,
    'words': LessonPageWords,
    'exercise': LessonPageExercise,
}

class CourseUserConfig(database.DBObject):
    course = ForeignKeyField(VersionedText)
    user = ForeignKeyField(User, backref="courses")
    source_dialect = CharField()
    target_dialect = CharField()

    class Meta:
        # (course, user) must be unique
        indexes = (
            (('course', 'user'), True),
        )

    @staticmethod
    def get_for_course(course: Course, user: Optional[User]) -> 'Course.Config':
        if user is not None:
            config = CourseUserConfig.get_or_none(course=course.id, user=user)
            if config is not None:
                source_dialect = course.src_lang.dialects[config.source_dialect]
                target_dialect = course.lang.dialects[config.target_dialect]
                return Course.Config(source_dialect=source_dialect,
                                     target_dialect=target_dialect)

        return Course.Config(source_dialect=course.src_dialects[0],
                             target_dialect=course.dialects[0])

    @staticmethod
    def set_for_course(course: Course, user: User, source_dialect: Dialect,
                       target_dialect: Dialect) -> None:
        assert source_dialect in course.src_lang.dialects
        assert target_dialect in course.lang.dialects

        config, created = CourseUserConfig.get_or_create(
            course=course.id, user=user, defaults={
                'source_dialect': source_dialect.name,
                'target_dialect': target_dialect.name,
            })
        if not created:
            config.source_dialect = source_dialect.name
            config.target_dialect = target_dialect.name
        config.save()

database.register_tables([CourseUserConfig])
