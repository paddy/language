from typing import *
import os
import base64
import database
from peewee import CharField, ForeignKeyField, IntegrityError
from peewee import DoesNotExist
from bottle import request, response
from passlib.hash import bcrypt

class PasswordVerificationFailed(Exception):
    pass

class User(database.DBObject):
    name = CharField(unique=True)
    password = CharField()

    def is_admin(self) -> bool:
        return str(self.name) == "admin"

    def verify_password(self, password: str) -> None:
        if not bcrypt.verify(password, self.password):
            raise PasswordVerificationFailed()

    @staticmethod
    def hash_password(password: str) -> str:
        return str(bcrypt.hash(password))

    @staticmethod
    def init_table() -> None:
        admin = User(name="admin", password=bcrypt.hash("admin"))
        admin.save()

class UserSession(database.DBObject):
    user = ForeignKeyField(User, backref="sessions")
    key = CharField(unique=True)

    @staticmethod
    def get_current() -> Optional['UserSession']:
        key = request.cookies.get("session")
        if not key:
            return None
        return cast(Optional['UserSession'], UserSession.get_or_none(UserSession.key == key))

    @staticmethod
    def login(username: str, password: str) -> bool:
        try:
            user = User.get(User.name == username)
            user.verify_password(password)
            while True:
                key = base64.b64encode(os.urandom(16)).decode('ASCII')
                try:
                    UserSession.create(user=user, key=key)
                except IntegrityError:
                    continue
                break
            response.set_cookie("session", key, max_age=30*24*3600)
            request.cookies["session"] = key
            return True
        except DoesNotExist:
            return False

    def logout(self) -> None:
        self.delete_instance()
        response.delete_cookie("session")

database.register_tables([User, UserSession])
